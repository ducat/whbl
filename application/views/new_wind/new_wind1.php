<?php 
$this->load->view('templates/header', 
		array('title' => lang('cop') . '__' . lang('instrument') ));
?>
<body>


<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => $active,
					'menu_map' => $menu_map));
			?>
			
			<div class = "span10" id = "cop_page" style="margin-top: 20px;min-height: 600px;background-color: rgba(200, 200, 200, 1);">
				
			</div>
			<script src="/assets/js/highcharts/highcharts.js" type="text/javascript"></script>
			<script src="/assets/js/highcharts/modules/exporting.js" type="text/javascript"></script>   
			<script src="/assets/js/highcharts/highcharts-more.js" type="text/javascript"></script>
			
		</div>
	</div>
</body>

<?php 
$this->load->view('templates/footer');
?>
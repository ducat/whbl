<?php 
$this->load->view('templates/header', 
		array('title' =>  $active));
?>
<body>


<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => $active,
					'menu_map' => $menu_map));
			?>
			<div class = "span10" id = "cop_page" style="margin-top: 20px;min-height: 570px;background-color: white;padding:10px;">
				<div id="grid">
				<table class="table table-condensed">
				<thead><tr><th>名称</th><th>编号</th><th>运行状态</th><th>启停控制</th><th>新风温度</th></tr></thead>
				<tbody>
				<?php foreach($data as $v):?>
					<tr>
						<td><?= $v['name']?></td>
						<td><?= $v['ahu']?></td>
						<td><?= $v['on_off']?></td>
						<td><?= $v['command']?></td>
						<td><?= $v['value']?></td>
					</tr>
				<?php endforeach ?>
				</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</body>
<?php 
$this->load->view('templates/footer');
?>
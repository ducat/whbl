<?php 
$this->load->view('templates/header', 
		array('title' => lang('point_view') . '__X'));
?>
<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu',
					array('active_id' => 'point_view'));
			?>
			
			<?php 
			$this->load->view('templates/iframe_right',
					array('url' => site_url('point/point_view')));
			?>
		</div>
	</div>
</body>

<?php 
$this->load->view('templates/footer');
?>
<?php 
$this->load->view('templates/header', 
		array('title' => lang('user_management') . '__X'));
?>
<body>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'user',
					'menu_map' => $menu_map));
			?>
			
			<div class="span10 textbox-holder" style="margin-top: 10px; min-height: 560px;">
				<h3 class="textbox-title">
					<?=lang('user_management')?>
				</h3>
				<div class="textbox-content" style="width: 98%; height: 85; margin: 15px auto;">
					<div id="infoMessage"><?php echo $message;?></div>

					<table class="table table-striped table-bordered" id="user-table">
						<thead>
							<tr class="mythread">
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Groups</th>
								<th>Status</th>
							</tr>
						</thead>
						<?php foreach ($users as $user):?>
							<tr>
								<td><?php echo $user->first_name;?></td>
								<td><?php echo $user->last_name;?></td>
								<td><?php echo $user->email;?></td>
								<td>
									<?php foreach ($user->groups as $group):?>
										<?php echo $group->name;?><br />
					                <?php endforeach?>
								</td>
								<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, 'Active') : anchor("auth/activate/". $user->id, 'Inactive');?></td>
							</tr>
						<?php endforeach;?>
					</table>
					
					<p><a href="<?php echo site_url('auth/create_user');?>">Create a new user</a></p>
				</div>
			</div>
		</div>
	</div>
</body>

<?php 
$this->load->view('templates/footer');
?>

<form method='post' action="<?=site_url('manage/alarm_point/update_alarm_point_detail')?>" style="margin-bottom:5px">
    <table id='detail_table_" . $point_id ."' class='table table-striped' style="margin-bottom:0px;border:1px">
        <tbody>
            <tr>
                <td><?= lang ('alarm_point_id') ?></td>
                <td><input name='id' class='easyui-numberbox' disabled style='background-color:#eeecec;font-size:12px; height: 16px' required='true'></input></td>
                <td><?= lang ('alarm_point_name') ?></td>
                <td><input name='display_name' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
                <td><?= lang ('alarm_point_chinese_name') ?></td>
                <td><input name='chinese_name' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_english_name') ?></td>
                <td><input name='english_name' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
                <td><?= lang ('alarm_point_alarm_high') ?></td>
                <td><input name='alarm_high' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_alarm_low') ?></td>
                <td><input name='alarm_low' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_level') ?></td>
                <td><input name='alarm_level' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
            </tr>       
        </tbody>
    </table>
    <div style="float:right; margin: 0px 5px 5px 0px">
        <button type="submit" class="btn btn-mini"><?=lang('alarm_point_submit')?></button>
    </div>
</form>
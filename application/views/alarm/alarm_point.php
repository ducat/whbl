<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?=$title?></title>
		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
		<link href="/assets/img/dfk.jpg" rel="shortcut icon" />
		<link href="/assets/img/dfk.jpg" rel="bookmark" />
		<link rel="stylesheet" type="text/css" href="/assets/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" type="text/css" href="/assets/easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="/assets/chosen/chosen.css">
		<script src="/assets/js/jquery-1.8.1.min.js"></script>
		<script src="/assets/easyui/jquery.easyui.min.js"></script>
		<script src="/assets/easyui/plugins/jquery.edatagrid.js"></script>
		<script src="/assets/js/bootstrap.js"></script>
		<script src="/assets/chosen/chosen.jquery.js"></script>
		<script type="text/javascript" src="/assets/easyui/plugins/datagrid-detailview.js"></script>
<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
				$this->load->view('templates/side_menu', array(
					'active_id' => $active,
					'menu_map' => $menu_map));
			?>
			
			<div class="span10" style="margin-left: 1%;margin-top: 20px;background-color: rgba(200, 200, 200, 1);width: 84%">
				<table id = "alarm_point_grid" style="height: 600px"></table>
			</div>

		</div>
	</div>
	<script type="text/javascript">	
		$("#alarm_point_grid").datagrid ({
			data: <?= json_encode($data); ?>,
			title: '<?= lang('alarm_point_manage')?>',
			columns:[[
				{
					field:'id', 
					title: '<?= lang('alarm_point_id')?>', 
					halign: 'center', 
					align: 'center'
				},
				{
					field:'display_name', 
					title: '<?= lang('alarm_point_name')?>', 
					halign: 'center', 
					align: 'center',
					editor: 'text',
					width: 20
				},
				{
					field:'chinese_name', 
					title: '<?= lang('alarm_point_chinese_name')?>', 
					halign: 'center', 
					align: 'center',
					editor: 'text',
					width: 40
				},
				{
					field:'english_name', 
					title: '<?= lang('alarm_point_english_name')?>', 
					halign: 'center', 
					align: 'center',
					editor: 'text',
					width: 40
				},
				{
					field:'alarm_high', 
					title: '<?= lang('alarm_point_alarm_high')?>', 
					halign: 'center', 
					align: 'center',
					editor: 'text'
				},
				{
					field:'alarm_low',
					title: '<?= lang('alarm_point_alarm_low')?>',
					halign: 'center', 
					align: 'center',
					editor: 'text'
				},
				{
					field:'alarm_level', 
					title: '<?= lang('alarm_level')?>', 
					halign: 'center', 
					align: 'center',
					editor : 'numberbox'
				}
			]],
			nowrap: true,
			autoRowHeight: true,
			striped: true,
			collapsible: false,
			singleSelect: true,
			checkOnSelect: false,
			selectOnCheck: false,
			pagination: false,
	//		pageNumber: 1,
	//		pageSize: 20,
			fitColumns: true,
			border: true,
			loadMsg: "<?= lang ("data_load_msg")?>",
			view: detailview,
		    detailFormatter:function(index,row){
		        return '<div class="ddv"></div>';
		    },
		    onExpandRow: function(index,row){
		        var ddv = $(this).datagrid('getRowDetail',index).find('div.ddv');
				ddv.panel({
		            border:true,
		            cache:true,
		            href:'<?= site_url ("manage/alarm_point/get_point_detail")?>?id='+row['id'],
		            onLoad:function(){
		                $('#alarm_point_grid').datagrid('fixDetailRowHeight',index);
		                $('#alarm_point_grid').datagrid('selectRow',index);
		                $('#alarm_point_grid').datagrid('getRowDetail',index).find('form').form('load',row);
		            }
		        });
		        $('#alarm_point_grid').datagrid('fixDetailRowHeight',index);
		    },
		})
	</script>
</body>

<script type="text/javascript">
	
</script>

<?php 
	$this->load->view('templates/footer');
?>
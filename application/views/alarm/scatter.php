<?php 
$this->load->view('templates/header', 
        array('title' => lang('alarm_scatter_chart') . "__" . lang('alarm') ));
?>

<body>
<?php
    $this->load->view('templates/page_top',
            array('user' => $user));
?>
    <div class="container-fluid">
        <div class="row-fluid">
            <?php 
            $this->load->view('templates/side_menu', array(
                    'active_id' => 'scatter',
                    'menu_map' => $menu_map));
            ?>          
            
            <div class="textbox-holder span10" style="margin-left: 1%;margin-top: 20px;min-height: 600px">
                <div id="alarm_chart" style="width:98%; height:550px; margin:15px auto;">
                </div>
            </div>
        </div>
    </div>
</body>


<script src="/assets/js/highcharts/highcharts.js"></script>
<script src="/assets/js/highcharts/modules/exporting.js"></script>
<script src="/assets/js/highcharts/themes/grid.js"></script>
<script type="text/javascript">
$(function() {  
    scatter_data = <?php echo json_encode($data, JSON_NUMERIC_CHECK)?> ;
    Highcharts.setOptions({     
        global: {     
            useUTC: false     
        }     
    });
    window.chart = new Highcharts.Chart({
        chart: {
            renderTo: 'alarm_chart',
            type: 'scatter',
            borderWidth: 0,
            zoomType: 'xy'
        },
        exporting : {
            url: '<?=site_url('export/index')?>',
            buttons : {
                printButton : {
                    enabled : false
                }
            }
        },  
        credits : {
            enabled : false
        },
        title: {
            text: '<?= lang('alarm_scatter_chart')?>'
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'TIMELINE'
            },
            type: 'datetime',
            minRange: 3600 * 1000,
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
        },
        yAxis: {
            min: 1,
            minTickInterval: 1,
            minRange: 3,
            title: {
                text: '<?= lang('alarm_point')?>'
            }
        },
        tooltip: {
            formatter: function() {
                if(this.y == 0)
                    return '<b><?= lang('alarm_type')?>:</b><?= lang('alarm_type_system')?><br>' +
                           '<b><?= lang('alarm_point_display_name')?>:</b>' + this.point.display_name + '<br>' +
                           '<b><?= lang('alarm_point_chinese_name')?>:</b>' + this.point.chinese_name + '<br>' +
                           '<b><?= lang('alarm_point_english_name')?>:</b>' + this.point.english_name + '<br>' +
                           '<b><?= lang('alarm_value')?>:</b>' + this.point.value;
                else
                    return '<b><?= lang('alarm_type')?>:</b><?= lang('alarm_type_point')?><br>' +
                           '<b><?= lang('alarm_point_display_name')?>:</b>' + this.point.display_name + '<br>' +
                           '<b><?= lang('alarm_point_chinese_name')?>:</b>' + this.point.chinese_name + '<br>' +
                           '<b><?= lang('alarm_point_english_name')?>:</b>' + this.point.english_name + '<br>' +
                           '<b><?= lang('alarm_value')?>:</b>' + this.point.value;
            }
        },
        legend: {
            enabled: false,
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            backgroundColor: '#FFFFFF',
            borderWidth: 1
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                }
            }
        },
        series: [{
            name: '<?= lang('alarm_type_point')?>',
            color: 'rgba(223, 83, 83, .5)',
            data: scatter_data    
            }]
    });
});
</script>

<?php 
$this->load->view('templates/footer');
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$title?></title>
		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
		<link href="/assets/img/dfk.jpg" rel="shortcut icon" />
		<link href="/assets/img/dfk.jpg" rel="bookmark" />
		<link rel="stylesheet" type="text/css" href="/assets/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" type="text/css" href="/assets/easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="/assets/chosen/chosen.css">
		<script src="/assets/js/jquery-1.8.1.min.js"></script>
		<script src="/assets/easyui/jquery.easyui.min.js"></script>
        <script src="/assets/easyui/plugins/jquery.edatagrid.js"></script>
		<script src="/assets/js/bootstrap.js"></script>
		<script src="/assets/chosen/chosen.jquery.js"></script>
<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
				$this->load->view('templates/side_menu', array(
					'active_id' => $active,
					'menu_map' => $menu_map));
			?>
			
			<div class="span10" style="margin-left: 1%;margin-top: 20px;background-color: rgba(200, 200, 200, 1);width: 84%">
				<?php echo form_open('alarm/get_logs_data'); ?>
				<table id = "logs_grid" style="height:600px"></table>
				<?php echo form_close();?>
			</div>
			<div class="modal hide fade" id="myModal" style="width:700px">
			  <div class="modal-header">
			    <a class="close" data-dismiss="modal">×</a>
			    <h3><?=lang ('alarm_search_title') ?></h3>
			  </div>
			  <div class="modal-body">
			  		<dl class="dl-horizontal" style="margin-bottom:10px">
					  <dt style="width:150px"><?= lang('alarm_log_time') ?>：</dt>
					  <dd style="margin-left:80px">
					  	  <div class="row-fluid">
					  		<div class="span10 well well-large" id="time_search_box" style="padding:8px;margin-bottom:5px;width:370px"></div>
					  		<div class="span2 btn" onclick="add_time_search ()"><i class="icon-plus icon-black"></i></div>
					  	  </div>
					  </dd>
					</dl>
					<dl class="dl-horizontal" style="margin-bottom:10px">
					  <dt style="width:150px"><?= lang('alarm_log_value') ?>：</dt>
					  <dd style="margin-left:80px">
					  		<div id="value_select_box" class="span10 well well-large" style="padding:8px;margin-bottom:5px;width:370px"></div>
					  		<div class="span2 btn" onclick="add_value_search ()"><i class="icon-plus icon-black"></i></div>
					  </dd>
					</dl>

					<dl class="dl-horizontal" style="margin-bottom:10px">
					  <dt style="width:150px"><?= lang('alarm_log_description') ?>：</dt>
					  <dd style="margin-left:80px">
					 	    <input type="text" id="description_box" placeholder="Input key words of alarm description ..." style="width:355px">
					  </dd>
					</dl>

					<dl class="dl-horizontal" style="margin-bottom:10px">
					  <dt style="width:150px"><?= lang('alarm_log_point') ?>：</dt>
					  <dd style="margin-left:80px">
				  			<select id='point_select_box' class='chosen-select' data-placeholder="Choose alarm point ..." style="width:337px" multiple >
				  			</select>
					  </dd>
					</dl>

					<dl class="dl-horizontal" style="margin-bottom:10px">
					  <dt style="width:150px"><?= lang('alarm_log_point_name') ?>：</dt>
					  <dd style="margin-left:80px">
				  			<select id='point_name_select_box' class='chosen-select' data-placeholder="Choose alarm point name ..." style="width:337px" multiple >
				  			</select>
					  </dd>
					</dl>
					
					<dl class="dl-horizontal" style="margin-bottom:10px">
					  <dt style="width:150px"><?= lang('alarm_log_level') ?>：</dt>
					  <dd style="margin-left:80px">
					  		<select id='level_select_box' class='chosen-select' data-placeholder="Choose alarm level ..." style="width:337px;" multiple>
					  		</select>
					  </dd>
					</dl>
					<dl class="dl-horizontal" style="margin-bottom:10px">
					  <dt style="width:150px"><?= lang('alarm_log_sent') ?>：</dt>
					  <dd style="margin-left:80px">
					  		<select id='sent_select_box' class='chosen-select' data-placeholder="Choose alarm status ..." style="width:337px;" multiple>
					  			<option value="1"><?= lang('alarm_success_sent') ?></option>
					  			<option value="-1"><?= lang('alarm_failed_sent') ?></option>
					  			<option value="0"><?= lang('alarm_not_sent') ?></option>
					  		</select>
					  </dd>
					</dl>
			  </div>
			  <div class="modal-footer">
      				<div class="btn-toolbar">
  						<a href="#" class="btn" data-dismiss="modal"><?=lang('alarm_search_cancel')?></a>
		  				<a href="#" class="btn btn-primary" onclick="alarm_search ()"><?=lang('alarm_search_data')?></a>
      				</div>
			  </div>
			</div>
		</div>
	</div>
	
	
	<script type="text/javascript">
		var condition = "";
		var search_time_index = 0;
		var search_value_index = 0;
		var alarm_data = <?= $data ?>;

		$("#logs_grid").datagrid ({
			url: '<?=site_url ("alarm/get_logs_data")?>',
			title: '<span><?= lang('alarm_log')?></span><a class="btn btn-mini" href="<?= site_url('export/exportexcel')?>" style="float:right;margin-right:2%;margin-bottom:10px"><img src="/assets/grocery_crud/themes/flexigrid/css/images/export.png"/>&nbsp<?= lang('export')?></span>',
			columns:[[
				{field:'timestamp', title: '<?= lang('alarm_log_time')?>', halign: 'center', align: 'center', width: 200},
				{field:'description', title: '<?= lang('alarm_log_description')?>', halign: 'center', width: 400, editor: 'text'},
		//		{field:'point_id', title: '<?= lang('alarm_point')?>', halign: 'center', align: 'center', width: 100},
				{field:'display_name', title: '<?= lang('alarm_log_point')?>', halign: 'center', align: 'center', width: 100},
				{field:'name', title: '<?= lang('alarm_log_point_name')?>', halign: 'center', align: 'center', width: 100},
				{field:'value', title: '<?= lang('alarm_log_value')?>', halign: 'center', align: 'center', width: 60, formatter: function (value) {
					return value.replace(/^(\d+\.\d{2})\d*$/,"$1");
				}},
				{field:'level', title: '<?= lang('alarm_log_level')?>', halign: 'center', align: 'center', width: 50},
				{field:'sent', title: '<?= lang('alarm_log_sent')?>', halign: 'center', align: 'center', width: 100, formatter: function (value) {
/*                    switch (value) {
                        case '0' : 
                            return "<?= lang('alarm_not_sent') ?>"; 
                        case '1' : 
                            return "<?= lang('alarm_mail').lang('alarm_success_sent') ?>"; 
                        case '-1' : 
                            return "<?= lang('alarm_mail').lang('alarm_failed_sent') ?>"; 
                        case '2' : 
                            return "<?= lang('alarm_mail').lang('alarm_failed_sent'). ';' .lang('alarm_sms').lang('alarm_success_sent') ?>"; 
                        case '-2' : 
                            return "<?= lang('alarm_success_sent'). ';' .lang('alarm_sms').lang('alarm_failed_sent') ?>"; 
                        case '3' : 
                            return "<?= lang('alarm_sms').lang('alarm_success_sent') ?>"; 
                        case '-3' : 
                            return "<?= lang('alarm_sms').lang('alarm_failed_sent') ?>"; 
                        case '4' : 
                            return "<?= lang('alarm_mail').lang('alarm_success_sent'). ';' .lang('alarm_sms').lang('alarm_success_sent') ?>"; 
                        case '-4' : 
                            return "<?= lang('alarm_mail').lang('alarm_success_sent'). ';' .lang('alarm_sms').lang('alarm_failed_sent') ?>"; 
                        default : return "<?= lang('alarm_sent_unknown') ?>";
                    }*/

                    if (value == 0) 
						return "<?= lang('alarm_not_sent') ?>";
					else if (value < 0)
						return "<?= lang('alarm_failed_sent') ?>";
					else if (value > 0)
						return "<?= lang('alarm_success_sent') ?>";
                    
                }},
			]],
			nowrap: true,
			autoRowHeight: false,
			striped: true,
			collapsible: false,
			rownumbers: true,
			singleSelect: true,
			pagination: true,
			pageNumber: 1,
			pageSize: 20,
			fitColumns: true,
			border: true,
			loadMsg: "<?= lang ("data_load_msg")?>",
			toolbar: [{
				text:'<?= lang('save')?>',
				iconCls: 'icon-save',
				handler: save_log
			}],
	/*		loadFilter : function (data) {
				for (var i = 0; i < data["rows"].length; i++) {
					switch (data["rows"][i]["sent"]) {
						case '0' : 
							data["rows"][i]["sent"] = "<?= lang('alarm_not_sent') ?>"; 
							break;
						case '1' : 
							data["rows"][i]["sent"] = "<?= lang('alarm_mail').lang('alarm_success_sent') ?>"; 
							break;
						case '-1' : 
							data["rows"][i]["sent"] = "<?= lang('alarm_mail').lang('alarm_failed_sent') ?>"; 
							break;
						case '2' : 
							data["rows"][i]["sent"] = "<?= lang('alarm_mail').lang('alarm_failed_sent'). ';' .lang('alarm_sms').lang('alarm_success_sent') ?>"; 
							break;
						case '-2' : 
							data["rows"][i]["sent"] = "<?= lang('alarm_success_sent'). ';' .lang('alarm_sms').lang('alarm_failed_sent') ?>"; 
							break;
						case '3' : 
							data["rows"][i]["sent"] = "<?= lang('alarm_sms').lang('alarm_success_sent') ?>"; 
							break;
						case '-3' : 
							data["rows"][i]["sent"] = "<?= lang('alarm_sms').lang('alarm_failed_sent') ?>"; 
							break;
						case '4' : 
							data["rows"][i]["sent"] = "<?= lang('alarm_mail').lang('alarm_success_sent'). ';' .lang('alarm_sms').lang('alarm_success_sent') ?>"; 
							break;
						case '-4' : 
							data["rows"][i]["sent"] = "<?= lang('alarm_mail').lang('alarm_success_sent'). ';' .lang('alarm_sms').lang('alarm_failed_sent') ?>"; 
							break;

						default : data["rows"][i]["sent"] = "<?= lang('alarm_sent_unknown') ?>";
					}
				};
				return data;
			},*/
			onClickRow: onClickRow
		})
		
		var pager = $("#logs_grid").datagrid ('getPager');
		$(pager).pagination ({
			pageSize: 20,
			pageList: [10, 20, 30, 40],
			beforePageText: '<?= lang ('data_grid_pager_before_text')?>',
			afterPageText: '<?= lang ('data_grid_pager_after_text')?>',
			displayMsg: '<?= lang('data_grid_pager_displayMsg')?>',
			buttons:[{
				iconCls:'icon-search',
				handler: function ()
				{
					$('#myModal').modal('show');
				}
			}]
		})
		$.extend($.fn.datagrid.methods, {
            editCell: function(jq,param){
                return jq.each(function(){
                    var opts = $(this).datagrid('options');
                    var fields = $(this).datagrid('getColumnFields',true).concat($(this).datagrid('getColumnFields'));
                    for(var i=0; i<fields.length; i++){
                        var col = $(this).datagrid('getColumnOption', fields[i]);
                        col.editor1 = col.editor;
                        if (fields[i] != param.field){
                            col.editor = null;
                        }
                    }
                    $(this).datagrid('beginEdit', param.index);
                    for(var i=0; i<fields.length; i++){
                        var col = $(this).datagrid('getColumnOption', fields[i]);
                        col.editor = col.editor1;
                    }
                });
            }
        });

		$(function() {
			for (var i = 0; i < alarm_data["points"].length; i++) {
				$("#point_select_box").append ("<option value='" + alarm_data["points"][i]["id"] + "'>" + alarm_data["points"][i]["display_name"] + "</option>");
			};

			for (var i = 0; i < alarm_data["points"].length; i++) {
				$("#point_name_select_box").append ("<option value='" + alarm_data["points"][i]["id"] + "'>" + alarm_data["points"][i]["name"] + "</option>");
			};

			for (var i = 0; i < alarm_data["levels"].length; i++) {
				$("#level_select_box").append ("<option>" + alarm_data["levels"][i]["level"] + "</option>");
			};
			//$(".search-box").chosen ();
			$(".chosen-select").chosen();
			add_time_search ("default");
			add_value_search ("default");
		})

		// formatter the output of datetimebox
		$.fn.datebox.defaults.formatter = function(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+m+'-'+d;
		}

		function alarm_search ()
		{
		/*	var start_time = $("#start-time-box").datetimebox ('getValue');
			var end_time = $("#end-time-box").datetimebox ('getValue');
			var type = $("#type-search-box").val ();
			var location = $("#location-search-box").val ();
			var value = $("#value-search-box").val ();
			var level = $("#level-search-box").val ();
			var state = $("#state-search-box").val ();
		*/
			condition = "";
			var page_number = $("#logs_grid").datagrid ('getPager').data ('pagination').options.pageNumber;
			var page_size = $("#logs_grid").datagrid ('getPager').data ('pagination').options.pageSize;
			var flag;
			var init_flag;

			init_flag = 0;
			/************************************* Get time search condition ************************************/
			flag = 0;
			for (var i = 0; i < search_time_index; i++) {
				if ($("#datetimebox_" + i + "_0").length > 0) {
					if (init_flag == 0) {
						init_flag = 1;
						flag = 1;
						condition += " where ((";
					}
					else if (flag == 0) {
						flag = 1;
						condition += " and ((";
					}
					else {
						condition += ") or (";
					}
					condition += "logs.timestamp >= '" + $("#datetimebox_" + i + "_0").datetimebox ('getValue') + "' and logs.timestamp <= '" + $("#datetimebox_" + i + "_1").datetimebox ('getValue') + "' ";
				};
			};
			if (flag == 1)
				condition += "))";

			/************************************ Get value search condition ************************************/
			flag = 0;
			for (var i = 0; i < search_value_index; i++) {
				if ($("#valuebox_sel_" + i).length > 0 && $("#valuebox_val_0").val () != "") {
					if (init_flag == 0) {
						init_flag = 1;
						flag = 1;
						condition += " where (";
					}
					else if (flag == 0) {
						flag = 1;
						condition += " and (";
					}
					else {
						condition += " and ";
					}

					op = $("#valuebox_sel_" + i).val ();
					value = $("#valuebox_val_" + i).val ();
					condition += "value " + op + " " + value;
				};
			};

			if (flag == 1) 
				condition += ")";
			/************************************ Get key words condition ************************************/
			var description = $("#description_box").val ();

			if (init_flag == 0) {
				init_flag = 1;
				condition += " where ";
			}
			else {
				condition += " and ";
			}
			if (description != "") {
				condition += "(chinese_description like '%" + description + "%' or description like '%" + description + "%')";
			};

			/**************************** Get points / point names search condition ******************************/
			var points = $("#point_select_box").val();
			var points_name = $("#point_name_select_box").val ();

			flag = 0;
			if (points != null) {
				for (var i = 0; i < points.length; i++) {
					if (init_flag == 0) {
						init_flag = 1;
						flag = 1;
						condition += " where (";
					}
					else if (flag == 0) {
						flag = 1;
						condition += " and (";
					}
					else
						condition += " or ";
					condition += "point_id = " + points[i];
				}
			}

			if (points_name != null) {
				for (var i = 0; i < points_name.length; i++) {
					if (init_flag == 0) {
						init_flag = 1;
						flag = 1;
						condition += " where (";
					}
					else if (flag == 0) {
						flag = 1;
						condition += " and (";
					}
					else
						condition += " or ";
					condition += "point_id = " + points_name[i];
				}
			}

			if (flag == 1)
				condition += ")";

			/************************************** Get level search condition **********************************/
			var levels = $("#level_select_box").val ();
			flag = 0;
			if (levels != null) {
				for (var i = 0; i < levels.length; i++) {
					if (init_flag == 0) {
						init_flag = 1;
						flag = 1;
						condition += " where (";
					}
					else if (flag == 0) {
						flag = 1;
						condition += " and (";
					}
					else
						condition += " or ";
					condition += "level = " + levels[i];
				}
			}

			if (flag == 1) 
				condition += ")";

			/********************************** Get sent status search condition ********************************/
			
			var sents = $("#sent_select_box").val ();
			flag = 0;
			if (sents != null) {
				for (var i = 0; i < sents.length; i++) {
					if (init_flag == 0) {
						init_flag = 1;
						flag = 1;
						condition += " where (";
					}
					else if (flag == 0) {
						flag = 1;
						condition += " and (";
					}
					else
						condition += " or ";

					if (sents [i] < 0)
						condition += "sent < 0";
					else if (sents [i] > 0)
						condition += "sent > 0";
					else
						condition += "sent = 0";
				}
			}

			if (flag == 1) 
				condition += ")";

			$.post('<?=site_url('alarm/get_search_data')?>', 
			{
				'condition': condition,
				'page_number' : page_number,
				'page_size' :page_size
			},function (data)
			{
				$('#myModal').modal('hide');
				$("#logs_grid").datagrid ('loadData', data);
			}, 'json');
		}

		function reset_search ()
		{
			
		}

		function add_time_search (type) {
			if (type == "default") {
				$("#time_search_box").append ("<div style='margin-bottom: 8px;font-size:12px'>" +
												"<span>" + 
													"From: <input id='datetimebox_" + search_time_index + "_0' type='text' style='width:125px'>" +
												"</span>" +
												"<span style='margin-left:5px'>" + 
													"To: <input id='datetimebox_" + search_time_index + "_1' type='text' style='width:125px'>" +
												"</span>" +
											"</div>");
			}
			else {
				$("#time_search_box").append ("<div class='alert alert-block' style='padding: 0 0 0 0;background-color: transparent;border: transparent;color: black;margin-bottom: 8px;width: 94%;font-size:12px'>" +
									"<button type='button' class='close' data-dismiss='alert'>&times;</button>" +
									"<div>" +
										"<span>" + 
											"From: <input id='datetimebox_" + search_time_index + "_0' type='text' style='width:125px'>" +
										"</span>" +
										"<span style='margin-left:5px'>" + 
											"To: <input id='datetimebox_" + search_time_index + "_1' type='text' style='width:125px'>" +
										"</span>" +
									"</div>" +
								"</div>");
			};


			$("#datetimebox_" + search_time_index + "_0").datetimebox ({
				value: alarm_data["times"]["start_time"],
				required: true,
				showSeconds: false
			})
			$("#datetimebox_" + search_time_index + "_1").datetimebox ({
				value: alarm_data["times"]["end_time"],
				required: true,
				showSeconds: false
			})

			search_time_index ++;
		}

		function add_value_search (type) {
			if (type == "default") {
				$("#value_select_box").append ("<div style='font-size:12px'>" +
													"<select id='valuebox_sel_" + search_value_index + "' style='width:80px;'>" +
														"<option>></option>" +
											  			"<option>>=</option>" +
											  			"<option><</option>" +
											  			"<option><=</option>" +
											  			"<option>==</option>" +
											  			"<option>!=</option>" +
											  		"</select>" +
											  		"<input id='valuebox_val_" + search_value_index + "' type='number' placeholder='  Input alarm value...' style='height:20px;padding:0;margin-left:20px'>" +
												"</div>");

			}
			else {
				$("#value_select_box").append ("<div class='alert alert-block' style='padding: 0 0 0 0;background-color: transparent;border: transparent;color: black;margin-bottom: 0px;width: 94%;font-size:12px'>" +
													"<button type='button' class='close' data-dismiss='alert'>&times;</button>" +
													"<div>" +
														"<select id='valuebox_sel_" + search_value_index + "' style='width:80px;'>" +
															"<option>></option>" +
												  			"<option>>=</option>" +
												  			"<option><</option>" +
												  			"<option><=</option>" +
												  			"<option>==</option>" +
												  			"<option>!=</option>" +
												  		"</select>" +
												  		"<input id='valuebox_val_" + search_value_index + "' type='number' placeholder='  Input alarm value...' style='height:20px;padding:0;margin-left:20px'>" +
													"</div>" +
												"</div>");
			}

			search_value_index ++;
		}

		function save_log ()
		{
			if(endEditing()){
				$.messager.confirm('', '<?= lang("alarm_level_save_confirm")?>', function(r){
					if (r){
						if ($('#logs_grid').datagrid('validateRow', editIndex)){
							oldIndex = editIndex;
							var changed_data = new Object ();
							var updated_rows = $("#logs_grid").datagrid ("getChanges", "updated");
							changed_data['updated_rows'] = JSON.stringify (updated_rows);
							$.post('<?=site_url('alarm/update_alarm_log')?>',changed_data,function (){
								$('#logs_grid').datagrid('endEdit', oldIndex);
							}, 'json');
							editIndex = undefined;
							return true;
						}		
					}
				});	
			}		
		}

        var editIndex = undefined;
        var oldIndex = -1;
		function endEditing(){
			if (editIndex == undefined){return true}
			if ($('#logs_grid').datagrid('validateRow', editIndex)){
				var ed = $('#logs_grid').datagrid('getEditor', {index:editIndex,field:'description'});
				var description = ed.target.val();
				var rows = $('#logs_grid').datagrid('getRows');
				var logs_id = rows[editIndex]['id'];
				oldIndex = editIndex;

				$('#logs_grid').datagrid('endEdit', oldIndex);
				editIndex = undefined;
				return true;
			} else {
				return false;
			}
		}
		function onClickRow(index){
			if (editIndex != index){
				if (endEditing()){
					$('#logs_grid').datagrid('selectRow', index)
							.datagrid('beginEdit', index);
					editIndex = index;
				} else {
					$('#logs_grid').datagrid('selectRow', editIndex);
				}
			}
		}
		
	</script>
</body>

<?php 
	$this->load->view('templates/footer');
?>
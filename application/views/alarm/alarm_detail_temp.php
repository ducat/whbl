
<form method='post' action="<?=site_url('manage/alarm_point/update_alarm_point_detail')?>" style="margin-bottom:5px">
    <table id='detail_table_" . $point_id ."' class='table table-striped' style="margin-bottom:0px;border:1px">
        <tbody>
            <tr>
                <td><?= lang ('alarm_point_id') ?></td>
                <td><input name='id' class='easyui-numberbox' disabled style='background-color:#eeecec;font-size:12px; height: 16px' required='true'></input></td>
                <td><?= lang ('alarm_point_name') ?></td>
                <td><input name='display_name' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
                <td><?= lang ('alarm_point_chinese_name') ?></td>
                <td><input name='chinese_name' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_english_name') ?></td>
                <td><input name='english_name' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
                <td><?= lang ('alarm_point_alarm_high') ?></td>
                <td><input name='alarm_high' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_alarm_low') ?></td>
                <td><input name='alarm_low' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_alarm_high_summer') ?></td>
                <td><input name='alarm_high_summer' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            
                <td><?= lang ('alarm_point_alarm_low_summer') ?></td>
                <td><input name='alarm_low_summer' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_alarm_high_winter') ?></td>
                <td><input name='alarm_high_winter' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>   
            <tr> 
                <td><?= lang ('alarm_point_alarm_low_winter') ?></td>
                <td><input name='alarm_low_winter' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_level') ?></td>
                <td><input name='alarm_level' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
            </tr>       
        </tbody>
    </table>
    <div style="float:right; margin: 0px 5px 5px 0px">
        <button type="submit" class="btn btn-mini"><?=lang('alarm_point_submit')?></button>
    </div>
</form>

<!--
<form method='post' action="<?=site_url('alarm/update_alarm_point_detail')?>" style="margin-bottom:5px">
    <table id='detail_table_" . $point_id ."' class='table table-striped' style="margin-bottom:0px;border:1px">
        <tbody>
            <tr>
                <td><?= lang ('alarm_point_id') ?></td>
                <td><input name='id' class='easyui-numberbox' disabled style='background-color:#eeecec;font-size:12px; height: 16px' required='true'></input></td>
                <td><?= lang ('alarm_point_name') ?></td>
                <td><input name='display_name' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
                <td><?= lang ('alarm_point_chinese_name') ?></td>
                <td><input name='chinese_name' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_english_name') ?></td>
                <td><input name='english_name' class='easyui-validatebox' style='font-size:12px; height: 16px' required='true'></input></td>
                <td><?= lang ('alarm_point_is_virtual_point') ?></td>
                <td><select name='is_virtual_point' disabled class='easyui-combobox'><option value=0>否</option><option value=1>是</option></select></td>
                <td><?= lang ('alarm_point_is_bacnet') ?></td>
                <td><select name='is_bacnet' disabled class='easyui-combobox'><option value=0>否</option><option value=1>是</option></select></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_type') ?></td>
                <td><input name='type' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_type_large') ?></td>
                <td>
                    <select name='type_large' class='easyui-combobox'>
                        <option value='binary'><?= lang ('alarm_point_binary') ?></option>
                        <option value='transient'><?= lang('alarm_point_treasient') ?></option>
                        <option value='accumulated'><?= lang('alarm_point_accumulated') ?></option>
                    </select>
                </td>
                <td><?= lang ('alarm_point_type_small') ?></td>
                <td><input name='type_small' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_serve_sys') ?></td>
                <td><input name='serve_sys' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_serve_obj') ?></td>
                <td><input name='serve_obj' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_unit') ?></td>
                <td><input name='unit' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_port') ?></td>
                <td><input name='source_port' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_offset') ?></td>
                <td><input name='offset' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_expression') ?></td>
                <td><input name='expression' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_expression_display') ?></td>
                <td><input name='expression_display' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_timestamp') ?></td>
                <td><input name='timestamp' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_delete') ?></td>
                <td><input name='deleted' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_alarm_high') ?></td>
                <td><input name='alarm_high' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_alarm_low') ?></td>
                <td><input name='alarm_low' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_alarm_high_summer') ?></td>
                <td><input name='alarm_high_summer' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_alarm_low_summer') ?></td>
                <td><input name='alarm_low_summer' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_alarm_high_winter') ?></td>
                <td><input name='alarm_high_winter' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_alarm_low_winter') ?></td>
                <td><input name='alarm_low_winter' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>   
            <tr>
                <td><?= lang ('alarm_point_alarm_attack') ?></td>
                <td><input name='alarm_attack' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_alarm_release') ?></td>
                <td><input name='alarm_release' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_filter_start') ?></td>
                <td><input name='alarm_filter_start' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_filter_end') ?></td>
                <td><input name='alarm_filter_end' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_level') ?></td>
                <td><input name='alarm_level' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_byte') ?></td>
                <td><input name='byte' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>
            <tr>
                <td><?= lang ('alarm_point_bcu_device_id') ?></td>
                <td><input name='bcu_device_id' disabled class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_proper_value_high') ?></td>
                <td><input name='proper_value_high' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
                <td><?= lang ('alarm_point_proper_value_low') ?></td>
                <td><input name='proper_value_low' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>       
            <tr>
                <td><?= lang ('alarm_modbus_id') ?></td>
                <td><input name='modbus_id' class='easyui-validatebox' style='font-size:12px; height: 16px'></input></td>
            </tr>
        </tbody>
    </table>
    <div style="float:right; margin: 0px 5px 5px 0px">
        <button type="submit" class="btn btn-mini"><?=lang('alarm_point_submit')?></button>
    </div>
 
</form>

-->
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?=$title?></title>
<?php 
foreach($output->css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
		<link href="/assets/jqueryui-bootstrap/css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet">
		<link href="/assets/img/dfk.jpg" rel="shortcut icon" />
		<link href="/assets/img/dfk.jpg" rel="bookmark" />

<?php foreach($output->js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
		<script src="/assets/js/bootstrap.js"></script>
<script>
var where1 = new Array(3); 
where1[0] = new comefrom("binary","On/Off|Run/Stop|Manual/Auto|Fault/Normal"); 
where1[1] = new comefrom("transient","Temperature|Area_Temperature|I|CO2|SPT|Humidity|Regulated_value|Flux|Cooling_Capacity|Pressure|Regulated_value|Power|Current|Voltage|Conductivity|Frequency|Power_Factor|Frequence_HZ_State|PH|IT_A_Temperature|UPS_A_Temperature|VAV_Temperature|VAV_A_Temperature"); 
where1[2] = new comefrom("accumulated","Electric_Consumption|Steam|Refrigeration|Heat|HotWater|Water"); 
var where1_display_chinese = new Array(3); 
where1_display_chinese[0] = new comefrom("开关","开/关|运行/停机|手动/自动|故障"); 
where1_display_chinese[1] = new comefrom("实时","温度|区域温度|焓值|CO2|设定值|湿度|开度反馈|流量|制冷量|压力|调节值|功率|电流|电压|电导率|频率|功率因数|反馈状态|PH值|IT机房温度|UPS机房温度|VAV系统送风温度|VAV区域房间温度"); 
where1_display_chinese[2] = new comefrom("累积","电量|蒸汽|制冷|供热|热水|水"); 
var where1_display_english = new Array(3); 
where1_display_english[0] = new comefrom("binary","On/Off|Run/Stop|Manual/Auto|Fault/Normal"); 
where1_display_english[1] = new comefrom("transient","Temperature|Area_Temperature|I|CO2|SPT|Humidity|Regulated_value|Flux|Cooling_Capacity|Pressure|Regulated_value|Power|Current|Voltage|Conductivity|Frequency|Power_Factor|Frequence_HZ_State|PH|IT_A_Temperature|UPS_A_Temperature|VAV_Temperature|VAV_A_Temperature"); 
where1_display_english[2] = new comefrom("accumulated","Electric_Consumption|Steam|Refrigeration|Heat|HotWater|Water"); 


var where2 = new Array(4); 
where2[0] = new comefrom("lighting_socket","Room|Emergency|Outdoor|Public_area"); 
where2[1] = new comefrom("airconditioner","Refrigeration_station|Heat_exchange_station|Air_conditioning_terminal"); 
where2[2] = new comefrom("power","Elevator|Pump|Ventilation|Fire_control"); 
where2[3] = new comefrom("special","Swimming|Laundry|Restaurant|Gym|Game_center"); 
var where2_display_chinese = new Array(4); 
where2_display_chinese[0] = new comefrom("照明与插座","房间照明与插座|走廊与应急照明|室外景观照明|公共区域照明"); 
where2_display_chinese[1] = new comefrom("空调","制冷站|换热站|空调末端"); 
where2_display_chinese[2] = new comefrom("动力","电梯|水泵|通风|消防"); 
where2_display_chinese[3] = new comefrom("特殊用途","游泳池|洗衣房|厨房餐厅|健身房|游戏中心"); 
var where2_display_english = new Array(4); 
where2_display_english[0] = new comefrom("lighting_socket","Room|Emergency|Outdoor|Public_area"); 
where2_display_english[1] = new comefrom("airconditioner","Refrigeration_station|Heat_exchange_station|Air_conditioning_terminal"); 
where2_display_english[2] = new comefrom("power","Elevator|Pump|Ventilation|Fire_control"); 
where2_display_english[3] = new comefrom("special","Swimming|Laundry|Restaurant|Gym|Game_center"); 






$(function(){
	<?php 
	if($output->state == 'edit'){
		if (current_lang() == "en"){
			echo "initEdit(where2, where2_display_english, 'serve-sys', 'serve-obj','" . $output->serve_sys . "', '" . $output->serve_obj . "');";
			echo "initEdit(where1, where1_display_english, 'type-large', 'type-small','" . $output->type_large . "', '" . $output->type_small . "');";
		}
		else {
			echo "initEdit(where2, where2_display_chinese, 'serve-sys', 'serve-obj','" . $output->serve_sys . "', '" . $output->serve_obj . "');";
			echo "initEdit(where1, where1_display_chinese, 'type-large', 'type-small','" . $output->type_large . "', '" . $output->type_small . "');";
		}
		
	}
	else if ($output->state == 'add'){
		if (current_lang() == "en"){
			echo "init(where2,where2_display_english,'serve-sys', 'serve-obj');";	
			echo "init(where1,where1_display_english,'type-large', 'type-small');";
		}
		else 
			echo "init(where2,where2_display_chinese,'serve-sys', 'serve-obj');";
			echo "init(where1,where1_display_chinese,'type-large', 'type-small');";
	}
		
	?>
	
});

function comefrom(loca,locacity) { this.loca = loca; this.locacity = locacity; }  
function select(where,where_dis,obj,c_id) { 
    var p_value = obj.value;    
    var city = document.getElementById(c_id); 
    for(var i = 0;i < where.length;i ++) { 
        if (where[i].loca == p_value) { 
            cities = (where[i].locacity).split("|");   
            cities_dis = (where_dis[i].locacity).split("|");           
            for(var j = 0;j < cities.length;j++) {
             city.options.length = cities.length; 
             city.options[j].text = cities_dis[j]; 
             city.options[j].value = cities[j];
            }
        }         
    }    
} 
function init(where,where_dis,p_id,c_id) { 
    var province = document.getElementById(p_id); 
    var city = document.getElementById(c_id);     
    with(province) { 
        length = where.length; 
        for(var k=0;k<where.length;k++) { options[k].text = where_dis[k].loca; options[k].value = where[k].loca; } 
        options[selectedIndex].text = where_dis[0].loca; options[selectedIndex].value = where[0].loca; 
    } 
    with(city) { 
        cities = (where[0].locacity).split("|"); 
        cities_dis = (where_dis[0].locacity).split("|"); 
        length = cities.length; 
        for(var l=0;l<length;l++) { options[l].text = cities_dis[l]; options[l].value = cities[l]; } 
        options[selectedIndex].text = cities_dis[0]; options[selectedIndex].value = cities[0]; 
    }
}

function initEdit(where,where_dis,p_id,c_id,p_value,c_value) {
    var province = document.getElementById(p_id); 
    var city = document.getElementById(c_id);
    var selected=0;
    with(province) { 
        length = where.length; 
        for(var k=0;k<where.length;k++) { 
         options[k].text = where_dis[k].loca; 
         options[k].value = where[k].loca; 
         if(p_value==where[k].loca){
          options[k].selected=true;
          selected=k;
         }
        }       
    } 
    with(city) { 
        cities = (where[selected].locacity).split("|"); 
        cities_dis = (where_dis[selected].locacity).split("|"); 
        length = cities.length; 
        for(var l=0;l<length;l++) { 
         options[l].text = cities_dis[l]; 
         options[l].value = cities[l]; 
         if(c_value==cities[l]){
          options[l].selected=true;
         }
        } 
        //options[selectedIndex].text = cities[0]; options[selectedIndex].value = cities[0]; 
       
    }
} 
function initProvince(where,where_dis,p_id,selected) { 
    var province = document.getElementById(p_id);     
    with(province) { 
        length = where.length; 
        for(var k=0;k<where.length;k++) { 
         options[k].text = where_dis[k].loca; 
         options[k].value = where[k].loca; 
         if(selected.length>0){
          if(selected==where[k].loca){
           options[k].selected=true;
          }
         }
        } 
        options[selectedIndex].text = where_dis[0].loca; 
        options[selectedIndex].value = where[0].loca; 
    }     
} 


</script>	
	

<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => $active,
					'menu_map' => $menu_map));
			?>
			
			
			<?php 
				if($type == "bacnet"){
					echo "<div style='margin-top: 20px;'>";
					echo "<a class='btn btn-warning' style='margin-left: 1%;' href='".site_url("point/translate/new")."'>".lang("point_translate_new")."</a>";
					echo "<a class='btn btn-warning' style='margin-left: 1%;' href='".site_url("point/translate/all")."'>".lang("point_translate_all")."</a>";
					echo "</div>";
				}
			?>
			<div class="span10" style="margin-left: 1%;margin-top: 20px;background-color: rgba(200, 200, 200, 1);-webkit-border-radius: 5px 5px 5px 5px;width: 84%">

				<div>
				
					<?php 
						echo $output->output;
					?>
				</div>

			</div>

		</div>
	</div>
</body>

<?php 
$this->load->view('templates/footer');
?>
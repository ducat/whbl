<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?=$title?></title>
<?php 
foreach($output->css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
		<link href="/assets/jqueryui-bootstrap/css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet">
		<link href="/assets/img/dfk.jpg" rel="shortcut icon" />
		<link href="/assets/img/dfk.jpg" rel="bookmark" />

<?php foreach($output->js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
		<script src="/assets/js/bootstrap.js"></script>
		<script src="/assets/js/unable_flat_chosen.js"></script>
		

<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => $active,
					'menu_map' => $menu_map));
			?>
			
			<div class="span10" style="margin-left: 1%;margin-top: 20px;background-color: rgba(200, 200, 200, 1);-webkit-border-radius: 5px 5px 5px 5px;width: 84%">
				<div>
				
					<?php 
						echo $output->output;
					?>
				</div>

			
			</div>

		</div>
	</div>
</body>

<?php 
$this->load->view('templates/footer');
?>
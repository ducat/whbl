<?php 
$this->load->view('templates/header', 
		array('title' => lang('analysis_chart') ));
?>
	
	<link href="/assets/jqueryui-bootstrap/third-party/jQuery-UI-Date-Range-Picker/css/ui.daterangepicker.css" media="screen" rel="Stylesheet" type="text/css" /> 


<script src="/assets/jqueryui-bootstrap/third-party/jQuery-UI-Date-Range-Picker/js/daterangepicker.jQuery.js"></script>	



 
 
<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'point_chart',
					'menu_map' => $menu_map));
			?>	
			
			<div class="span10" style="margin-left: 1%;margin-top: 20px;min-height: 600px">
				<div class="textbox-holder" style="background-color: whiteSmoke;overflow: visible; min-height: 550px" > 
					<div class="span8" style="margin:15px 50px auto;">					
						<form class="form-horizontal span10" style="" method="post" action="<?=site_url('analysis/single_point_report/' . $info->id)?>">							
							
							<legend><?=lang('point_chart') . "__" . (current_lang() == "en"? $info->english_name:$info->chinese_name)?></legend>

							<div class="control-group">
								<label class="control-label" for="report_type"><?=lang('point_chart_type');?></label>
								<div class="controls">
									<select id="report_type" name="report_type" class="span6" onchange="append_time()">
										<option value="year"><?=lang("point_report_year")?></option>
										<option value="month"><?=lang("point_report_month")?></option>
										<option value="day"><?=lang("point_report_day")?></option>
										<option value="runtime"><?=lang("point_report_runtime")?></option>
			              			</select>
								</div>
							</div>
							
		
							<div class="control-group">
								<label class="control-label" for="time_selector"><?=lang('point_time_range');?></label>
								<div class="controls" id="time_selector">
									<select id="start_year" class="span6" name="start_year"></select><?=lang('year')?>
								</div> 
							</div>
	  						
	  						<div class="form-actions">
								<button type="submit" class="btn btn-primary" id="submit_btn"><?=lang('finish')?></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript" src="/assets/chosen/chosen.jquery.js" ></script>
<script type="text/javascript">	


var myDate = new Date();
var year = myDate.getFullYear(); 
var month = myDate.getMonth() + 1;			
var today = myDate.toTimeString();

$(function(){
	append_time();
	init_sel();
	$("#submit_btn").click(function(){		
		return judge();	
	});	
});

var month_year = <?=json_encode($year_month)?>;

function init_sel(){
	
	$("#start_year").empty();
	$("#start_month").empty();
	$.each(month_year, function(year, months){
		$("#start_year").append("<option value='" + year + "'>" + year + "</option>");
	});
	var year_selected = $("#start_year option:selected").val();
	$.each(month_year[year_selected], function(i, month){						
		$("#start_month").append("<option value='" + month + "'>" + month + "</option>");
	});
	
	$("#start_year").change(function(){	
		$("#start_month").empty();
		$.each(month_year[this.value], function(i, month){	
			$("#start_month").append("<option value='" + month + "'>" + month + "</option>");
		});
	});
}



function append_time(){
	$("#time_selector").empty();
	var type = $("#report_type option:selected").val();
	if(type == "year"){
		$("#time_selector").append("<select id='start_year' class='span6' name='start_year'></select><?=lang('year')?>");
		init_sel();
	}
	else if(type == "month"){
	
		$("#time_selector").append("<select class='span6' id='start_year' name='start_year'></select><?=lang('year')?>");
		$("#time_selector").append("<br>");
		$("#time_selector").append("<select class='span6' id='start_month' name='start_month'></select><?=lang('month')?>");
		init_sel();
	}
	else if(type == "day"){
		$("#time_selector").append("<input type='text' id='from' name='from' value='<?=$start?>' placeholder='<?=lang('click_start_time')?>' readonly/>");
		$("#time_selector").append("<input type='text' id='to' name='to' value='<?=$end?>' placeholder='<?=lang('click_end_time')?>'  readonly/>");
		init_range();
		
	}
	else if(type == "runtime"){
		$("#time_selector").append("<input type='text' id='from' name='from' value='<?=$start?>' placeholder='Click to select start date...'  readonly/>");
		$("#time_selector").append("<input type='text' id='to' name='to' value='<?=$end?>' placeholder='Click to select end date...'  readonly/>");
		init_range();
	}
	
	
}

function init_range() {
    $( "#from" ).datepicker({
        defaultDate: new Date(),
        maxDate: "+0D",
        changeMonth: true,
        dateFormat: 'yy-mm-dd',
        numberOfMonths: 2,
        onClose: function( selectedDate ) {
            $( "#to" ).datepicker( "option", "minDate", selectedDate );
        }
    });
    $( "#to" ).datepicker({
        defaultDate: +1,
        maxDate: "+0D",
        changeMonth: true,
        dateFormat: 'yy-mm-dd',
        numberOfMonths: 2,
        onClose: function( selectedDate ) {
            $( "#from" ).datepicker( "option", "maxDate", selectedDate );
        }
    });
}

function judge(){
	var type = $("#report_type option:selected").val();
	if(type == 'day' || type == 'runtime'){
		var from = $("#from").val();
		var to = $("#to").val();
		if(from == '' || to == ''){
			alert("<?=lang('valid_date')?>");
			return false;
		}			
		else
			return true;
	}
	else
		return true;
}

</script>
</body>


<?php 
$this->load->view('templates/footer');
?>
<?php 
$this->load->view('templates/header', 
		array('title' => lang('analysis') . '__X'));
?>
<body>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => "analysis_view_cop/" . $analysis_id,
					'menu_map' => $menu_map));
			?>			
			
			<div class="tabbable span10" style="margin-left: 1%;overflow: visible;margin-top: 20px;min-height: 600px">
				
				<ul class="nav nav-tabs">  
					<li class="active">
						<a id="a_cop" href="#cop" class="mytab" data-toggle="tab" onclick="drawCOP()"><?=lang('analysis_cop')?></a>
					</li> 
					<li>
						<a id="a_sumW" href="#sumW" class="mytab" data-toggle="tab" onclick="drawW()">KW Usage</a>
					</li>
					<li>
						<a id="a_sumQ" href="#sumQ" class="mytab" data-toggle="tab" onclick="drawQ()"><?=lang('analysis_Q')?></a>
					</li>
				</ul> 
				<div class="tab-content textbox-holder" style="background-color:whiteSmoke;margin-top:-11px;overflow: visible; min-height: 560px"> 
						<div class="tab-pane active" id = 'cop' style="height:480px; margin:15px auto;"></div>
						<div class="tab-pane" id="sumW" style="height:480px; margin:15px auto;"></div>
						<div class="tab-pane" id="sumQ" style="height:480px; margin:15px auto;"></div>
				</div>
			
			</div>
			
		</div>
	</div>
</body>
<script src="/assets/js/highcharts/highstock.js"></script>
<script src="/assets/js/highcharts/modules/exporting.js"></script>
<script type="text/javascript">
$(function() {
	Highcharts.setOptions({     
	    global: {     
	        useUTC: false     
	    }     
	});
	drawCOP();
	$("#a_cop"). bind ('click',function(){
		$("#cop"). css( "min-width",	$("#cop").parent().width() );
		drawCOP();
	});
	$("#a_sumW"). bind ('click',function(){
		$("#sumW"). css( "min-width",	$("#sumW").parent().width() );
		drawW();
	});
	$("#a_sumQ"). bind ('click',function(){
		$("#sumQ"). css( "min-width",	$("#sumQ").parent().width() );
		drawQ();
	});

		
});
function drawW(){
	dataW = <?php echo json_encode($result['W'],JSON_NUMERIC_CHECK)?> ;
	dataSumW = <?php echo json_encode($result['sumW'],JSON_NUMERIC_CHECK)?> ;	
	render(dataW,'sumW', 'P','%');
}
function drawQ(){
	dataQ = <?php echo json_encode($result['Q'],JSON_NUMERIC_CHECK)?> ;
	dataSumQ = <?php echo json_encode($result['sumQ'],JSON_NUMERIC_CHECK)?> ;
	render(dataQ,'sumQ', 'Q','%');
}
function drawCOP(){
	dataCOP = <?php echo json_encode($result['COP'],JSON_NUMERIC_CHECK)?> ;
	render(dataCOP,'cop', 'COP','%');	
}

function render(data, renderTo, name, metric){
	window.chart = new Highcharts.StockChart({
		chart : {
			renderTo : renderTo,
			type: 'spline',
			backgroundColor: 'transparent',
			zoomType: 'x'
		},
        credits : {
            enabled : false
        }, 
		rangeSelector : {
			inputEnabled : false,
			selected : 1
		},
		title : {
			text : ' '
		},	
		exporting : {
			url: '<?=site_url('export/index')?>',
			buttons : {
				printButton : {
					enabled : false
				}
			}
		},		
		yAxis: {
			min:0
		},
		series : [{
			name : name,
			data : data,
			marker : {
				enabled : true,
				radius : 2
			},
			shadow : true,
			tooltip : {
				valueDecimals : 5
			}
		}]
	});	
}
</script>

<?php 
$this->load->view('templates/footer');
?>
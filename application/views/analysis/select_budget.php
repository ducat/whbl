<?php 
$this->load->view('templates/header', 
		array('title' => lang('budget_view') . "__" . lang('analysis')));
?>
	
	<link href="/assets/jqueryui-bootstrap/third-party/jQuery-UI-Date-Range-Picker/css/ui.daterangepicker.css" media="screen" rel="Stylesheet" type="text/css" /> 


<script src="/assets/jqueryui-bootstrap/third-party/jQuery-UI-Date-Range-Picker/js/daterangepicker.jQuery.js"></script>	



 
 
<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'budget_view',
					'menu_map' => $menu_map));
			?>	
			
			<div class="span10" style="margin-left: 1%;margin-top: 20px;min-height: 600px">
				<div class="textbox-holder" style="background-color: whiteSmoke;overflow: visible; min-height: 550px" > 
					<div class="span8" style="margin:15px 50px auto;">					
						<form class="form-horizontal span10" style="" method="post" action="<?=site_url('analysis/budget_show')?>">							
							
							<legend><?=lang('budget_view') ?></legend>

							<div class="control-group">
								<label class="control-label" for="base"><?=lang('budget_select_year');?></label>
								<div class="controls">
									<select id="base" name="base" class="span6">
									<?php 
										foreach ($budget as $val){											
											echo "<option value=". $val['id'] . ">".$val["name"]."</option>";
										}
									?>
			              			</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="compare"><?=lang('budget_compare_year');?></label>
								<div class="controls">
									<select id="compare" name="compare" class="span6">
									<?php 
										foreach ($budget as $val){
											echo "<option value=". $val['id'] . ">".$val["name"]."</option>";
										}
									?>
									</select>
								</div>
							</div>
							
		
	  						
	  						<div class="form-actions">
								<button type="submit" class="btn btn-primary" id="submit_btn"><?=lang('finish')?></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>


<?php 
$this->load->view('templates/footer');
?>
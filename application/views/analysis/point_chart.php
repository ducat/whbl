<?php 
$this->load->view('templates/header', 
		array('title' => lang('point_view') . '__X'));
?>
<body>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'point_chart',
					'menu_map' => $menu_map));
			?>			

			<div class="textbox-holder span10" style="margin-left: 1%;margin-top: 20px;min-height: 600px">
				<div id="point_chart" style="width:98%; height:550px; margin:15px auto;">
				</div>
			</div>
		</div>
	</div>
</body>

<script src="/assets/js/highcharts/highstock.js" type="text/javascript"></script>
<script src="/assets/js/highcharts/modules/exporting.js" type="text/javascript"></script>   

<script type="text/javascript">
$(".language").empty();
var start_orig = <?=$start?>,
	end_orig = <?=$end?>,
	range_orig = end_orig - start_orig;


$(function() {
	Highcharts.setOptions({     
	    global: {     
	        useUTC: false     
	    }     
	});

    $.getJSON("<?=$url?>?id=<?=$point['id']?>&start=<?=$start?>&end=<?=$end?>&callback=?", function(data) {
	//$.getJSON('http://www.highcharts.com/samples/data/from-sql.php?callback=?', function(data) {  
        // create the chart
        window.chart = new Highcharts.StockChart({
            chart : {
                renderTo : 'point_chart',
                type: 'spline',
                zoomType: 'x'
            },

            credits : {
                enabled : false
            }, 
			rangeSelector : {
				inputEnabled : false,
				selected : 5
			},
            navigator : {
                adaptToUpdatedData: false,
                series : {
                    data : data
                }
            },
            
			title : {
				text : '<?=(current_lang()=="en"?$point['english_name']:$point['chinese_name']) . ' ' . lang("point_report_runtime") ?>'
			},	

			subtitle: {
				text : '<?=$point['from'] ." ". lang('to') ." ". $point['to'] ?>'
			},
			exporting : {
				url: '<?=site_url('export/index')?>',
				buttons : {
					printButton : {
						enabled : false
					}
				}
			},
                                  
            xAxis : {
                events : {
                    afterSetExtremes : afterSetExtremes
                }
            //,
            //	minRange: 3600 * 1000
            },

			yAxis: {
				title : {
					text: "<?= (current_lang()=="en"?$point['english_name']:$point['chinese_name']) . '(' . $point['unit'] . ')'?>"
				},
				minorTickInterval: 'auto',
				lineColor: '#000',
				lineWidth: 1,
				tickWidth: 1,
				tickColor: '#000'
	            
	        },	

            series : [{
				name : '<?=(current_lang()=="en"?$point['english_name']:$point['chinese_name'])?>',
				data : data,
				marker : {
					enabled : true,
					radius : 2
				},
				shadow : true,
				tooltip : {
					valueDecimals : 2
				}
			}]
        });
    });
});


/**
 * Load new data depending on the selected min and max
 */
function afterSetExtremes(e) {

	if(range_orig <  7 * 24 * 3600 * 1000){
	}
	else if(range_orig <  90 * 24 * 3600 * 1000){

	    var url,
	        currentExtremes = this.getExtremes(),
	        range = e.max - e.min;
	    if(range < 7 * 24 * 3600 * 1000){
	    	refresh(e.min, e.max);
		}
	    else if(range < 90 * 24 * 3600 * 1000){
	    	refresh(e.min, e.max);
		}
	    else{
	    	refresh(e.min, e.max);
		}
	}
	else {
	    var url,
	        currentExtremes = this.getExtremes(),
	        range = e.max - e.min;
	    if(range < 7 * 24 * 3600 * 1000){
	    	refresh(e.min, e.max);
		}
	    else if(range < 90 * 24 * 3600 * 1000){
	    	refresh(e.min, e.max);
		}
	    else{
			refresh(e.min, e.max);
		}		
	}    
}

function refresh(min, max){
	chart.showLoading('Loading data from server...');
    $.getJSON("<?=$url?>?id=<?=$point['id']?>&start="+ Math.round(min) +
            "&end="+ Math.round(max) +"&callback=?", function(data) {		        
        chart.series[0].setData(data);
        chart.hideLoading();
    });
}
</script>

<?php 
$this->load->view('templates/footer');
?>
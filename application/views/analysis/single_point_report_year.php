<?php 
$this->load->view('templates/header', 
		array('title' => lang('point_view')));
?>
<body>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'point_chart',
					'menu_map' => $menu_map));
			?>			
			
			<div class="textbox-holder span10" style="margin-left: 1%;margin-top: 20px;min-height: 600px">
				<div id="point_chart" style="width:98%; height:550px; margin:15px auto;">
				</div>
			</div>
		</div>
	</div>
</body>

<script src="/assets/js/highcharts/highstock.src.js" type="text/javascript"></script>
<script src="/assets/js/highcharts/highcharts-more.js" type="text/javascript"></script>
<script src="/assets/js/highcharts/modules/exporting.js" type="text/javascript"></script>   

<script type="text/javascript">

$(".language").remove();
$(function() {
	Highcharts.setOptions({     
	    global: {     
	        useUTC: false     
	    }     
	});
	$('#example_length').append("<a id='export' href='<?=site_url('export_csv/export/point')."/".$point['id'];?>' class='btn'><?=lang('export')?></a>");
		
	<?php 
	$max = array();
	$min = array();
	$avg = array();
	$sum = array();
	$day = array();
	foreach ($point['data'] as $row){
	
			array_push($day, date("M",strtotime($row->date)));
			array_push($max, $row->max);
			array_push($min, $row->min);
			array_push($avg, $row->avg);
			array_push($sum, $row->sum);
	}
	?>
	
	day = <?php echo json_encode($day,JSON_NUMERIC_CHECK)?> ;

	<?php 
		if ($point["type_large"] == "binary" || $point["type_large"] == "accumulated"){
			echo "sum = " . json_encode($sum,JSON_NUMERIC_CHECK) . ";";
		}
		else {
			echo "min = " . json_encode($min,JSON_NUMERIC_CHECK) . ";";
			echo "max = " . json_encode($max,JSON_NUMERIC_CHECK) . ";";			
			echo "avg = " . json_encode($avg,JSON_NUMERIC_CHECK) . ";";
		}

	?>

	window.chart = new Highcharts.Chart({
			chart : {
				renderTo : 'point_chart',
				 type: 'column',
				zoomType: 'x'
			},
            credits : {
                enabled : false
            }, 
			rangeSelector : {
				inputEnabled : false,
				selected : 5
			},
			title : {
				text : '<?=(current_lang()=="en"?$point['english_name']:$point['chinese_name']) . ' ' . $point['year'] . lang("point_report_year") ?>'
			},	
			exporting : {
				url: '<?=site_url('export/index')?>',
				buttons : {
					printButton : {
						enabled : false
					}
				}
			},	
			xAxis: {
                categories: day
            },				
			yAxis: {
				title : {
					text: "<?php 
						if($point["type_large"] == "binary"){
							echo (current_lang()=="en"?$point['english_name']:$point['chinese_name']) . '(' . lang("hour") . ')';
						}
						else{
							echo (current_lang()=="en"?$point['english_name']:$point['chinese_name']) . '(' . $point['unit'] . ')';
						}						
					?>"
				},
				minorTickInterval: 'auto',
				lineColor: '#000',
				lineWidth: 1,
				tickWidth: 1,
				tickColor: '#000'
	            
	        },	
			series : [
			<?php 
				if($point["type_large"] == "binary" || $point["type_large"] == "accumulated"){			
			?>
			{
				name : '<?=lang('report_sum')?>',
				data : sum,
				tooltip : {
					valueDecimals : 2
				}
			}
			<?php }else{?>
			{
				name : '<?=lang('report_max')?>',
				data : max,
				tooltip : {
					valueDecimals : 2
				}
			},{
				name : '<?=lang('report_avg')?>',
				data : avg,
				tooltip : {
					valueDecimals : 2
				}
			},{
				name : '<?=lang('report_min')?>',
				data : min,
				tooltip : {
					valueDecimals : 2
				}
			}
			<?php }?>
			]
		});
	});
</script>

<?php 
$this->load->view('templates/footer');
?>
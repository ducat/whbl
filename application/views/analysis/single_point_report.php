<?php 
$this->load->view('templates/header', 
		array('title' => lang('point_view') . '__X'));
?>
<body>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'point_view',
					'menu_map' => $menu_map));
			?>			
			
			<div class="tabbable span10" style="margin-left: 1%;margin-top: 20px;min-height: 600px">
				<ul class="nav nav-tabs">  
					<li class="active">
						<a href="#point_chart" class="mytab" data-toggle="tab"><?=lang('analysis_chart')?></a>
					</li> 
					<li>
						<a href="#point_table" class="mytab" data-toggle="tab"><?=lang('analysis_table')?></a>
					</li>
				</ul> 
				<div class="tab-content textbox-holder" style="overflow: visible; min-height: 550px"> 
					<div class="tab-pane" id = 'point_table'  style="width:98%; height:480px; margin:15px auto;">
						
						<table class="table table-striped table-bordered" id="example">	
							<thead>
								<tr class="mythread">
									<th><?=lang('point_display_name')?></th>
									<th><?=lang('report_timestamp')?></th>
									<th><?=lang('report_value')?></th>
								</tr>
							</thead>	
							<tbody>
							<?php 
							$i = 0;
							foreach ($point['data'] as $row){
							if($i % 2 == 0)
								echo '	<tr class="evenrow">';	
							else
								echo '	<tr class="oddrow">';
							
								echo '		<td>' . $point['name'] . '</td>';
								echo '		<td>' . $row->timestamp . '</td>';
								echo '		<td>' . $row->value . '</td>';
								echo '	</tr>';
							}
							?>
							</tbody>
						</table>			
					</div>
					<div class="tab-pane active" id="point_chart" style="width:98%; height:480px; margin:15px auto;">
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script src="/assets/js/highcharts/highstock.js" type="text/javascript"></script>
<script src="/assets/js/highcharts/modules/exporting.js" type="text/javascript"></script>   

<script type="text/javascript">
/* Table initialisation */
 
 
$(".language").empty(); 
 

 
$(document).ready(function() {
	$('#example').dataTable( {
		"sDom": "<' '<'span4'l><'span8'f>r>t<' '<'span4'i><'span8'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
				"sLengthMenu": "<?=lang('per_page')?> _MENU_ <?=lang('record')?>",
				"sSearch": "<?=lang('search')?>:",
				"oPaginate":{
					"sPrevious": "<?=lang('previous')?>",
					"sNext": "<?=lang('next')?>",
				},	
				"sEmptyTable": "<?=lang('no_data')?>",
				"sZeroRecords": "<?=lang('no_match')?>",
				"sInfoEmpty": "<?=lang('show_empty')?>",
				"sInfoFiltered": "(<?=lang('total')?> _MAX_ <?=lang('record')?>)",			
				"sInfo": "<?=lang('show')?> _START_ - _END_ / <?=lang('total')?> _TOTAL_ <?=lang('record')?>"
		}
	} );
} );
$(function() {
	Highcharts.setOptions({     
	    global: {     
	        useUTC: false     
	    }     
	});
	$('#example_length').append("<a id='export' href='<?=site_url('export_csv/export/point')."/".$point['id'];?>' class='btn'><?=lang('export')?></a>");
		
	<?php 
	$data = array();
	foreach ($point['data'] as $row){
			$pair = array(strtotime($row->timestamp) . '000', $row->value);
			array_push($data, $pair);
	}
	?>
	point = <?php echo json_encode($data,JSON_NUMERIC_CHECK)?> ;

	window.chart = new Highcharts.StockChart({
			chart : {
				renderTo : 'point_chart',
				zoomType: 'x'
			},
            credits : {
                enabled : false
            }, 
			rangeSelector : {
				inputEnabled : false,
				selected : 1
			},
			title : {
				text : ' '
			},	
			exporting : {
				url: '<?=site_url('export/index')?>',
				buttons : {
					printButton : {
						enabled : false
					}
				}
			},					
			yAxis: {
				title : {
					text: "<?= $point['name'] . '(' . $point['unit'] . ')'?>"
				},
				minorTickInterval: 'auto',
				lineColor: '#000',
				lineWidth: 1,
				tickWidth: 1,
				tickColor: '#000'
	            
	        },	
			series : [{
				name : '<?= $point['name']?>',
				data : point,
				marker : {
					enabled : true,
					radius : 2
				},
				shadow : true,
				tooltip : {
					valueDecimals : 2
				}
			}]
		});
	});
</script>

<?php 
$this->load->view('templates/footer');
?>
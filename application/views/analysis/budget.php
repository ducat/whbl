<?php 
$this->load->view('templates/header', 
		array('title' => lang('budget_view') . "__" . lang('analysis')));
?>
<body>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'budget_view',
					'menu_map' => $menu_map));
			?>
			
			<div class="tabbable span10" style="margin-left: 1%;margin-top: 20px;min-height: 600px">
				<ul class="nav nav-tabs">  
					<li class="active">
						<a id="a_chart" href="#budget_chart" class="mytab" data-toggle="tab"><?=lang('budget_chart')?></a>
					</li> 
					<li>
						<a id="a_table" href="#budget_table" class="mytab" data-toggle="tab"><?=lang('budget_table')?></a>
					</li>
				</ul> 
				<div class="tab-content textbox-holder" style="overflow: visible; min-height: 550px"> 
					<div class="tab-pane" id = 'budget_table' style="width:98%;height:480px; margin:15px auto;">
						
						<table class="table table-striped table-bordered" id="example">	
							<thead>
								<tr class="mythread">
									<th><?=lang('budget_month')?></th>
									<th><?=$result['info']['base']['name']?></th>
									<th><?=$result['info']['compare']['name']?></th>
									<th><?=lang('delta')?></th>
								</tr>
							</thead>	
							<tbody>
							<?php 
							
							for($i = 0; $i <12; $i++){
								if($i % 2 ==0)
									echo '	<tr class="evenrow">';	
								else
									echo '	<tr class="oddrow">';
								echo '		<td>' . ($i + 1) . '</td>';
								echo '		<td>' . $result['data']['base'][$i] . '</td>';
								echo '		<td>' . $result['data']['compare'][$i] . '</td>';
								echo '		<td>' . ($result['data']['base'][$i] - $result['data']['compare'][$i]) . '</td>';
								echo '	</tr>'; 
							}

							?>
							</tbody>
						</table>	
								
					</div>
					<div class="tab-pane active" id="budget_chart" style="width:98%;height:480px; margin:15px auto;"></div>
				</div>
			</div>

		</div>
	</div>
</body>
<script src="/assets/js/highcharts/highcharts.js"></script>
<script src="/assets/js/highcharts/modules/exporting.js"></script>    
<script type="text/javascript">
$(".language").remove();
$(document).ready(function() {
	
	$('#example').dataTable( {
		"sDom": "<' '<'span4'l><'span8'f>r>t<' '<'span4'i><'span8'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "<?=lang('per_page')?> _MENU_ <?=lang('record')?>",
			"sSearch": "<?=lang('search')?>:",
			"oPaginate":{
				"sPrevious": "<?=lang('previous')?>",
				"sNext": "<?=lang('next')?>",
			},	
			"sEmptyTable": "<?=lang('no_data')?>",
			"sZeroRecords": "<?=lang('no_match')?>",
			"sInfoEmpty": "<?=lang('show_empty')?>",
			"sInfoFiltered": "(<?=lang('total')?> _MAX_ <?=lang('record')?>)",			
			"sInfo": "<?=lang('show')?> _START_ - _END_ / <?=lang('total')?> _TOTAL_ <?=lang('record')?>"
		}
	} );

	
} );
							
	$(function () {
	    $(document).ready(function() {
	    	$('#example_length').append("<a id='export' href='<?=site_url('export_csv/export/budget' . '/' . $result['info']['base']['year'] . '/' . $result['info']['compare']['year']);?>' class='btn'><?=lang('export')?></a>");
		    
	    	Highcharts.setOptions({     
	    	    global: {     
	    	        useUTC: false     
	    	    }     
	    	});
		    
	        chart = new Highcharts.Chart({
	    
	            chart: {
	                renderTo: 'budget_chart',
	                type: 'column'
	            },
	    
	            title: {
	                text: '<?=lang('budget_view')?>'
	            },

				credits:{
					enabled:false
				},

				exporting : {
					url: '<?=site_url('export/index')?>',
					buttons : {
						printButton : {
							enabled : false
						}
					}
				},	
	            xAxis: {
	                categories: ['<?=lang('jan')?>', 
		         	             '<?=lang('feb')?>',
			         	         '<?=lang('mar')?>', 
			         	         '<?=lang('apr')?>',
				         	     '<?=lang('may')?>', 
				         	     '<?=lang('jun')?>',
					         	 '<?=lang('jul')?>', 
					         	 '<?=lang('aug')?>',
						         '<?=lang('sep')?>', 
						         '<?=lang('oct')?>',
						         '<?=lang('nov')?>',
						         '<?=lang('dec')?>'
		         	            ]
	            },
	    
	            yAxis: {
	                allowDecimals: false,
	                min: 0,
	                title: {
	                    text: "<?=lang('budget') . '(' . $result['info']['base']['unit'] . ')'?>"
	                }
	            },
				legend:{
				    enabled:true
				},
	    
	            tooltip: {
	                formatter: function() {
	                    return '<b>'+ this.x +'</b><br/>'+
	                        this.series.name +': '+ this.y ;
	                }
	            },
	            series: [{
	                name: "<?=$result['info']['base']['name'] . '(' . $result['info']['base']['unit'] . ')'?>",
	                data: <?=json_encode($result['data']['base'],JSON_NUMERIC_CHECK)?>
	            }, {
	            	name: "<?=$result['info']['compare']['name'] . '(' .$result['info']['compare']['unit'] . ')'?>",
		            data: <?=json_encode($result['data']['compare'],JSON_NUMERIC_CHECK)?>
	            }]
	        });
	    });
	    
	});

</script>

<?php 
$this->load->view('templates/footer');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?=lang("point_management")?></title>
		

			<link type="text/css" rel="stylesheet" href="/assets/grocery_crud/css/jquery_plugins/chosen/chosen.css" />
	<link type="text/css" rel="stylesheet" href="/assets/grocery_crud/css/ui/simple/jquery-ui-1.8.23.custom.css" />
	<link type="text/css" rel="stylesheet" href="/assets/grocery_crud/themes/flexigrid/css/flexigrid.css" />
		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
<!-- 		<link href="/assets/jqueryui-bootstrap/css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet"> -->
		

	<script src="/assets/grocery_crud/js/jquery-1.8.1.min.js"></script>
	<script src="/assets/grocery_crud/js/jquery_plugins/jquery.chosen.min.js"></script>
	<script src="/assets/grocery_crud/js/jquery_plugins/config/jquery.chosen.config.js"></script>
	<script src="/assets/grocery_crud/js/jquery_plugins/ui/jquery-ui-1.8.23.custom.min.js"></script>
	<script src="/assets/grocery_crud/js/jquery_plugins/ui/i18n/datepicker/jquery.ui.datepicker-zh-cn.js"></script>
	<script src="/assets/grocery_crud/themes/flexigrid/js/jquery.form.js"></script>
	<script src="/assets/grocery_crud/themes/flexigrid/js/flexigrid-edit.js"></script>
		<script src="/assets/js/bootstrap.js"></script>

	</head>
<body>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'point_virtual',
					'menu_map' => $menu_map));
			?>			
			

			
			<div class="container span10 textbox-holder" style="background-color: whiteSmoke;margin: 20px 1% auto;min-height: 600px">
				<div class="span8" style="margin:15px 50px auto;">
			
					<form class="form-horizontal span10" style="" method="post" action="<?=site_url('calc/show_calc/'.$info->id)?>">
					
					<legend><?=lang('point_select_time_and_candidate') . "__" . $info->display_name ?></legend>
					<label class="span12" style="margin-left: 0"><?=lang('start_month');?></label>

					<div class="control-group">
						<label class="control-label" for="report_type"><?=lang('point_time_start')?></label>
						<div class="controls">
							<input id='field-start_time' name='start_time' type='text' value='2008-01-01' maxlength='10' class='datepicker-input span5' readonly/> 
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" for="report_type"><?=lang('point_time_end')?></label>
						<div class="controls">
							<input id='field-end_time' name='end_time' type='text' value='<?=date("Y-m-d",strtotime("-1 day"))?>' maxlength='10' class='datepicker-input span5' readonly/> 
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" for="report_type"><?=lang('point_select')?></label>
						<div class="controls">
							<select id="point-select" data-placeholder="<?=lang('point_search')?>" name="point[]" class="chosen-select span10" multiple tabindex="4">
								<?php 
									foreach ($points as $row){
										echo "<option value='".$row->id."'>".$row->display_name."</option>";
									}
								?>
              				</select>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" for="report_type"><?=lang('point_base_select')?></label>
						<div class="controls">
							<select id="base-select" data-placeholder="<?=lang('base_search')?>" name="base[]" class="chosen-select span10" multiple tabindex="4">
								<option value></option>
								<?php 
									foreach ($bases as $row){
										echo "<option value='".$row->id."'>".$row->name."</option>";
									}
								?>
              				</select>
						</div>
					</div>
              			
              		<div class="form-actions">
						<button type="submit" class="btn btn-primary" id="submit_btn"><?=lang('next_step')?></button>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="/assets/chosen/chosen.jquery.js" ></script>
<script type="text/javascript">

$(function(){
	$('.datepicker-input').datepicker({
			dateFormat: 'yy-mm-dd',
			showButtonPanel: false,
			changeMonth: true,
			changeYear: true,
			maxDate: "+1D"
	});
	
	$("#submit_btn").click(function(){		
		return judge();	
	});	
	
});



function judge(){
	var start = $("#field-start_time").val();
	var end = $("#field-end_time").val();
	var point = $("#point-select option:selected").length;
	var base = $("#base-select option:selected").length;
	
	if(start > end){
		alert("<?=lang('point_valid_time')?>");
		return false;
	}			
	else{
		if(point==0){
			alert("<?=lang('point_point_not_empty')?>");
			return false;
		}
		else{
			return true;
		}

	}
}
</script>

<?php 
$this->load->view('templates/footer');
?>
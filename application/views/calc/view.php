<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<link href="/assets/css/bootstrap.css" rel="stylesheet">
<link href="/assets/css/style.css" rel="stylesheet">

<script src="/assets/js/jquery-1.8.1.min.js"></script>
<script src="/assets/js/bootstrap.js"></script>
<script src="/assets/js/jquery-ui-1.8.23.custom.min.js"></script>	


</head>
<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
<div class="container-fluid">
	<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'point_virtual',
					'menu_map' => $menu_map));
			?>

	<div class = "span10" style="margin-left: 1%;margin-top: 20px;min-height: 600px;background-color: white;-webkit-border-radius: 5px 5px 5px 5px;">
		
		<h4><center><?php echo $info->display_name . "__" . lang('point_expression')?></center></h4>
		<div id="compGroupA" class="span6 offset3" style="overflow:auto;margin-top: 5px;height: 150px;border: 1px solid;">
			<ol class="nav nav-list"> 
				<?php 
					foreach($dot as $key => $val) {
						echo "<li><a class='span12' val='$key'>$val</a></li>";
					}
					foreach($base as $row) {
						echo "<li><a class='span12' val='$row->value'>$row->name</a></li>";
					}
				?>
			</ol> 
		</div>
		<div class="span11" style="margin:20px 40px auto;border: 1px solid;">
			<ol class="span11" id="explist"></ol> 
		</div>
		
		<div id="compGroupB" class="offset2 span8" style="margin-top: 40px;min-height: 400px;">			
			<form class="" method="post" action="<?=site_url('Calc/submit')?>">
				<div class="span4 offset2">
					<p>
						<input type="button" value="7" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="8" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="9" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						</p>
					<p>
						<input type="button" value="4" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="5" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="6" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
					</p>
					<p>
						<input type="button" value="1" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="2" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="3" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						</p>	
					<p>
						<input type="button" value="0" class="btn btn-large btn-primary span2" style="width:83px" href="one">
						<input type="button" value="." class="btn btn-large btn-primary span1" style="width:39px;" href="one">
					</p>
				
				</div>	
				<div class="span4">
					<p>
<!-- 					<input type="button" value="∫" class="btn btn-large btn-primary span1" href="one">	 -->
						<input type="button" value="/" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="*" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="sin" class="btn btn-large btn-primary span1" style="width:39px;padding-left:8px;" href="one">
					</p>
					<p>
						<input type="button" value="-" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="+" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="cos" class="btn btn-large btn-primary span1" style="width:39px;padding-left:8px;" href="one">
					</p>
					<p>
						<input type="button" value="^" class="btn btn-large btn-primary span1" style="width:39px;" href="one" title="乘方运算  如x^y">	
						<input type="button" value="(" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value=")" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						
					</p>
					<p>
						<input type="button" value="√" class="btn btn-large btn-primary span1" style="width:39px;" href="one">
						<input type="button" value="R" class="btn btn-large btn-primary span2" style="width:83px" id="delete">
					</p>
				
						
<!-- 						<input type="button" value=">" class="btn btn-large btn-primary span1" href="one"> -->
<!-- 						<input type="button" value="<" class="btn btn-large btn-primary span1" href="one"> -->
<!-- 						<input type="button" value="and" class="btn btn-large btn-primary span1" href="one"> -->
<!-- 						<input type="button" value="or" class="btn btn-large btn-primary span1" href="one"> -->
								
				</div>	
				<div class='span10'>	
					<p class="">	
			
						<button type="submit" class="btn btn-large btn-primary span2 pull-right" style="width:75px;margin-left: 10px" id="submitid"><?=lang('calc_submit')?></button>
						<button type="button" value='<?=lang('calc_chongzhi')?>' class="btn btn-large btn-primary span2 pull-right" style="width:75px" id="chongzhi"><?=lang('calc_chongzhi')?></button>
					</p>
										
				</div>
			</form>	
		</div>   
		
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-body">
				<p><?=lang("calc_waiting")?></p>
			</div>
		</div>

		
		
	</div>  
</div>

</div>

<script type="text/javascript">


$(document).ready(function() { 

$("form div input:text").addClass("input");
var str="";
var submitstr="";
$("form div :button[href]").click(function(){
	
	str=$(this).val();
	//submitstr=submitstr+$(this).val();	
	if ($(this).val()=="sin"||$(this).val()=="cos"||$(this).val()=="and"||$(this).val()=="or"){
		str=str+"(";
		submitstr=str;
		
	}
	
	else if($(this).val()=="√"){
		str=str+"(";
		submitstr="_root(";
		
		//echo(submitstr);
	}
	else if($(this).val()=="∫"){
		str=str+"(";
		submitstr="_sum(";
	}
	else if($(this).val()==">"){
		str=str+"(";
		submitstr="_big(";
	}
	else if($(this).val()=="<"){
		str=str+"(";
		submitstr="_small(";
	}
	else{
		submitstr=$(this).val();	
	}
	
	var width = "";
	if($(this).val()=="sin"||$(this).val()=="cos"){
		width = "40px";
	}
		
	else{
		width = "10px"; 
	}
		
		
	$("#explist").append("<li class='span1'><a class='span12 btn disabled' name='"+submitstr+"'>"+str+"</a></li>");
	//alert(str);
	$("#exp").val(str);
	submitstr="";
	
});

$("#delete").click(function(){	
	//str=str.substring(0,str.length-1);
	//submitstr=submitstr.substring(0,str.length-1);
	//$("#exp").val(str);
	$("#explist li:last").detach();
	
});
 
$("form div :button#chongzhi").click(function(){	
	str="";
	submitstr="";
	$("#explist").empty();
	$("#exp").val(str);
});



$("#submitid").click(function(){

	//
	//str=str+"#";
	//submitstr=submitstr+"#";
	str="";
	submitstr="";
    var i=$("#explist li ").last().index();

    for(var j=0;j<=i;j++){
    	submitstr += $("#explist li :eq("+j+") a").attr("name");
    	//alert(submitstr);
    	str += $("#explist li :eq("+j+") a").text();
    }
	str += "#";
	submitstr += "#";
	

	if(str=="#"){
		str="";
		submitstr=str;
		alert("<?=lang('calc_experror')?>");
		
		return false;
	}
//	$("#exp").val(submitstr);



	$('#myModal').modal({
		  keyboard: false,
		  backdrop: 'static'
	});
	$('#myModal').modal('show');
	$.post("<?=site_url('Calc/submit')?>", {'exp' : submitstr,'exp_display': str, 'id' :<?=$id?>, 'start' :'<?=$start?>', 'end' :'<?=$end?>' },
		   function(data) {
			    //alert(data); 
			    if(data == "success"){			    	
				    alert("<?=lang("calc_success")?>");
				    $('#myModal').modal('hide');
				    window.location.href="<?=site_url('manage/point_virtual')?>";
				}
			    else if(data == "error"){
			    	$('#myModal').modal('hide');
			    	alert("<?=lang("calc_error")?>");			    	
				}
			    else if(data == "error_batch"){
			    	$('#myModal').modal('hide');
			    	alert("<?=lang("calc_error_batch")?>");			    	 
				}
			    else
			    {
			    	$('#myModal').modal('hide');
			    	alert("<?=lang("calc_error_compute")?>");	
				}
			    	 
				str="";
				submitstr=str;
				$("#exp").val(str);
		   	});

	//zzl 
	return false;
	
});

//$( "#selectable" ).selectable();
//$( "#selectable" ).selectable({
//	   create: function(event, ui) { alert("abc"); },
//	   selected: function () {
//			alert($(this).attr('id'));
//		}
//	});
//$( "#selectable" ).bind( "selectablecreate", function(event, ui) {
//	alert("abc");
//	});
/*$("#selectable").click(function(){
	str=str+$(this).text();
	submitstr=str;
	$("exp").val(str);
	alert($(this).text());
});*/
$('#compGroupA ol li a').each(function() {
	$(this).click(function() {

		str += $(this).html();
		submitstr += $(this).attr('val');

		$("#explist").append("<li class='span3'><a class='span12 btn disabled' name='"+$(this).attr('val')+"'>"+$(this).html()+"</a></li>");
		
		//alert(str);
		$('#exp').val(str);
	});
})
});
</script> 
</body>
<?php 
$this->load->view('templates/footer');
?>
 
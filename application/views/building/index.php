<?php 
$this->load->view('templates/header', 
		array('title' =>  $active));
?>
<body>


<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => $active,
					'menu_map' => $menu_map));
			?>
			
			<div class = "span10" id = "cop_page" style="margin-top: 20px;min-height: 400px;background-color: white;padding:10px;">
				<h1 style="text-align: center">主界面</h1>
				<?php $i = 0;?>
				<?php foreach ($link as $k => $v):?>
				<?php $i++; ?>
				<?php if($i%3==1):?>
				<div class="row-fluid" style="margin:30px 0;">
				<?php endif;?>
				<a href="<?= $v ?>" class="btn btn-default span4 btn-large"><?= $k ?></a>
				<?php if($i%3==0):?>
				</div>
				<?php endif;?>
				<?php endforeach;?>
			</div>
		</div>
	</div>
</body>
<?php 
$this->load->view('templates/footer');
?>
<?php 
$this->load->view('templates/header', 
		array('title' =>  $active));
?>
<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => $active,
					'menu_map' => $menu_map));
			?>
			<div class = "span10" id = "cop_page" style="margin-top: 20px;min-height: 570px;background-color: white;padding:20px;">
				<div id="grid">
				<table class="table table-condensed">
				<thead>
					<tr>
					<?php foreach ($field as $v):?>
                        <?php if($v == 'value' && (strpos($active, 'air_condition')!==false || strpos($active, 'new_wind')!==false ) ):?>
                        <th>回风温度</th>
                        <?php else:?>
						<th><?= lang($v) ?></th>
                        <?php endif?>
					<?php endforeach ?>
					</tr>
				</thead>
				<tbody>
				<?php foreach($data as $v):?>
					<tr>
					<?php foreach($field as $f):?>
                        <?php if($f == 'value'):?>
                            <td><?= $v['value']?> <?= $v['unit']?></td>
                        <?php else:?>
                            <td><?= $v[$f]?></td>
                        <?php endif?>
					<?php endforeach ?>
					</tr>
				<?php endforeach ?>
				</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</body>
<?php 
$this->load->view('templates/footer');
?>
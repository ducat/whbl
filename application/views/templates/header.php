<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?=$title?></title>
		<link href="/assets/img/dfk.jpg" rel="shortcut icon" />
		<link href="/assets/img/dfk.jpg" rel="bookmark" />
		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
		<link href="/assets/css/DT_bootstrap/DT_bootstrap.css" rel="stylesheet">
		<link href="/assets/jqueryui-bootstrap/css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet">
		
		<script src="/assets/js/jquery-1.8.1.min.js"></script>
		<script src="/assets/js/bootstrap.js"></script>
		<script src="/assets/js/jquery-ui-1.8.16.custom.min.js"></script>		
		<script src="/assets/js/DT_bootstrap/jquery.dataTables.js"></script>
		<script src="/assets/js/DT_bootstrap/DT_bootstrap.js"></script>
		<script src="/assets/js/jplayer/jquery.jplayer.min.js"></script>
		
		<link rel="stylesheet" type="text/css" href="/assets/easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" type="text/css" href="/assets/easyui/themes/icon.css"> 
		<script src="/assets/easyui/jquery.easyui.min.js"></script>
	</head>
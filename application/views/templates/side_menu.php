
<div class="span2" style="margin-top: 20px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<script src="/assets/js/side_menu.js" ></script>
	<ul class="nav nav-list" id="accordion1" style="font-size: 10px;">
		<?php
		$colors_map = array('btn-custom', 'btn-custom', 'btn-custom', 'btn-custom', 'btn-custom', 'btn-custom');
		$i = -1;
		foreach ($menu_map as $key => $items) {
			$is_in = array_key_exists($active_id, $items);
			$i++;
		?>
			<li class="accordion-group">
				<div class="accordion-heading">
					<a id="head-<?=$key?>" class="btn btn-custom accordion-toggle<?=$is_in ? ' ' : ' collapsed'?>" data-toggle="collapse" data-parent="#accordion1" href="#collapse-<?=$key?>">
						<i class="icon-folder icon-white"></i>
						<?=lang($key)?>
					</a>
				</div>
				<div style= "overflow:auto" id="collapse-<?=$key?>" class="accordion-body collapse<?=$is_in ? ' in' : ''?>">
					<ul class="accordion-inner nav nav-list side-menu-inner"  >
					<?php
						foreach ($items as $id => $name) {
							$class = $active_id == $id ? ' class="active"' : '';
					?>
						<li<?=$class?>><a href="<?=site_url($key . '/' . $id)?>"><?=$name?></a>
					<?php
						}
					?>
						<!-- <li><a href="<?=site_url($key . '/add')?>">定制</a></li> -->
					</ul>
				</div>
			</li>
		<?php } ?>
	</ul>
</div>
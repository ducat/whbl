<div class="body">
<div class="navbar navbar-inverse navbar-fixed-top" >
	<div class="navbar-inner">
		<div class="container-fluid">

			<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span>
			</a>
			
			<a href="<?=site_url("/" . current_lang())?>" class="brand" style="padding: 5px 0px 5px;"><img class='img-rounded' src='/assets/img/Trane-icon.png' style="height:35px;"/>&nbsp;&nbsp;<?=lang('subtitle')?></a>
		
			<div class="nav-collapse">
				<script type="text/javascript">
					function judge_alarm() {
						$.get("<?=site_url("alarm/judge_alarm")?>", function(result){
							if(result == 'true'){
								$('#alarm-status').css('background', 'red');
								$("#alert").jPlayer("play");
							}
						    else if(result == 'false'){
						    	$('#alarm-status').css('background', '#5bb75b');
								$("#alert").jPlayer("stop");
							}
						});
					}
					
					$(document).ready(function(){
						$("#alert").jPlayer({
							ready: function () {
								$(this).jPlayer("setMedia", {
									wav: "/assets/audio/alarm.wav"
								}).jPlayer("stop");
							},
							ended: function (event) {
								$(this).jPlayer("play");
							},
							swfPath: "/assets/js/jplayer",
							supplied: "wav"
						});

						judge_alarm();
						setInterval(judge_alarm,5*60*1000);
					});		
				</script>
				<ul class="nav pull-right">
					<li>
                		<a id='alarm-status' style='color: #fff; background: #5bb75b' href="<?=site_url('alarm/log')?>"><?=lang('alarm_status')?></a>
              		</li>
				</ul>
				<ul class="nav pull-right">
					<li>
						<a  style="color: #fff;"><?=mdate(lang('date_format_string'), time())?></a>
					</li>
				</ul>
				
				<ul class="nav pull-right">
					<li class="dropdown profile">
						<a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown"  style="color: #fff;">
							<?=lang('hello') . lang('comma') . 
							$user->username?>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu" role="menu">
							
							<li><a href="<?=site_url('user/logout')?>"><?=lang('log_out')?></a></li>
						</ul>
					</li>
					<li class="divider-vertical"></li>
					
	 				<li class="dropdown language">
						<a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown"   style="color: #fff;">
							<?=lang('language') . lang('colon') . lang($this->lang->lang())?>
							<b class="caret"></b>
						</a>
 
						<ul class="dropdown-menu" role="menu">
							<li><?=anchor($this->lang->switch_uri('cn'), lang('cn'))?></li>
							<li><?=anchor($this->lang->switch_uri('en'), lang('en'))?></li>
						</ul>					
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div id="alert" class="jp-jplayer" display="none"></div>
<div style="height: 40px"></div>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?=$title?></title>
	<style type='text/css'>
		body {
			font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
			margin: 0;
		}
		
		h1 {
			margin: 0;
			padding: 0.5em;
		}
		
		p.description {
			font-size: 0.8em;
			padding: 1em;
			position: absolute;
			top: 3.2em;
			margin-right: 400px;
		}
		
		#message {
			font-size: 0.7em;
			position: absolute;
			top: 1em; 
			right: 1em;
			width: 350px;
			display: none;
			padding: 1em;
			background: #ffc;
			border: 1px solid #dda;
		}
		
	</style>
<?php 
foreach($output->css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
		<link href="/assets/img/dfk.jpg" rel="shortcut icon" />
		<link href="/assets/img/dfk.jpg" rel="bookmark" />
		
		<link rel='stylesheet' type='text/css' href='/assets/css/jquery.weekcalendar.css' />
		<link rel="stylesheet" type="text/css" href="/assets/css/skins/default.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/skins/gcalendar.css" />

<?php foreach($output->js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
		<script src="/assets/js/bootstrap.js"></script>
		<script type='text/javascript' src='/assets/jqueryui1.8/jquery-ui-1.8.23.custom.min.js'></script>
		<script type='text/javascript' src='/assets/js/jquery.weekcalendar.js'></script>

<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => $active,
					'menu_map' => $menu_map));
			?>
			<div class="span10" style="margin-left: 1%;margin-top: 20px;background-color: rgba(200, 200, 200, 1);width: 84%">
				<div>
					<?php 
						echo $output->output;
					?>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
	function update_filter (filter_name) {
		var es = $('#' + filter_name + '_cal').weekCalendar('serializeEvents');

		var filter_time = '';
		for (i = 0; i < es.length; ++i)
		{
			filter_time += get_event_hour (filter_name, es[i].start) + ',' + get_event_hour (filter_name, es[i].end);
			if (i < es.length - 1) {
				filter_time += ',';
			};
		}
		$('#' + filter_name).val (filter_time);
	}

	function remove_filter_event (filter_name, calEvent) {
		$('#' + filter_name + '_cal').weekCalendar('removeEvent', calEvent.id);
	}

	function get_event_hour (filter_name, time) {
		var hour = $('#' + filter_name + '_cal').weekCalendar('formatDate', time, 'G');
		var minute = $('#' + filter_name + '_cal').weekCalendar('formatDate', time, 'i');

		var hour_minute = Number(hour) + Number(minute / 60);
		return hour_minute.toFixed(1);
	}

	function get_event_time (hour) {
		var year = new Date().getFullYear();
		var month = new Date().getMonth();
		var day = new Date().getDate();

		return new Date(year, month, day, hour);
	}

</script>

<?php 
$this->load->view('templates/footer');
?>
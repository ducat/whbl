<div class="body">
<div class="navbar navbar-inverse navbar-fixed-top" >
	<div class="navbar-inner">
		<div class="container-fluid">

			<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>

			<a href="<?=site_url("/" . current_lang()."/demo/main_show")?>" class="brand" style="padding: 5px 0px 5px;"><img class='img-rounded' src='/assets/img/logo.jpg' style="height:35px;"/>&nbsp;&nbsp;<?=lang('demo_title')?></a>

			<div class="nav-collapse">
				<ul class="nav pull-right">
					<li>
						<a  style="color: #fff;"><?=mdate(lang('date_format_string'), time())?></a>
					</li>
				</ul>

				<ul class="nav pull-right">
					<li class="dropdown profile">
						<a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown"  style="color: #fff;">
							<?=lang('hello') . lang('comma') .
							$user->username?>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu" role="menu">

							<li><a href="<?=site_url('user/logout')?>"><?=lang('log_out')?></a></li>
						</ul>
					</li>
					<!-- <li class="divider-vertical"></li> -->


	 				<!-- <li class="dropdown language">
						<a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown"   style="color: #fff;">
							<?=lang('language') . lang('colon') . lang($this->lang->lang())?>
							<b class="caret"></b>
						</a>

						<ul class="dropdown-menu" role="menu">
							<li><?=anchor($this->lang->switch_uri('cn'), lang('cn'))?></li>
							<li><?=anchor($this->lang->switch_uri('en'), lang('en'))?></li>
						</ul>
					</li> -->

				</ul>
			</div>
		</div>
	</div>
</div>
<div style="height: 40px"></div>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?=$instrument->name . '__' . 
				($edit ? lang('instrument_management') : lang('instrument')) ?></title>
		<link href="/assets/img/dfk.jpg" rel="shortcut icon" />
		<link href="/assets/img/dfk.jpg" rel="bookmark" />
		
		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
		<link href="/assets/css/DT_bootstrap/DT_bootstrap.css" rel="stylesheet">
		<link href="/assets/css/cupertino/jquery-ui-1.8.23.custom.css" rel="stylesheet">
		
		<script src="/assets/js/jquery-1.8.1.min.js"></script>
		<script src="/assets/js/bootstrap.js"></script>
		
		<script src="/assets/js/jquery-ui-1.10.3.js"></script>
		<script src="/assets/js/DT_bootstrap/jquery.dataTables.js"></script>
		<script src="/assets/js/DT_bootstrap/DT_bootstrap.js"></script>

	</head>
<body>
<script type="text/javascript">
//动态更新所有点的数据
var refresh_ponits_data = function() {
		$.post('<?=site_url('instrument/get_points_data')?>', 
				{
					'instrument_id': <?=$instrument->id?>,
					'timestamp': new Date().getTime()
				},
				function(data) {
					for ( var i = 0; i < data.length; i++) {
						var point = data[i];
						if(point.value !== undefined && point.value !== null) {
							$('#point-data-' + point.id).html(Number(point.value).toFixed(2));
						}
					}
				}, 'json');
		setTimeout('refresh_ponits_data()', 5000);
	};

	refresh_ponits_data();
</script>
<?php if($edit) {?>
<script type="text/javascript">
$(function() {
	//drag
	$('.view-item').draggable({
		cursor: 'move',
		containment: '.view-sandmap',
		distance: 20,
		cancel: '.view-item-remove',
		snap: true,
		snapMode: 'outer',
		stack: '.view-sandmap .view-item',
		stop: function(event, ui) {
			var ele_id_array = String($(this).attr('id')).split('-');
			var type = ele_id_array[0];
			var id = ele_id_array[1];
			var x = Math.round($(this).position().left);
			var y = Math.round($(this).position().top);
			$.post('<?=site_url('manage/instrument/set_location')?>',
					{
						'type': type,
						'id': id,
						'x': x,
						'y': y
					});
		}
	});

	//img resize
	$('.item-img').each(function() {
		$(this).resizable({
			containment: '.view-sandmap',
			distance: 20,
			stop: function(event, ui) {
				var img = $(this).children('img');
				var width = img.width();
				var height = img.height();
				var id = img.attr('id').slice(8);
				$.post('<?=site_url('manage/instrument/resize_image')?>',
						{
							'id': id,
							'w': width,
							'h': height
						});
			}
		});
	});

	//delete
	$('.view-item .view-item-remove').click(function() {
		if(window.confirm('<?=lang('instrument_remove_confirm_head')?>' + $(this).attr('title') + '<?=lang('instrument_remove_confirm_tail')?>')) {
			var ele_id_array = String($(this).attr('id')).split('-');
			var type = ele_id_array[0];
			var id = ele_id_array[2];
			var point_id = ele_id_array[3];
			$.post('<?=site_url('manage/instrument/remove_item')?>',
				{
					'type': type,
					'id': id,
					'instrument_id': <?=$instrument->id?>
				}, function(data) {
					if(data) {
						$('#' + type + '-' + id + '-' + point_id).fadeOut(500);
					}
				});
		}
	});
});
</script>
<?php } else if (1 == 0) {?>
<script type="text/javascript">
function close_dialog(){
	$(this).dialog("close");
}
$(function() {
    
	$( '.ctl-form' ).dialog({
		autoOpen: false,
		height: 120,
		width: 250,
		modal: true,
		resizable: false,
		draggable: false,
		buttons: {
			<?=lang('set')?>: function() {
				var ele_id_array = String($(this).attr('id')).split('-');
				var id = ele_id_array[2];
				var value = $("#ctl-value-" + id).val();
				if(isNaN(value)){
					alert("<?=lang('not_number')?>");
				}
				else if(value == ''){
					alert("<?=lang('not_null')?>");
				}
				else{
					$.post("<?=site_url('instrument/send_cmd')?>" + "/" + id + "/" + value,{},function(result){
					});
					$(this).dialog("close");
				}
			},	              
		    <?=lang('cancel')?>: function() {
				$(this).dialog("close");
			}
	    }
		
    });
 
    $( ".item-point" )
      .click(function() {
    	var ele_id_array = String($(this).attr('id')).split('-');
    	var id = ele_id_array[2];
    	$( "#dialog-form-" + id ).dialog( "open" );
        //$('.ui-dialog-titlebar').hide();
        $('.ui-dialog-titlebar-close').hide();
      });
  });
 
</script>
<?php }?>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$active_id = $edit ? 'instrument' : "view/$instrument->id";
			$this->load->view('templates_v2/side_menu_v2', array(
				'active_id' => $active_id,
				'menu_map' => $menu_map));
			?>
			
			<div class="span10 textbox-holder" style="margin-left: 1%; margin-top: 20px;">
				<h3 class="textbox-title">
					<?=$instrument->name?>
					<?php 
						echo "<small>" . ($edit?lang('instrument_edit_view_hint'):lang('instrument_control_hint')) . "</small>";
					?>
				</h3>
				<div class="textbox-content view-sandmap">
				<?php 
					foreach ($inst_images as $image) {
				?>
					<div id="img-<?=$image->id?>" class="view-item item-img" 
						style="top: <?=$image->coordinate_y?>px; left: <?=$image->coordinate_x?>px;<?=$image->display_height ? 'height:' . $image->display_height . 'px;' : ''?><?=$image->display_width ? 'width:' . $image->display_width . 'px;' : ''?>">
						<img id="img-img-<?=$image->image_id?>" src="<?=base_url(UPLOAD_IMG_ROOT . $image->url)?>" 
							alt="<?=$image->display_name?>" style="height: 100%; width: 100%;">
						<?php if($edit) {?>
						<button type="button" class="btn btn-danger btn-mini view-item-remove" 
							id="img-remove-<?=$image->id?>" title="<?=lang('instrument_remove_image')?>">×</button>
						<?php }?>
					</div>
				<?php 
					}
				?>
				<?php 
					foreach ($inst_points as $point) {
				?>
					<div id="point-<?=$point->relation_id?>-<?=$point->id?>" class="view-item item-point label" style="top: <?=$point->coordinate_y?>px; left: <?=$point->coordinate_x?>px;">
						<span class="view-item-label"><?=current_lang()=="en"? $point->english_name:$point->chinese_name?><?=lang('colon')?></span>
						<span class="view-item-data" id="point-data-<?=$point->id?>"><?=lang('instrument_ponit_no_data')?></span>
						<span class="view-item-unit">(<?=$point->unit?>)</span>
						<?php if($edit) {?>
						<button type="button" class="btn btn-danger btn-mini view-item-remove" 
							id="point-remove-<?=$point->relation_id?>-<?=$point->id?>" title="<?=lang('instrument_remove_point')?>">×</button>
						<?php }?>
					</div>
					
					<?php
						if(!$edit) {
							if($point->type != "Analog Input" && $point->type != "Binary Input"){
					?>
<!--					
					<div id="dialog-form-<?=$point->id?>" title="<?=$point->chinese_name . lang('set')?>" class="ctl-form">
			  			<input type="text" id="ctl-value-<?=$point->id?>"  style='margin: auto' />
 					</div>
-->
					<?php }}?>
					
				<?php		
					}
				?>
				</div>
				
			</div>
		</div>
	</div>
</body>

<?php 
$this->load->view('templates/footer');
?>
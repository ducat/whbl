<?php 
$this->load->view('templates/header', 
		array('title' => lang('cop') . '__' . lang('instrument') ));
?>
<body>


<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'cop',
					'menu_map' => $menu_map));
			?>
			
			<div class = "span10" id = "cop_page" style="margin-left: 1%;margin-top: 20px;min-height: 600px;background-color: rgba(200, 200, 200, 1);width: 84%">
				<div class="row-fluid">
					<div class = "span7 textbox-holder" id = "cop_line" style="margin:3% 3.5% auto; min-height: 270px;background-color: whiteSmoke;">
					
						<div class="tabbable" style="height: 240px;margin: 5px 10px auto;">
							<ul class="nav nav-tabs">  
								<li class="active" >
									<a id="a_cop" href="#cop" class = "mytab_cop" data-toggle="tab"><?=lang('analysis_cop')?></a>
								</li> 
								<li>
									<a id="a_sumW" href="#sumW" class = "mytab_cop" data-toggle="tab">KW Usage</a>
								</li>
								<li>
									<a id="a_sumQ" href="#sumQ" class = "mytab_cop" data-toggle="tab"><?=lang('analysis_Q')?></a>
								</li>
							</ul> 
					    	<div id="tab-cont" class="tab-content" style="overflow: visible;height: 250px;"> 
									<div class="tab-pane active" id='cop' style="height: 230px;margin: 8px auto;"></div>
									<div class="tab-pane" id="sumW" style="height: 230px;margin: 8px auto;"></div>
									<div class="tab-pane" id="sumQ" style="height: 230px;margin: 8px auto;"></div>
							</div>
						</div>			
					</div>
					<div class="span4 textbox-holder" id = "building_info" style="margin:3% auto;min-height: 270px;">
					    	<h1 class="textbox-title">
					    		<center>
					    			<?=$station_info->name?>
					    		</center>
					    	</h1>
					    		
					    	<div >
					    		<div class="row-fluid">
					    			<div class="span5" style="line-height: 140px;  height: 140px; margin-left: 5px;">
					    				<img border="0" src="/assets/img/2010612141333562.jpg" class="img-rounded"  style="width:138px;height:140px;"/>
					    			<?php 
						    			if($station_info->image_url != null){
						    				echo "<img class='img-rounded' src='" . base_url(UPLOAD_IMG_ROOT . $station_info->image_url) . "'/>" ;
						    			}					    			
					    			?>					    				
					    			</div>
					    			<div class="span7" style="margin-left: 0;">
					    				<p class="shadow-font"> <?=$station_info->num_chiller . lang('station_tai') . lang('station_chiller')?> </p>		
										<p class="shadow-font"> <?=$station_info->num_cooling_pump . lang('station_tai') . lang('station_cooling_pump')?> </p>	
										<p class="shadow-font"> <?=$station_info->num_freezing_pump . lang('station_tai') . lang('station_freezing_pump')?> </p>	
										<p class="shadow-font"> <?=$station_info->num_tower . lang('station_tai') . lang('station_tower')?> </p>	
					    		 		<p class="shadow-font">
					    		 			<div class="btn-group" style="margin-left:8px">
												<a class="btn btn-mini dropdown-toggle" data-toggle="dropdown" href="#">
													<?= lang('sc_controller') ?>
													<span class="caret"></span>
												</a>
												<ul class="dropdown-menu">
													<?php
													//	$sc_list = explode(";", $station_info->sc_list);
														for ($i=0; $i < count($sc_info); $i++) { 
															echo "<li><a href='http://" . $sc_info[$i]['ip_address'] . "' target='view_window'>" . $sc_info[$i]['name'] . "</a></li>";
														}
													?>
												</ul>
											</div>
										</p>
					    		 	</div>
					    		</div>	
					    		<div class="row-fluid">
						    		<p class="shadow-font"> <?php echo lang('station_duration') . lang('colon') .
												$station_info->days_perweek . lang('station_day') . "/" . lang('station_perweek') . 
												' ' . $station_info->hours_perday . lang('station_hour')."/". lang('station_perday')  ?> </p>	
									<p class="shadow-font"> <?=lang('station_air_condition_area') . lang('colon') . $station_info->air_condition_area . lang('station_m2')?> </p>	
									<p class="shadow-font">
									<?php 	if($station_info->building_type == "commercial")
												$display = lang('station_commercial');
											elseif ($station_info->building_type == "hotel")
												$display = lang('station_hotel');
											elseif ($station_info->building_type == "office")
												$display = lang('station_office');
											elseif ($station_info->building_type == "hospital")
												$display = lang('station_hospital');
											elseif ($station_info->building_type == "others")
												$display = lang('station_others');
											else 
												$display = $station_info->building_type; 											
											echo lang('station_building_type') . lang('colon') . $display ;
									 ?>
									</p>	
					    		</div>
						    </div>
					</div>

				</div>
				<div class="row-fluid" >
					<div class="row-fluid span7 textbox-holder" style="margin:0 3.5% auto;min-height: 250px;background-color: whiteSmoke">
						<div class="" id = "cop_bar1" style="margin-top:20px; min-height: 80px"></div>			
						<div class="" id = "cop_bar2" style="margin-top:20px; min-height: 80px"></div>	
					</div>

				    <div class="span4" id = "cop_gauges" style="min-height: 250px;margin:0  auto;">
				    	<div id = "dual_meter">
				    		<div class="span6 textbox-holder">
				    			<h3 class="textbox-title"><?=lang('outside_temperature')?></h3>
				    			<div class="" id="gauge1" style="height: 210px;"></div>
				    		</div>    	
				    		<div class="span6 textbox-holder">
				    			<h3 class="textbox-title"><?=lang('outside_moisture')?></h3>
				    			<div class="" id="gauge2" style="height: 210px;"></div>
				    		</div>
						</div>
				    </div>
				 </div>
			</div>
			<script src="/assets/js/highcharts/highcharts.js" type="text/javascript"></script>
			<script src="/assets/js/highcharts/modules/exporting.js" type="text/javascript"></script>   
			<script src="/assets/js/highcharts/highcharts-more.js" type="text/javascript"></script>
			
		</div>
	</div>
<script type="text/javascript">	

function refresh() 
{ 
       window.location.reload(); 
} 
setTimeout('refresh()',1000 * 60 * 10); //指定刷新 
//setTimeout('refresh()',1000 * 5); //指定刷新 
$(function () {

	Highcharts.setOptions({     
	    global: {     
	        useUTC: false     
	    }     
	});
	drawGuage(gauge1,<?php print_r($cur_temp)?>,'℃',-10,50);
	drawGuage(gauge2,<?php print_r($cur_humi)?>,'%',0,100);
	drawCOP();
	
	$("#a_cop"). bind ('click',function(){
		$("#cop"). css( "width",	$("#cop").parent().width() );
		drawCOP();
	});
	$("#a_sumW"). bind ('click',function(){
		$("#sumW"). css( "width",	$("#sumW").parent().width() );
		drawW();
	});
	$("#a_sumQ"). bind ('click',function(){
		$("#sumQ"). css( "width",	$("#sumQ").parent().width() );
		drawQ();
	});

	drawBar('cop_bar1','sumW','<?=lang('day_power_consumption')?>','#9DD22F','KWh');
	drawBar('cop_bar2','sumQ','<?=lang('day_chill_consumption')?>','#90B1D8','Ton');

});

function drawW(){
	dataW = <?= json_encode($cop_data['W'],JSON_NUMERIC_CHECK);?> ;
	render(dataW,'sumW', 'W','KW');
}
function drawQ(){
	dataQ = <?= json_encode($cop_data['Q'],JSON_NUMERIC_CHECK);	?> ;
	render(dataQ,'sumQ', 'Q','KW');
}
function drawCOP(){
	dataCOP = <?=json_encode($cop_data['COP'],JSON_NUMERIC_CHECK); ?> ;
	render(dataCOP,'cop', 'COP','COP');	
}

function render(data, renderTo, name, metric){
	window.chart = new Highcharts.Chart({
		chart : {
			renderTo : renderTo,
			backgroundColor: 'transparent',
			zoomType: 'x',
			borderWidth: 0,
			type: 'spline'
		},	
        credits : {
            enabled : false
        }, 
        legend : {
        	enabled : false
        },
		rangeSelector : {
			inputEnabled : false,
			selected : 1
		},
		title : {
			text : ''
		},	
		exporting : {
			buttons : {
				printButton : {
					enabled : false
				},
				exportButton : {
					enabled : false
				}
			}
		},	
		xAxis: {
			//gridLineWidth: 1,
			lineColor: '#000',
			tickColor: '#000',
			type: "datetime",
			labels: {
				style: {
					color: '#000',
					font: '11px Trebuchet MS, Verdana, sans-serif'
				}
			},
		
			title: {
				style: {
					color: '#333',
					fontWeight: 'bold',
					fontSize: '12px',
					fontFamily: 'Trebuchet MS, Verdana, sans-serif'

				}
			}
		},
		yAxis: {
			minorTickInterval: 'auto',
			min:0,
			lineColor: '#000',
			lineWidth: 1,
			tickWidth: 1,
			tickColor: '#000',
			labels: {
				style: {
					color: '#000',
					font: '11px Trebuchet MS, Verdana, sans-serif'
				}
			},
			title: {
				text: metric,
				style: {
					color: '#333',
					fontWeight: 'bold',
					fontSize: '12px',
					fontFamily: 'Trebuchet MS, Verdana, sans-serif'
				}
			}
		},
        tooltip: {
            crosshairs: true
        },
		series : [{
			name : name,
			data : data,
			marker : {
				enabled : true,
				radius : 4
			},
			shadow : true
		}]
	});	
}

function drawGuage(container,value,metric,min,max){	
    var chart = new Highcharts.Chart({	
	    chart: {
	        renderTo: container,
	        type: 'gauge',
	        plotBackgroundColor: null,
	        plotBackgroundImage: null,
	        plotBorderWidth: 0,
	        plotShadow: false,
	        backgroundColor: 'transparent',
	        borderRadius: 0
	    },	    
	    title: {
	        text: null
	    },
	    exporting : {
			buttons : {
				printButton : {
					enabled : false
				},
				exportButton : {
					enabled : false
				}
			}
		},	
        credits : {
            enabled : false
        }, 
	    pane: {
	        startAngle: -150,
	        endAngle: 150,
	        background: [{
	            backgroundColor: {
	                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
	                stops: [
	                    [0, '#FFF'],
	                    [1, '#333']
	                ]
	            },
	            borderWidth: 0,
	            outerRadius: '109%'
	        }, {
	            backgroundColor: {
	                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
	                stops: [
	                    [0, '#333'],
	                    [1, '#FFF']
	                ]
	            },
	            borderWidth: 1,
	            outerRadius: '107%'
	        }, {
	            // default background
	        }, {
	            backgroundColor: '#DDD',
	            borderWidth: 0,
	            outerRadius: '105%',
	            innerRadius: '103%'
	        }]
	    },
	       
	    // the value axis
	    yAxis: {
	        min: min,
	        max: max,
	        
	        minorTickInterval: 'auto',
	        minorTickWidth: 1,
	        minorTickLength: 10,
	        minorTickPosition: 'inside',
	        minorTickColor: '#666',
	
	        tickPixelInterval: 30,
	        tickWidth: 2,
	        tickPosition: 'inside',
	        tickLength: 10,
	        tickColor: '#666',
	        
	        labels: {
	            step: 2,
	            rotation: 'auto'
	        },
	        title: {
	        	y: 15,
	            text: metric
	        }
	        ,
	      plotBands: [{
	            from: min,
	            to: min + (max - min) * 0.3,
	            color: '#55BF3B' // green
	        }, {
	            from: min + (max - min) * 0.3,
	            to: min + (max - min) * 0.7,
	            color: '#DDDF0D' // yellow
	        }, {
	            from: min + (max - min) * 0.7,
	            to: max,
	            color: '#DF5353' // red
	        }]       
	    },
	
	    series: [{
	        name: 'metric',
	        data: [value],
	        tooltip: {
	            valueSuffix: metric
	        }
	    }]
	
	}
	// Add some life
	);
}


function drawBar(container, type, category, color, metric) {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: container,
                backgroundColor: 'transparent',
                type: 'bar',
                events: {
                    load: function() {
                    	var series = this.series[0];
                		$.getJSON('<?=site_url('instrument/get_sum_data') . '/' . date("Y-m-d")?>', function(result){
                			var data = result[type];
                			series.addPoint([0,data], true, true)
                		});
                    }
                }
            },
            title: {
                text: null
            },
            credits : {
                enabled : false
            }, 
            exporting : {
    			buttons : {
    				printButton : {
    					enabled : false
    				},
    				exportButton : {
    					enabled : false
    				}
    			}
    		},
			tooltip : {
					formatter: function() {
						return '<b>'+ this.series.name +'</b><br/>'+
						this.y + metric;
					},
			},
            xAxis: {
            	title: {
    				text: category
    			},
    	        labels: {
    	            enabled: false
    	        },
			    categories: [category]
            },
            yAxis: {
                min: 0,
                title: {
                    text: null
                },  
                labels: {
                    overflow: 'justify'
                }
            },
            legend: {
            	enabled:false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                    	formatter: function() {
                    		return this.y + metric;
                    	},
                    	align: 'right',
                    	x: -10,
                    	color: '#FFFFFF',
                        enabled: true
                    }
                },
                series: {
                    stacking: 'normal'
                }
            },
            series: [{
                name: category,
                color: color,
                data: [[0,0]]
            }]
        });
    }
</script>
</body>

<?php 
$this->load->view('templates/footer');
?>
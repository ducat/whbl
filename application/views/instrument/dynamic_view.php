<!DOCTYPE html>
<html lang="en">
	<head>
	    <?php if($edit){?>
	    		<meta charset="utf-8">
	    <?php }else{?>
	    		<meta charset="utf-8" <?php if($refresh_interval != 0) {?>http-equiv="refresh" content="<?=$refresh_interval?>;url=<?=$next_url?>"<?php }?>>
	    <?php }?>
		<title><?=$instrument->name . '__' . 
				($edit ? lang('instrument_management') : lang('instrument')) ?></title>
		<link href="/assets/img/favico.ico" rel="shortcut icon" />
		<link href="/assets/img/favico.ico" rel="bookmark" />
		
		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
		<link href="/assets/css/DT_bootstrap/DT_bootstrap.css" rel="stylesheet">
		<link href="/assets/css/cupertino/jquery-ui-1.8.23.custom.css" rel="stylesheet">
		
		<script src="/assets/js/jquery-1.8.1.min.js"></script>
		<script src="/assets/js/bootstrap.js"></script>
		
		<script src="/assets/js/jquery-ui-1.10.3.js"></script>
		<script src="/assets/js/DT_bootstrap/jquery.dataTables.js"></script>
		<script src="/assets/js/DT_bootstrap/DT_bootstrap.js"></script>

		<style>
    		.ctl-form { border: 1px solid transparent; padding: 0.3em; }
		</style>
	</head>
<body>
<script type="text/javascript">
//动态更新所有点的数据
var refresh_points_data = function() {
		$.post('<?=site_url('instrument/get_points_data')?>', 
				{
					'instrument_id': <?=$instrument->id?>,
					'timestamp': new Date().getTime()
				},
				function(data) {
					for ( var i = 0; i < data.length; i++) {
						var point = data[i];
						if(point.value !== undefined && point.value !== null) {
							$('#point-data-' + point.point_id).html(Number(point.value).toFixed(1));
							if(point.dynamic_rule !== null && point.dynamic_value !== null && point.dynamic_rule !== undefined && point.dynamic_value !== undefined){
								point.value = Number(point.value);
								point.dynamic_value = Number(point.dynamic_value);
								var judge = false;
								switch (point.dynamic_rule)
								{
									case 'eq':
									  	judge = (point.value == point.dynamic_value);
									  	break;
									case 'ne':
										judge = (point.value !== point.dynamic_value);
										break;
									case 'gt':
										judge = (point.value > point.dynamic_value);
										break;
									case 'ge':
										judge = (point.value >= point.dynamic_value);
										break;
									case 'lt':
										judge = (point.value < point.dynamic_value);
										break;
									case 'le':
										judge = (point.value <= point.dynamic_value);
										break;
									default:
										judge = false;
								
								}
								
								
								if(judge)
									$('#group-img-static-' + point.id).css('display','none');
								else
									$('#group-img-static-' + point.id).css('display','block');
							}
						}
					}
				}, 'json');
		setTimeout('refresh_points_data()', 60000);
	};

	refresh_points_data();
	
<?php if($edit) {?>
$(function() {
	//drag
	$('.view-item').draggable({
		cursor: 'move',
		containment: '.view-sandmap',
		distance: 20,
		cancel: '.view-item-remove',
		//snap: true,
		//snapMode: 'outer',
		stack: '.view-sandmap .view-item',
		stop: function(event, ui) {
			var ele_id_array = String($(this).attr('id')).split('-');
			var type = ele_id_array[0];
			var id = ele_id_array[1];
			var x = Math.round($(this).position().left);
			var y = Math.round($(this).position().top);
			$.post('<?=site_url('manage/instrument/set_location')?>',
					{
						'type': type, 'id': id, 'x': x, 'y': y
					});
		}
	});

	//img resize
	$('.item-img').each(function() {
		$(this).resizable({
			containment: '.view-sandmap',
			distance: 20,
			stop: function(event, ui) {
				var ele_id_array = String($(this).attr('id')).split('-');
				var width = $(this).width();
				var height = $(this).height();
				var id = ele_id_array[1];
				var type = ele_id_array[0];
				$.post('<?=site_url('manage/instrument/resize_image')?>',
						{
							'type': type,
							'id': id,
							'w': width,
							'h': height
						});
			}
		});
	});
});


<?php } else {?>
$(document).ready(function(){
	$(".item-point").mousedown(function(e){
		$('.modal-footer').find('a').remove();
		var point_data_array = String($(this).children(".view-item-data")[0].id).split('-');
		var point_id = point_data_array[2];		
		var ele_id_array = String($(this).attr('id')).split('-');
		var type = ele_id_array[2];
		var point_name = String($(this).children(".view-item-label")[0].textContent);
		if(type == "ao" || type == "bo"){
			if(e.which == 1){ //left click
				$('#modal-ctl .modal-body legend').html(point_name);
				$('#ctl-point-id').html(point_id);
				$('.modal-footer').prepend('<a href = "<?php echo site_url('analysis/show_chart/');?>/'+point_id+'">'+"<button class='btn' ><?=lang('check')?></button>"+'</a>');

				if(type == "ao"){
					$('#bo-ctl-value').css("display","none");
					$('#ao-ctl-value').css("display","block");
				}
				else{
					$('#ao-ctl-value').css("display","none");
					$('#bo-ctl-value').css("display","block");
				}
				$('#modal-ctl').modal({
		    		  keyboard: false,
		    		  backdrop: 'static'
		    	});
				$('#modal-ctl').modal('show');
				
			}
			else if(e.which == 3){ //right click
				$.getJSON("<?=site_url('instrument/send_cmd')?>" + "/" + point_id + "/1",function(result){
					if(result.length < 16){
						alert(result);
						return false;
					}
					
					$("#ctl-list tbody").empty();
				    $.each(result, function(i, field){
				    	$("#ctl-list tbody").append("<tr>");
				    	$("#ctl-list tbody").append("<td>" + (i+1) + "</td>");
				    	$("#ctl-list tbody").append("<td>" + field + "</td>");
				    	$("#ctl-list tbody").append("<td><button id='clear-btn-"+ point_id +"-" + (i+1) + "' type='button' class='btn btn-danger clear-btn'><?=lang("control_level_clear")?></button></td>");
				    	$("#ctl-list tbody").append("</tr>");
				    });
				    $('#modal-del').modal({
			    		  keyboard: false,
			    		  backdrop: 'static'
			    	});
					$('#modal-del').modal('show');
					$('#modal-del').on('hidden', function () {
						$("#ctl-list tbody").empty();
					});
					enable_clear_btn();
				});
			}
			else
				return false;
		}
		else
			return false;
  		
		return false;//阻止链接跳转
	});

	$("#ctl-submit").click(function(){
		var ctl_val = $('.ctl-value:visible').val();
		var priority = $('#ctl-priority').val();
		var point_id = $('#ctl-point-id').html();
		if(ctl_val == ""){
			alert('<?=lang('control_not_null')?>');
			return false;
		}
		else{
			if(isNaN(ctl_val)){
			   alert("<?=lang("control_valid_number")?>");
			   return false;
			}else{
				$.post("<?=site_url('instrument/send_cmd')?>" + "/" + point_id + "/0/" + priority + "/" + ctl_val, function(data) {
					alert(data);
					$('#modal-ctl').modal('hide');
					return true;
				});
			}
		}
	});
});	

function enable_clear_btn(){
	$(".clear-btn").click(function(){
		var ele_id_array = String($(this).attr('id')).split('-');
		var point_id = ele_id_array[2];
		var clear_priority = ele_id_array[3];
		$.getJSON("<?=site_url('instrument/send_cmd')?>" + "/" + point_id + "/2/" + clear_priority,function(result){
			if(result.length < 16){
				alert(result);
				return false;
			}
			$("#ctl-list tbody").empty();
		    $.each(result, function(i, field){
		    	$("#ctl-list tbody").append("<tr>");
		    	$("#ctl-list tbody").append("<td>" + (i+1) + "</td>");
		    	$("#ctl-list tbody").append("<td>" + field + "</td>");
		    	$("#ctl-list tbody").append("<td><button type='button' class='btn btn-danger disabled'><?=lang("control_level_clear")?></button></td>");
		    	$("#ctl-list tbody").append("</tr>");
		    });
		});
	});
}


<?php } ?>
</script>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$active_id = $edit ? 'instrument' : "view/$instrument->id";
			$this->load->view('templates/side_menu', array(
				'active_id' => $active_id,
				'menu_map' => $menu_map));
			?>
			
			<div class="span10 textbox-holder" style="margin-left: 1%; margin-top: 20px;">
				<h3 class="textbox-title">
					<?=$instrument->name?>
					<?php 
						echo "<small>" . ($edit?lang('instrument_edit_view_hint'):lang('instrument_control_hint')) . "</small>";
					?>
				</h3>
				<div class="textbox-content view-sandmap">
				<?php 
					foreach ($inst_images as $image) {
				?>
					<div id="img-<?=$image->id?>" class="view-item item-img" 
						style="top: <?=$image->image_y?>px; left: <?=$image->image_x?>px;<?=$image->image_height ? 'height:' . $image->image_height . 'px;' : ''?><?=$image->image_width ? 'width:' . $image->image_width . 'px;' : ''?>">
						<img id="img-img-<?=$image->id?>" src="<?=base_url(UPLOAD_IMG_ROOT . $image->url)?>" 
							alt="<?=$image->display_name?>" style="height: 100%; width: 100%;">
						<?php if(false) {?>
						<button type="button" class="btn btn-danger btn-mini view-item-remove" 
							id="img-remove-<?=$image->id?>" title="<?=lang('instrument_remove_image')?>">×</button>
						<?php }?>
					</div>
				<?php 
					}
				?>
				<?php 
					foreach ($inst_points as $point) {
						if ($point->display_text == NULL)
							$label_name = $point->chinese_name;
						else
							$label_name = $point->display_text;
				?>
				<a href = "<?php if($edit == false)echo site_url('analysis/show_chart/'.$point->point_id);else echo '#';?>">
					<div id="point-<?=$point->id?>-<?=$point->type?>" class="view-item item-point label" style="top: <?=$point->related_point_y?>px; left: <?=$point->related_point_x?>px;">
						<span class="view-item-label" style="color:<?= $point->font_color?>;font-style: <?= $point->font_style?>;font-weight: <?= $point->font_weight?>;font-size: <?= $point->font_size?>px"><?=$label_name?><?=lang('colon')?></span>
						<span class="view-item-data" style="color:<?= $point->font_color?>;font-style: <?= $point->font_style?>;font-weight: <?= $point->font_weight?>;font-size: <?= $point->font_size?>px" id="point-data-<?=$point->point_id?>">--</span>
						
						<?php if(false) {?>
						<button type="button" class="btn btn-danger btn-mini view-item-remove" 
							id="point-remove-<?=$point->id?>" title="<?=lang('instrument_remove_point')?>">×</button>
						<?php }?>
					</div>
				</a>
				<?php } ?>
					
					
				<?php if(!$edit){?>	
				<div id="modal-ctl" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modal-ctl-label" aria-hidden="true">
					<div class="modal-header">
				    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    	<h3 id="modal-ctl-label"><?=lang("instrument_control")?></h3>
				  	</div>
				  	<div class="modal-body">
						<form>
							<fieldset>
								<legend></legend>
								<label><?=lang('control_point')?></label>
								<label id="ctl-point-id"></label>
								<label><?=lang("control_priority")?></label>
								<select id='ctl-priority'>
									<option>3</option>
									<option>4</option>
									<option selected>8</option>
									<option>12</option>
									<option>16</option>
								</select>
								<label><?=lang("control_setvalue")?></label>
								<input type="text" id='ao-ctl-value'  class='ctl-value' placeholder="Control value...">
								<select id='bo-ctl-value'  class='ctl-value'>
									<option>0</option>
									<option>1</option>
								</select>
							</fieldset>
						</form>	
				  	</div>
					<div class="modal-footer">
						
				    	<button class="btn" data-dismiss="modal" aria-hidden="true"><?=lang('cancel')?></button>
				    	<button class="btn btn-primary" id='ctl-submit'><?=lang("save")?></button>
				  	</div>
				</div>
				<div id="modal-del" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modal-del-label" aria-hidden="true">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					    <h3 id="modal-del-label"><?=lang('control_level_list')?></h3>
					</div>
					<div class="modal-body">
						<table class="table table-hover" id="ctl-list">
              				<thead>
			                	<tr>
			                  		<th><?=lang('control_level')?></th>
			                  		<th><?=lang('control_level_value')?></th>
			                  		<th></th>
			                	</tr>	
              				</thead>
              				<tbody></tbody>
            			</table>
					</div>
					<div class="modal-footer">
					    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					</div>
				</div>
 				<?php }?>
					
					
				<?php 
					foreach ($inst_img_groups as $group) {
				?>
					<div id="group-<?=$group->id?>" class="view-item item-img" 
						style="top: <?=$group->img_group_y?>px; left: <?=$group->img_group_x?>px;<?=$group->img_group_height ? 'height:' . $group->img_group_height . 'px;' : ''?><?=$group->img_group_width ? 'width:' . $group->img_group_width . 'px;' : ''?>">
						<img id="group-img-static-<?=$group->id?>" src="<?=base_url(UPLOAD_IMG_ROOT . $group->static_url)?>" 
							alt="<?=$group->display_name?>" style="position:absolute; z-index:2; height: 100%; width: 100%;">
						<img id="group-img-dynamic-<?=$group->id?>" src="<?=base_url(UPLOAD_IMG_ROOT . $group->dynamic_url)?>" 
							alt="<?=$group->display_name?>" style="position:absolute; z-index:1; height: 100%; width: 100%;">
						<?php if(false) { ?>
						<button type="button" class="btn btn-danger btn-mini view-item-remove" 
							id="group-remove-<?=$group->id?>" title="<?=lang('instrument_remove_image')?>">×</button>
						<?php }?>
					</div>
				<?php 
					}
				?>
				
				
				
				</div>
				
			</div>
		</div>
	</div>
</body>

<?php 
$this->load->view('templates/footer');
?>
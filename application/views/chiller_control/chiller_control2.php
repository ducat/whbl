<?php 
$this->load->view('templates/header', 
		array('title' => lang('cop') . '__' . lang('instrument') ));
?>
<body>


<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => $active,
					'menu_map' => $menu_map));
			?>
			
			<div class = "span10" id = "cop_page" style="margin-top: 20px;min-height: 570px;background-color: white;padding:10px">
				<table class="table table-condensed">
				<thead><tr><th>名称</th><th>运行状态</th><th>故障报警</th><th>手自动状态</th><th>频率反馈</th></tr></thead>
				<tbody>
				<?php foreach($data as $v):?>
					<tr>
						<td><?= $v['name']?></td>
						<td><?= $v['on_off']?></td>
						<td><?= $v['alarm']?></td>
						<td><?= $v['auto_manual']?></td>
						<td><?= $v['frequency']?></td>
					</tr>
				<?php endforeach ?>
				</tbody>
				</table>
			</div>
			<script src="/assets/js/highcharts/highcharts.js" type="text/javascript"></script>
			<script src="/assets/js/highcharts/modules/exporting.js" type="text/javascript"></script>   
			<script src="/assets/js/highcharts/highcharts-more.js" type="text/javascript"></script>
			
		</div>
	</div>
</body>

<?php 
$this->load->view('templates/footer');
?>
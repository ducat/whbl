<?php 
$this->load->view('templates/header', 
		array('title' =>  lang('custom_report_thumbnail') . "__" . lang('custom_report')));
?>
<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 

			$this->load->view('templates/side_menu', array(
				'active_id' => "custom_report_thumbnail",
				'menu_map' => $menu_map));
			?>
			
			<div class="span10" style="margin: 20px 1% auto;min-height: 600px;"> 
				<h4 style="color:rgba(203,233,243,1);"><?=lang('custom_report_thumbnail')?></h4>
				<ul class="thumbnails" style="margin-top: 15px;">
				
				<?php 
				$i = 0;
				foreach ($reports as $row)
				{
					if($i%4 == 0){
						echo "<div class='row-fluid' style='margin-top: 20px;'>";
					}
					echo "<li class='span3 textbox-holder'>";
					echo "	<div class='thumbnail'>";
					echo "		<h4 style='font-size:15px;'><center>". $row->display_name ."</center></h4>";			
				    echo "		<a href='custom_report_view/".$row->id."'><img src='/assets/img/chart_thumbnail.jpeg' alt='no img' ></img></a>";
				    echo "		<p style='font-size:15px;' align='right'>". $row->username ."</p>";
				    //   echo "		<a class='btn btn-medium btn-primary' href='custom_report_view/".$row->id."'> " . lang('custom_report_enter'). "</a>"; 
				    echo " 	</div>";
				    echo "</li>";
				    
				    if($i%4 == 3){
				    	echo "</div>";
				    }
				    $i++;
				}
				?>
				</ul>
			</div>
		</div>
	</div>
</body>

<?php 
$this->load->view('templates/footer');
?>
<?php 
$this->load->view('templates/header', 
		array('title' =>  lang('custom_report') . "__" . lang('analysis')));
?>
<body>

<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
	?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 

			$this->load->view('templates/side_menu', array(
				'active_id' => "custom_report_thumbnail",
				'menu_map' => $menu_map));
			?>
			
			<?php 
				$this->load->view("custom_report/multi_point_report.php", array('custom_report' => $custom_report));
			?>
		</div>
	</div>
</body>

<?php 
$this->load->view('templates/footer');
?>
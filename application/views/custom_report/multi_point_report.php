<div class="span10 textbox-holder" style="margin-left: 1%;margin-top: 20px;min-height: 600px">
	<h3 class="textbox-bigtitle" style="font-size: 19px" align="center"><?php echo $custom_report->report_info->display_name; ?></h3>
	
	<div class="tabbable" style='min-height: 480px'>
		<ul class="nav nav-tabs" style='margin: 0 0 0px 0px;'>  
			<li class="active">
				<a id="a_dual" href="#dual_axis" class="mytab" data-toggle="tab" onclick="draw_dual()"><?=lang('custom_report_dual')?></a>
			</li> 
			<li>
				<a id="a_left" href="#left_axis" class="mytab" data-toggle="tab" onclick="draw_left()"><?=lang('custom_report_left')?></a>
			</li>
			<li>
				<a id="a_right" href="#right_axis" class="mytab" data-toggle="tab" onclick="draw_right()"><?=lang('custom_report_right')?></a>
			</li>
<!-- 			<li> -->
<!-- 				<a id="a_table" class="btn"  onclick="window.open('<?=site_url("analysis/single_point_table/1")?>');"><?=lang('custom_report_show')?></a>-->
<!-- 			</li> -->
			<p align="right"><?php echo lang("custom_report_creator_id")." : ".$custom_report->report_info->username; ?></p>
			
		</ul> 
		
		<div class="tab-content" style="overflow: visible;min-height: 460px"> 
				<div class="tab-pane active" id = 'dual_axis' style="height:450px;"></div>
				<div class="tab-pane" id="left_axis" style="height:450px;"></div>
				<div class="tab-pane" id="right_axis" style="height:450px;"></div>
			</div>
	</div>
	
</div>	
<script src="/assets/js/highcharts/highstock.js" type="text/javascript"></script>
<script src="/assets/js/highcharts/themes/grid.js" type="text/javascript"></script>
<script src="/assets/js/highcharts/modules/exporting.js" type="text/javascript"></script>   
<script type="text/javascript">
$(function() {
	Highcharts.setOptions({     
	    global: {     
	        useUTC: false     
	    }     
	});
	draw_dual();
	/*$("#a_dual"). bind ('click',function(){
		$("#dual_axis"). css( "min-width",	$("#dual_axis").parent().width() );
		drawCOP();
	});
	$("#a_left"). bind ('click',function(){
		$("#left_axis"). css( "min-width",	$("#left_axis").parent().width() );
		drawW();
	});
	$("#a_right"). bind ('click',function(){
		$("#right_axis"). css( "min-width",	$("#right_axis").parent().width() );
		drawQ();
	});*/

	$("#dual_axis"). bind ('resize',function(){
		$("#dual_axis"). css( "width",	$("#dual_axis").parent().width() );
	});
	$("#left_axis"). bind ('resize',function(){
		$("#left_axis"). css( "width",	$("#left_axis").parent().width() );
	});
	$("#right_axis"). bind ('resize',function(){
		$("#right_axis"). css( "width",	$("#right_axis").parent().width() );
	});
	

});
function draw_dual(){
	draw(0);
}
function draw_left(){
	draw(1);
}
function draw_right(){
	draw(2);
}

var points = <?php echo json_encode($custom_report->points); ?>;
function draw(type) {
    var seriesOptions = [],
        yAxisOptions = [],
        seriesCounter = 0,
        
        colors = Highcharts.getOptions().colors,
        container = 'dual_axis';
    if(type == 0)
        container = 'dual_axis';   
    else if(type == 1)
        container = 'left_axis';   
    else
        container = 'right_axis';        
    $.each(points, function(i, point) {
	    $.getJSON("<?=site_url("analysis/get_single_point_data")?>?id="+ point["id"] + "&callback=?", function(data) {
			if(type == 0)
	            seriesOptions[i] = {
	                name: point['name'],
	                data: data,
	                yAxis: i==1? 1:0,
	            };
			else if(type == 1)
	            seriesOptions[i] = {
	                name: point['name'],
	                data: data,
	                yAxis: 0,
	            };
			else
	            seriesOptions[i] = {
	                name: point['name'],
	                data: data,
	                yAxis: 1,
	            };
            if(i == 0)
            yAxisOptions[0] = {
                title: {
                    text: (type==0 || type == 1)? point['name'] + '(' + point['unit'] + ')' : null
                },
				labels: {
					formatter: function() {
						return this.value;
                    }
				},
				opposite: false
			};
			if(i == 1)
            yAxisOptions[1] = {
                title: {
                	text: (type==0 || type == 2)? point['name'] + '(' + point['unit'] + ')' : null
                },   
    			labels: {
    				formatter: function() {
    					return this.value;
                    }
    			},
    			opposite: true
    		};

            // As we're loading the data asynchronously, we don't know what order it will arrive. So
            // we keep a counter and create the chart when all the data is loaded.
            seriesCounter++;
            if (seriesCounter == points.length) {                
                createChart(container);
            }
        });
    });
	
    // create the chart when all data is loaded
    function createChart(cont) {
        chart = new Highcharts.StockChart({
            chart: {
	                renderTo: cont,
	                borderWidth: 0,
	                borderRadius: 0,
	                backgroundColor: 'whiteSmoke',
	                type: 'spline',
	                zoomType: 'x'                
            },
            credits : {
                enabled : false
            },   
            legend: {
                enabled: true,
                align: 'center',
				verticalAlign: 'top',
				borderWidth: 2,
                itemStyle: {
                	fontSize: '14px'
                }
            },  

            navigator : {
                adaptToUpdatedData: false,
                series : {
                    data : seriesOptions[0]['data']
                }
            },
			exporting : {
				url: '<?=site_url('export/index')?>',
				buttons : {
					printButton : {
						enabled : false
					}
				}
			},	
            rangeSelector: {
            	inputEnabled : false,
                selected: 5
            },

            xAxis : {
                events : {
                    afterSetExtremes : afterSetExtremes
                },
            	minRange: 3600 * 1000
            },            
            yAxis: yAxisOptions,
   
                     
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                valueDecimals: 2
            },            
            series: seriesOptions
        });
    }
}

function afterSetExtremes(e) {

		
	/*if(range_orig <  7 * 24 * 3600 * 1000){
	}
	else if(range_orig <  90 * 24 * 3600 * 1000){

	    var url,
	        currentExtremes = this.getExtremes(),
	        range = e.max - e.min;
	    if(range < 7 * 24 * 3600 * 1000){
	    	refresh(e.min, e.max);
		}
	    else if(range < 90 * 24 * 3600 * 1000){
	    	refresh(e.min, e.max);
		}
	    else{
	    	refresh(e.min, e.max);
		}
	}
	else*/
	{
	    var url,
	        currentExtremes = this.getExtremes(),
	        range = e.max - e.min;
	    if(range < 7 * 24 * 3600 * 1000){
	    	refresh(e.min, e.max);
		}
	    else if(range < 90 * 24 * 3600 * 1000){
	    	refresh(e.min, e.max);
		}
	    else{
			refresh(e.min, e.max);
		}		
	}    
}

function refresh(min, max){
	var seriesCounter = 0;
	var multi_data = [];
	chart.showLoading('Loading data from server...');	
	$.each(points, function(i, point) {
		$.getJSON("<?=site_url("analysis/get_single_point_data")?>?id=" + point["id"] + "&start="+ Math.round(min) +
		            "&end="+ Math.round(max) +"&callback=?", function(data) {
			multi_data[i] = data;
            seriesCounter++;
            if (seriesCounter == points.length) {
                
                for(var j = 0; j < points.length; j++){
					chart.series[j].setData(multi_data[j]);
                }
                chart.hideLoading();
                
                
            }
        });
    });
	
}
</script>
<?php
$this->load->view('templates/header',
	array('title' => lang('welcome')));
	?>
	<body class="welcome-body">
		<header class="jumbotron masthead" id="overview" style='height:70%'>
			<div class="container">
				 <!--  <img class='img-rounded' style='margin-bottom:20px;width:250px;height:250px;' src='/assets/img/base_download_att.jpg' style="height:200px;"/> -->

				<br>

					<p class="lead">武&nbsp;&nbsp;&nbsp;汉&nbsp;&nbsp;&nbsp;保&nbsp;&nbsp;&nbsp;利&nbsp;&nbsp;&nbsp;文&nbsp;&nbsp;&nbsp;化&nbsp;&nbsp;&nbsp;广&nbsp;&nbsp;&nbsp;场</p>
					<p class="lead">集&nbsp;&nbsp;成&nbsp;&nbsp;管&nbsp;&nbsp;理&nbsp;&nbsp;系&nbsp;&nbsp;统</p>
				
			</div>
			<div class="container unit">
				<?php
				if($user) {
					?>
					<h2>

						<?=lang('welcome_back') . lang('comma') . $user->username?>
					</h2>
					<p>
						<?=anchor('demo', '进入系统' . '&raquo;',
						array('class' => 'btn btn-primary'))?>

						<?=anchor('user/logout', lang('log_out'),
						array('class' => 'btn'))?>
					</p>
					<?php } else {?>
					<h2><?=lang('please_sign_in')?></h2>
					<form class="form-inline" method="post" action="<?=site_url('user/login')?>">
						<input id="username" name="username" type="text" class="input-medium" id="username" name="username" placeholder="<?=lang('username')?>">
						<input id="password" name="password" type="password" class="input-medium" placeholder="<?=lang('password')?>">
						<label class="checkbox">
							<input type="checkbox" name="remenber" value="1">
							<?=lang('remember_me')?>
						</label>
						<button type="submit" class="btn btn-primary"><?=lang('sign_in')?></button>
					</form>
					<?php }?>
				</div>
			</header>
		</body>
		

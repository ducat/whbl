<?php 
$this->load->view('templates/header', 
		array('title' => lang('point_view') ));
?>
<body>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'charge_table',
					'menu_map' => $menu_map));
			?>			
			
			<div class="textbox-holder span10" style="margin-left: 1%;margin-top: 20px;min-height: 600px">
				<div id="point_chart" style="width:98%; height:550px; margin:15px auto;">
				</div>
			</div>
		</div>
	</div>
</body>

<script src="/assets/js/highcharts/highstock.src.js" type="text/javascript"></script>
<script src="/assets/js/highcharts/highcharts-more.js" type="text/javascript"></script>
<script src="/assets/js/highcharts/modules/exporting.js" type="text/javascript"></script>   

<script type="text/javascript">
$(".language").empty();
$(function() {
	Highcharts.setOptions({     
	    global: {     
	        useUTC: false     
	    }     
	});
		
	<?php 
	
	$peak = array();
	$flat = array();
	$valley = array();
	$all = array();
	$day = array();
	foreach ($charge['peak'] as $row){
		array_push($day, date("j",strtotime($row->date)));
		array_push($peak, $row->sum);
	}
	foreach ($charge['flat'] as $row){
		array_push($day, date("j",strtotime($row->date)));
		array_push($flat, $row->sum);
	}
	foreach ($charge['valley'] as $row){
		array_push($day, date("j",strtotime($row->date)));
		array_push($valley, $row->sum);
	}
	foreach ($charge['all'] as $row){
		array_push($day, date("j",strtotime($row->date)));
		array_push($all, $row->sum);
	}
	?>
	
	day = <?php echo json_encode($day,JSON_NUMERIC_CHECK)?> ;
	<?php 
			echo "peak = " . json_encode($peak,JSON_NUMERIC_CHECK) . ";";
			echo "flat = " . json_encode($flat,JSON_NUMERIC_CHECK) . ";";			
			echo "valley = " . json_encode($valley,JSON_NUMERIC_CHECK) . ";";
			echo "all = " . json_encode($all,JSON_NUMERIC_CHECK) . ";";
	?>
	
	peak_charge = <?=$charge["peak_charge"]?>;
	valley_charge = <?=$charge["valley_charge"]?>;
	flat_charge = <?=$charge["flat_charge"]?>;

	window.chart = new Highcharts.Chart({
			chart : {
				renderTo : 'point_chart',
				type: 'column',
				zoomType: 'x'
			},
            credits : {
                enabled : false
            }, 
			rangeSelector : {
				inputEnabled : false,
				selected : 5
			},
			title : {
				text : '<?=$charge['name'] . ' ' . lang("charge_report") ?>'
			},	
			subtitle : {
				text : '<?=$charge['year'] . "-" . $charge['month']?>'
			},	
			exporting : {
				url: '<?=site_url('export/index')?>',
				buttons : {
					printButton : {
						enabled : false
					}
				}
			},	
			xAxis: {
                categories: day
            },				
			yAxis: {
				title : {
					text: "<?=lang("yuan")?>"
				},
				minorTickInterval: 'auto',
				lineColor: '#000',
				lineWidth: 1,
				tickWidth: 1,
				tickColor: '#000'
	            
	        },	
	        tooltip: {
	            formatter: function() {
		           	var charge;
		           	if(this.series.name == "<?=lang('charge_peak_time')?>"){
			           	charge = <?=$charge['peak_charge']?>;
			        }
		           	else if(this.series.name == "<?=lang('charge_valley_time')?>"){
		           		charge = <?=$charge['valley_charge']?>;
			        }
		           	else if(this.series.name == "<?=lang('charge_flat_time')?>"){
		           		charge = <?=$charge['flat_charge']?>;
			        }
		           	else{
		           		charge = 1;	
			        }

                    return	' '  + '<b>' + this.x + + '<?=lang('charge_electric')?>''</b><br> ' + 
                    		' ' + this.series.name  +' : '+ (this.y * charge) + '<br>' + 
                    		' ' + '<?=lang('charge_electric_charge')?>' + ' : ' + charge + '<br>' +
                    		' ' + '<?=lang('charge_electric')?>' + ' : ' +  this.y;	 
  
            		         
	            }
	        },
	        series : [{
				name : '<?=lang('charge_peak_time')?>',
				data : peak,
				tooltip : {
					valueDecimals : 2
				}
			},{
				name : '<?=lang('charge_valley_time')?>',
				data : valley,
				tooltip : {
					valueDecimals : 2
				}
			},{
				name : '<?=lang('charge_flat_time')?>',
				data : flat,
				tooltip : {
					valueDecimals : 2
				}
			},{
				name : '<?=lang('charge_all_time')?>',
				data : all,
				tooltip : {
					valueDecimals : 2
				}
			}]
		});
	});
</script>

<?php 
$this->load->view('templates/footer');
?>
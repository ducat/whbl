<?php 
$this->load->view('templates/header', 
		array('title' => lang('charge') ));
?>
<body>
<?php
	$this->load->view('templates/page_top',
			array('user' => $user));
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php 
			$this->load->view('templates/side_menu', array(
					'active_id' => 'charge_table',
					'menu_map' => $menu_map));
			?>			
			
			<div class="tabbable span10" style="margin: 20px 1% auto;min-height: 600px">
				<ul class="nav nav-tabs">  
					<li class="active">
						<a href="#chart" class="mytab" data-toggle="tab"><?=lang('charge_chart')?></a>
					</li> 
					<li>
						<a href="#consumption_table" class="mytab" data-toggle="tab"><?=lang('charge_charge_list')?></a>
					</li>
					<li>
						<a href="#charge_table" class="mytab" data-toggle="tab"><?=lang('charge_electric_list')?></a>
					</li>
				</ul> 
				<div class="tab-content textbox-holder" style="overflow: visible; min-height: 550px" > 
					<div class="tab-pane" id = 'consumption_table'  style="width:98%; height:480px; margin:15px auto;">
						<table class="table table-striped table-bordered" id="example1">	
							<thead>
								<tr class="mythread">
									<th><?=lang('charge_date')?></th>
									<th><?=lang('charge_charge_peak')?></th>
									<th><?=lang('charge_charge_valley')?></th>
									<th><?=lang('charge_charge_flat')?></th>
									<th><?=lang('charge_charge_total')?></th>									
								</tr>
							</thead>	
							<tbody>
							<?php 
							$i=0;
							foreach ($charge['data'] as $key => $val){
								if($i % 2 ==0)
									echo '	<tr class="evenrow">';
								else
									echo '	<tr class="oddrow">';
								echo '		<td>' . $key . '</td>';			
								echo '		<td>' . ($val['peak'] * $charge['peak_charge']) . '</td>';
								echo '		<td>' . ($val['valley'] * $charge['valley_charge']) . '</td>';
								echo '		<td>' . ($val['flat'] * $charge['flat_charge']) . '</td>';
								echo '		<td>' . $val['all'] . '</td>';
								echo '	</tr>';
							}
							?>
							</tbody>
						</table>			
					</div>
					
					<div class="tab-pane" id = 'charge_table'  style="width:98%; height:480px; margin:15px auto;">
						<table class="table table-striped table-bordered" id="example2">	
							<thead>
								<tr class="mythread">
									<th><?=lang('charge_date')?></th>
									<th><?=lang('charge_electric_peak')?></th>
									<th><?=lang('charge_electric_valley')?></th>
									<th><?=lang('charge_electric_flat')?></th>
									<th><?=lang('charge_electric_total')?></th>									
								</tr>
							</thead>	
							<tbody>
							<?php 
							$i=0;
							foreach ($charge['data'] as $key => $val){
								if($i % 2 ==0)
									echo '	<tr class="evenrow">';
								else
									echo '	<tr class="oddrow">';
								echo '		<td>' . $key . '</td>';			
								echo '		<td>' . $val['peak'] . '</td>';
								echo '		<td>' . $val['valley'] . '</td>';
								echo '		<td>' . $val['flat'] . '</td>';
								echo '		<td>' . ($val['peak'] + $val['valley'] + $val['flat']) . '</td>';
								echo '	</tr>';
							}
							?>
							</tbody>
						</table>	
					</div>
					<div class="tab-pane active" id="chart" style="width:98%; height:480px; margin:15px auto;">
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="/assets/js/highcharts/highstock.js" type="text/javascript"></script>
<script src="/assets/js/highcharts/highcharts-more.js" type="text/javascript"></script>
<script src="/assets/js/highcharts/modules/exporting.js" type="text/javascript"></script>   
<script type="text/javascript">

$(".language").remove();
$(document).ready(function() {
	$('#example1, #example2').dataTable( {
		"aaSorting": [[0,'asc']],
		"sDom": "<' '<'span4'l><'span8'f>r>t<' '<'span4'i><'span8'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "<?=lang('per_page')?> _MENU_ <?=lang('record')?>",
			"sSearch": "<?=lang('search')?>:",
			"oPaginate":{
				"sPrevious": "<?=lang('previous')?>",
				"sNext": "<?=lang('next')?>",
			},	
			"sEmptyTable": "<?=lang('no_data')?>",
			"sZeroRecords": "<?=lang('no_match')?>",
			"sInfoEmpty": "<?=lang('show_empty')?>",
			"sInfoFiltered": "(<?=lang('total')?> _MAX_ <?=lang('record')?>)",			
			"sInfo": "<?=lang('show')?> _START_ - _END_ / <?=lang('total')?> _TOTAL_ <?=lang('record')?>"
		}
	} );
} );

$(function() {
	Highcharts.setOptions({     
	    global: {     
	        useUTC: false     
	    }     
	});
		
	<?php 
		$peak = array();
		$flat = array();
		$valley = array();
		$all = array();
		foreach ($charge['data'] as $key => $value){
			array_push($peak, array(strtotime($key) . "000", $value['peak'] * $charge['peak_charge']));
			array_push($flat, array(strtotime($key) . "000", $value['flat'] * $charge['flat_charge']));
			array_push($valley, array(strtotime($key) . "000", $value['valley'] * $charge['valley_charge']));
			array_push($all, array(strtotime($key) . "000", $value['all']));
		}
	
		echo "peak = " . json_encode($peak,JSON_NUMERIC_CHECK) . ";";
		echo "flat = " . json_encode($flat,JSON_NUMERIC_CHECK) . ";";			
		echo "valley = " . json_encode($valley,JSON_NUMERIC_CHECK) . ";";
		echo "all = " . json_encode($all,JSON_NUMERIC_CHECK) . ";";
	?>
	
	window.chart = new Highcharts.StockChart({
			chart : {
				renderTo : 'chart',
				type: 'column',
				zoomType: 'x'
			},
            credits : {
                enabled : false
            }, 
			rangeSelector : {
				inputEnabled : false,
				selected : 5
			},
			title : {
				text : '<?=$charge["name"] . ' ' . lang("charge_report_day") ?>'
			},	
			subtitle: {
				text : '<?=$charge['from'] ." ". lang('to') ." ". $charge['to'] ?>'
			},
			exporting : {
				url: '<?=site_url('export/index')?>',
				buttons : {
					printButton : {
						enabled : false
					}
				}
			},	
			xAxis: {
           //     categories: day
            },				
			yAxis: {
				title : {
					text: "<?=lang("yuan")?>"
				},
				minorTickInterval: 'auto',
				lineColor: '#000',
				lineWidth: 1,
				tickWidth: 1,
				tickColor: '#000'
	            
	        },	
	        tooltip: {
		       

	        },
			series : [{
				name : '<?=lang('charge_peak_time')?>',
				data : peak,
				tooltip : {
					valueDecimals : 2
				}
			},{
				name : '<?=lang('charge_valley_time')?>',
				data : valley,
				tooltip : {
					valueDecimals : 2
				}
			},{
				name : '<?=lang('charge_flat_time')?>',
				data : flat,
				tooltip : {
					valueDecimals : 2
				}
			},{
				name : '<?=lang('charge_all_time')?>',
				data : all,
				tooltip : {
					valueDecimals : 2
				}
			}]
		});
	});
</script>

<?php 
$this->load->view('templates/footer');
?>
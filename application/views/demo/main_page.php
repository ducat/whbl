<?php
$this->load->view('templates/header',
		array('title' => lang("sel_module") ));
?>
<body>
	<?php
		$this->load->view('templates/page_top',
				array('user' => $user));
	?>

	<div class="jumbotron">
		
		<!-- <h1>模块选择</h1> -->
		<div >
			<img src="/assets/img/dfk.jpg">
		</div>
		<br>
		<div class="container unit">
			<div class="row-fluid">
				<div class="span2 offset1">
					<p class="text-center">
						<img border="0" src="/assets/img/412_1363617257_8680114.jpg" class="img-rounded" style="width:80px;height:80px;"/>
					</p>
					<p class="text-center">
						<button class="btn btn-success" type="button" onclick="location.href='<?=site_url ("building_control/index")?>'"> <?=lang('BMS-BAS')?> </button>
					</p>
				</div>
				<div class="span2 offset2">
					<p class="text-center">
						<img border="0" src="/assets/img/images.jpg" class="img-rounded"  style="width:80px;height:80px;"/>
					</p>
					<p class="text-center" disabled>
						<button class="btn btn-success" type="button" onclick="alert('尚未启动');"> <?=lang('BMS-SCEURITY')?> </button>
					</p>
				</div>
				
				<div class="span2 offset2">
					<p class="text-center">
						<img border="0" src="/assets/img/2008070323240375.jpg" class="img-rounded" style="width:80px;height:80px;"/>
					</p>
					<p class="text-center">
						<button class="btn btn-success" type="button" onclick="alert('尚未启动');"> <?=lang('BMS-CCTV')?> </button>
					</p>
				</div>
				
			</div>
			<div style = 'margin-top:20px;'></div>
			<div class="row-fluid">
				<div class="span2 offset1">
					<p class="text-center">
						<img border="0" src="/assets/img/48bOOOPIC6b_202.jpg" class="img-rounded" style="width:80px;height:80px;"/>
					</p>
					<p class="text-center">
						<button class="btn btn-success" type="button" onclick="alert('尚未启动');"> <?=lang('BMS-ACCESS')?> </button>
					</p>
				</div>
				<div class="span2 offset2">
					<p class="text-center">
						<img border="0" src="/assets/img/w.jpg" class="img-rounded" style="width:80px;height:80px;"/>
					</p>
					<p class="text-center">
						<button class="btn btn-success" type="button" onclick="alert('尚未启动');"> <?=lang('BMS-CAS')?> </button>
					</p>
				</div>
				<div class="span2 offset2">
					<p class="text-center">
						<img border="0" src="/assets/img/bg.jpg" class="img-rounded" style="width:80px;height:80px;"/>
					</p>
					<p class="text-center">
						<button class="btn btn-success" type="button" onclick="alert('尚未启动');"> <?=lang('BMS-OA')?> </button>
					</p>
				</div>
			</div>
		</div>
	</div>
</body>

<?php
$this->load->view('templates/footer');
?>

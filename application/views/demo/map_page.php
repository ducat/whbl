<?php
$this->load->view('templates/header',
		array('title' => lang("sel_module") ));
?>
<body>
	<?php
		$this->load->view('templates/page_top',
				array('user' => $user));
	?>
	<br>
	<div class="container-fluid">
		<div class="span6">
			<img src="/assets/img/t01.png" style="BORDER-RIGHT: #ccccff 10px outset; BORDER-TOP: #ccccff 10px outset; BORDER-LEFT: #ccccff 10px outset; BORDER-BOTTOM: #ccccff 10px outset"/>
			<br>
			<br>


			<div class="span6">
				<div class="span1">
					<h4 style="font-color: #14799d; width: 100px">机房</h4>
				</div>
				<div class="span1">
					<h4 style="font-color: #14799d; width: 100px">温度</h4>
				</div>
				<div class="span1">
					<h4 style="font-color: #14799d; width: 100px">烟感</h4>
				</div>
				<div class="span1">
					<h4 style="font-color: #14799d; width: 100px">入侵报警</h4>
				</div>
			</div>
			<div class="span6">
				<div class="span1">
          <span class="label label-inverse">1号机房</span>
        </div>
				<div class="span1">
          <span class="label label-success">22.3</span>
          <span class="label label-success">23.1</span>
				</div>
				<div class="span1">
          <span class="label label-warning">警告</span>
          <span class="label label-warning">警告</span>
				</div>
				<div class="span1">
          <span class="label label-important">危险</span>
          <span class="label label-important">危险</span>
				</div>
			</div>
      <div class="span6">
				<div class="span1">
          <span class="label label-inverse">2号机房</span>
        </div>
				<div class="span1">
          <span class="label label-warning">43.3</span>
          <span class="label label-warning">45.1</span>
				</div>
				<div class="span1">
          <span class="label label-success">正常</span>
          <span class="label label-success">正常</span>
				</div>
				<div class="span1">
          <span class="label label-important">危险</span>
          <span class="label label-important">危险</span>
				</div>
			</div>
      <div class="span6">
				<div class="span1">
          <span class="label label-inverse">3号机房</span>
        </div>
				<div class="span1">
          <span class="label label-success">21.2</span>
          <span class="label label-success">22.6</span>
				</div>
				<div class="span1">
          <span class="label label-important">危险</span>
          <span class="label label-important">危险</span>
				</div>
				<div class="span1">
          <span class="label label-warning">警告</span>
          <span class="label label-warning">警告</span>
				</div>
			</div>
      <div class="span6">
				<div class="span1">
          <span class="label label-inverse">4号机房</span>
        </div>
				<div class="span1">
          <span class="label label-important">55.1</span>
          <span class="label label-important">60.0</span>
				</div>
				<div class="span1">
          <span class="label label-success">正常</span>
          <span class="label label-success">正常</span>
				</div>
				<div class="span1">
          <span class="label label-success">正常</span>
          <span class="label label-success">正常</span>
				</div>
			</div>
      <div class="span6">
				<div class="span1">
          <span class="label label-inverse">5号机房</span>
        </div>
				<div class="span1">
          <span class="label label-success">22.3</span>
          <span class="label label-success">23.1</span>
				</div>
				<div class="span1">
          <span class="label label-warning">警告</span>
          <span class="label label-warning">警告</span>
				</div>
				<div class="span1">
          <span class="label label-important">危险</span>
          <span class="label label-important">危险</span>
				</div>
			</div>
      <div class="span6">
				<div class="span1">
          <span class="label label-inverse">6号机房</span>
        </div>
				<div class="span1">
          <span class="label label-success">23.1</span>
				</div>
				<div class="span1">
          <span class="label label-warning">警告</span>
				</div>
				<div class="span1">
          <span class="label label-warning">警告</span>
				</div>
			</div>


		</div>

		<div class="span6">
			<img src="/assets/img/t02.png" style="BORDER-RIGHT: #ccccff 10px outset; BORDER-TOP: #ccccff 10px outset; BORDER-LEFT: #ccccff 10px outset; BORDER-BOTTOM: #ccccff 10px outset"/>
		</div>
	</div>
</body>

<?php
$this->load->view('templates/footer');
?>

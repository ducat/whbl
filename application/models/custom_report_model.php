<?php

class Custom_report_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function get_by_id($id = '') {
		if(empty($id)) {
			return FALSE;
		}
		$this->db->select('custom_report.id as id, username, display_name')
		->join('users', 'custom_report.creator_id = users.id','left')
		->where("custom_report.id", $id)
		->from('custom_report');
		$query = $this->db->get();
		return $query->row();
		

	}
	
	public function get_multi_point_by_report_id($id = '') {
	
		$this->db->select("point.id as point_id, point.display_name as display_name, point.chinese_name as chinese_name, unit")
		->from('custom_report')
		->join('custom_report_point_relation', 'custom_report.id = custom_report_point_relation.custom_report_id')
		->join('point', 'custom_report_point_relation.point_id = point.id')
		->where('custom_report_id', $id);
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_all_with_name() {
		$this->db->select('custom_report.id as id, display_name, username')
		->join('users', 'custom_report.creator_id = users.id', 'left')
		->from('custom_report');
		$query = $this->db->get();
		return $query->result();
	}
	

}

<?php
class Station_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get_station_info(){
		$this->db->select('*');
		$this->db->from('station');
		$this->db->where('id',0);
		
		$query=$this->db->get();
		return $query->row();
	}

	function get_sc_info () {
		$this->db->select('name, ip_address');
		$this->db->from('bcu_devices');
		$this->db->where('type', '1');

		$query=$this->db->get();
		return $query->result_array ();
	}
	
	function initialized(){
		$this->db->select('initialized');
		$this->db->from('station');
		$this->db->where('id',0);
	
		$query=$this->db->get();
		return $query->row()->initialized;
	}
	
	function set_initialized(){
		$this->db->where("id", 0);
		$this->db->update('station',array("initialized" => 1));
		
		return true;
	}

	
}
?>
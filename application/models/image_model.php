<?php

class Image_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function add($image) {
		return $this->db->insert('image', $image);
	}
	
	public function update_image_size($id, $w, $h) {
		$this->db->set('image_width', $w)
				 ->set('image_height', $h)
				 ->where('id', $id)
				 ->update('instrument_image_relation');
		return true;
	}
	
	public function update_image_group_size($id, $w, $h) {
		$this->db->set('img_group_width', $w)
				 ->set('img_group_height', $h)
				 ->where('id', $id)
				 ->update('instrument_img_group_relation');
		return true;
	}
	
	public function get_one_by_image_url($url) {
		$this->db->where('url', $url);
		$query = $this->db->get('image');
		return $query->row();
	}
}

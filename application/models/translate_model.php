<?php
class Translate_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function empty_translate(){
		$this->db->empty_table('translate');
		return true;
	}

	function insert_into_translate($array){
		$this->db->insert_batch('translate', $array); 
		return true;
	}

	function get_point_info_by_name($name){
		$this->db->select('*');
		$this->db->where('display_name', $name);
		$query = $this->db->get('translate');
		return $query->row();
	}
	
}

<?php
class Point_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		
	}

	
	function update_point_location($id, $x, $y) {
		$this->db->set('coordinate_x', $x)
				->set('coordinate_y', $y)
				->where('id', $id)
				->update('point');
		return true;
	}
	
	function update_point_info($array, $id) {
		$this->db->where('id', $id)
				 ->update('point', $array);
		return true;
	}
	

	function get_point_and_name()
	{
		$this->db->select(array('id','display_name'));
		$this->db->from('point');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_point_and_name_by_array($arr)
	{
		$this->db->select('id, display_name, chinese_name, english_name');
		$this->db->or_where_in('id',$arr);
		$this->db->from('point');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_all_points()
	{
		$this->db->select('*');
		$this->db->from('point');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_all_non_virtual_points()
	{
		$this->db->select('*');
		$this->db->where('is_virtual_point', 0);
		$this->db->or_where('is_virtual_point');
		$this->db->from('point');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_all_bacnet_points()
	{
		$this->db->select('*');
		$this->db->where('is_bacnet', 1);
		$this->db->from('point');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_bacnet_points_need_trans() //需要翻译的点位 display为空
	{
		$this->db->select('*');
		$this->db->where('is_bacnet', 1);
		$this->db->where('display_name');
		$this->db->from('point');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_points_by_type($type="transient")
	{
		$this->db->select('*');
		$this->db->from('point');
		$this->db->where('type_large', $type);
		$query=$this->db->get();
		return $query->result();
	}
	
	function is_temprature($id)
	{
		$this->db->select('*');
		$this->db->from('point');
		$this->db->where('id', $id);
		$query=$this->db->get();
		$result = $query->row();
		return ($result->type_small == 'Temperature');
	}
	
	function get_small_type_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('point');
		$this->db->where('id', $id);
		$query=$this->db->get();
		$result = $query->row();
		return $result == null? null : $result->type_small;
	}
	
	function get_unit_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('point');
		$this->db->where('id', $id);
		$query=$this->db->get();
		$result = $query->row();
		return $result->unit;
	}
	
	function get_type($id)
	{
		$this->db->select('*');
		$this->db->from('point');
		$this->db->where('id', $id);
		$query=$this->db->get();
		$result = $query->row();
		return $result == null? "null" : $result->type;
	}
	
	function get_all_alarm()
	{
		$this->db->select('*');
		$this->db->from('point');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_id_by_name($name){

		$this->db->select('id');
		$this->db->from('point');
		$this->db->where('display_name',$name);
		$query=$this->db->get();
		return $query->row() == null? null : $query->row()->id;
	}
	
	
	
	
	function get_chiller_current(){
		$this->db->select('*');
		$this->db->like('display_name', 'CH', 'after');
		$this->db->like('display_name', '_CURRENT', 'before');
		$this->db->from('point');
		$query = $this->db->get();
		$result = $query->result();
		$result_regex = array();
		foreach ($result as $obj){
			$regex = '#^CH[0-9]+_CURRENT$#';
			$str = $obj->display_name;
			$matches = array();
			if(preg_match($regex, $str, $matches)){
				array_push($result_regex, $obj);
			}
		}
		return $result_regex;
		
	}
	
	function get_chiller_current_st(){
		$this->db->select('*');
		$this->db->like('display_name', 'CH', 'after');
		$this->db->like('display_name', '_ST', 'before');
		$this->db->from('point');
		$query = $this->db->get();
		$result = $query->result();
		$result_regex = array();
		foreach ($result as $obj){
			$regex = '#^CH[0-9]+_ST$#';
			$str = $obj->display_name;
			$matches = array();
			if(preg_match($regex, $str, $matches)){
				array_push($result_regex, $obj);
			}
		}
		return $result_regex;
	}
	
	function get_chiller_flw(){
		$this->db->select('*');
		$this->db->where('display_name', 'CHW_FLW');
		$this->db->from('point');
		$query = $this->db->get();
		$result = $query->row();
		return $result == null? null : $result->id;
	}
	
	function get_chiller_swt(){
		$this->db->select('*');
		$this->db->where('display_name', 'CHW_SWT');
		$this->db->from('point');
		$query = $this->db->get();
		$result = $query->row();
		return $result == null? null : $result->id;
	}
	
	function get_chiller_rwt(){
		$this->db->select('*');
		$this->db->where('display_name', 'CHW_RWT');
		$this->db->from('point');
		$query = $this->db->get();
		$result = $query->row();
		return $result == null? null : $result->id;
	}
	
	function get_all_point_by_instrument_id($instrument_id) {
		if(empty($instrument_id)) {
			return null;
		}
	
		$this->db->select('point.*, instrument_img_group_relation.instrument_id, instrument_img_group_relation.id as relation_id')
		->from('instrument_img_group_relation')
		->join('point', 'point.id = instrument_img_group_relation.related_point_id', 'left')
		->where('instrument_id', $instrument_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_virtual_point_by_instrument_id($instrument_id) {
		if(empty($instrument_id)) {
			return null;
		}
	
		$this->db->select('point.*, instrument_img_group_relation.instrument_id, instrument_img_group_relation.id as relation_id, instrument_img_group_relation.dynamic_rule, instrument_img_group_relation.dynamic_value')
		->from('instrument_img_group_relation')
		->join('point', 'point.id = instrument_img_group_relation.related_point_id', 'left')
		->where('instrument_id', $instrument_id)
		->where('is_virtual_point', 1);
		$query = $this->db->get();
		return $query->result();
	}
	
	
	function get_point_list_by_instrument_id($instrument_id){
		$point_list = array(-1);
		if(empty($instrument_id)) {
			return $point_list;
		}
		$this->db->select('point_id')
				 ->from('instrument_point_relation')
				 ->join('point', 'point.id = instrument_point_relation.point_id' )
				 ->where('instrument_id', $instrument_id)
				 ->where('is_virtual_point', 0)
			     ->or_where('is_virtual_point');
		$query = $this->db->get();
		$result = $query->result();
		foreach($result as $row){
			array_push($point_list, $row->point_id);
		}
		return $point_list;
	}
	
	function get_non_virtual_point_latest_data_by_instrument_id($instrument_id) {
		if(empty($instrument_id)) {
			return null;
		}
	
		$latest_batch = self::get_latest_batch()->batch;
		$point_list = self::get_point_list_by_instrument_id($instrument_id);
		
		$this->db->select('instrument_point_relation.point_id, point_data.value, point_data.timestamp, point_data.batch, instrument_point_relation.id, point.display_name')
				 ->from('point_data')
				 ->where('batch', $latest_batch)
				 ->where_in('point_data.point_id',$point_list)
				 ->join('point', 'point_data.point_id = point.id')
				 ->join('instrument_point_relation', 'point_data.point_id = instrument_point_relation.point_id' );
				 
		
		$query = $this->db->get();
		return $query->result();
	}
	
	
	function get_dynamic_point_list_by_instrument_id($instrument_id){
		$point_list = array(-1);
		if(empty($instrument_id)) {
			return $point_list;
		}
		$this->db->select('related_point_id')
				 ->from('instrument_img_group_relation')
				 ->join('point', 'point.id = instrument_img_group_relation.related_point_id' )
				 ->where('instrument_id', $instrument_id)
				 ->where('is_virtual_point', 0)
			     ->or_where('is_virtual_point');
		$query = $this->db->get();
		$result = $query->result();
		foreach($result as $row){
			array_push($point_list, $row->related_point_id);
		}
		return $point_list;
	}

	function get_dynamic_point_data_by_instrument_id($instrument_id) {
		if(empty($instrument_id)) {
			return null;
		}
	
		$latest_batch = self::get_latest_batch()->batch;
		$point_list = self::get_dynamic_point_list_by_instrument_id($instrument_id);

		
		$this->db->select('instrument_img_group_relation.related_point_id as point_id, point_data.value, point_data.timestamp, point_data.batch, instrument_img_group_relation.id, instrument_img_group_relation.dynamic_rule, instrument_img_group_relation.dynamic_value, point.display_name, point.type')
		->from('point_data')
		->where('batch', $latest_batch)
		->where_in('point_data.point_id',$point_list)
		->join('point', 'point_data.point_id = point.id' )
		->join('instrument_img_group_relation', 'point_data.point_id = instrument_img_group_relation.related_point_id');
		$query = $this->db->get();
		return $query->result();
	}
	
	


	function get_latest_batch() {
		$this->db->select_max('batch')
				->where('batch >', 0)
				->from('point_data');
		$query = $this->db->get();
		return $query->row();
	}
	
	
	//获得点信息，是否是虚拟点，以及表达式
	function get_point_info($id = 0){
		if(empty($id)) {
			return null;
		}
		$this->db->select('*')
				 ->from('point')
				 ->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	//获取id点，batch批次号的value,如果没有查到 数据则返回null
	function get_point_data($id, $batch){
		if(empty($id)) {
			return null;
		}
		$this->db->select('value')->from('point_data')
			->where('point_id',$id)
			->where('batch',$batch);
		$query = $this->db->get();
		return $query->row();
	}
	
	//将输入的表达式存入point表
	function update_expression($id,$expjson,$exp_display){
		//echo("abc");
		$dt=array('expression' => $expjson , 'expression_display' => $exp_display);
		$this->db->from('point');
		$this->db->where('id',$id);
		$this->db->update('point',$dt);
	}
	
	//将输入的表达式存入alarm表
	function update_alarm_expression($id,$expjson,$exp_display){
		$dt=array('expression' => $expjson,
				  'expression_display' => $exp_display);
		$this->db->from('alarm');
		$this->db->where('id',$id);
		$this->db->update('alarm',$dt);		
	}
	
	//查找point_data表中的最大批次号
	function get_max_batch(){
		$this->db->select('max(batch)')
			     ->from('point_data');
		$this->db->where('batch >', 0);
		$query=$this->db->get();
		return $query->result();		
		
	}
	
	//将id点位batch批次号的数据value存入point_data表
	function insert_point_data($id,$batch,$value){
		$data=array('point_id' => $id, 'batch' => $batch, 'value' => $value);
		$this->db->insert('point_data', $data);
	}
	
	//将报警信息存入alarmlog表
	function insert_alarm_log($alarmid){
		$timestamp=now();
		echo($timestamp);
		$data=array('alarmid' => $alarmid,'timestamp' => $timestamp);
		$this->db->insert('alarmlog',$data);
	}
    
	//返回alarm表id及表达式的键值对数组
	function alarm_key_value(){
		$akv=array();
		$this->db->select('id, expression')->from('alarm');
		$query=$this->db->get();
		$result=$query->result();
		foreach($result as $row){
			$akv[$row->id] = $row->expression;
		}
		return $akv;
	}
	
	
	//查找point表中虚拟点位，并返回id，表达式键值对数组
	function pid_exp_key_value(){
		$pekv=array();
		$this->db->select('id, expression')->from('point')
		->where('is_virtual_point')
		->or_where('is_virtual_point',1);
		$query=$this->db->get();
		$result=$query->result();
		foreach($result as $row){
			if($row->expression!=null){
				$pekv[$row->id]=$row->expression;
			}
		}
		return $pekv;
	}
	//价差point_data表中id点位batch批次的数据是否已经写入，如果已写入返回result，否则返回null
	function check_value_exist($id,$batch){
		$this->db->select('value')->from('point_value')->where('$batch',$batch)
		->where('point_id',$id);
		$query=$this->db->get();
		$result=$query->result();
		return $result;
	}
}
?>
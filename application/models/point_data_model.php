<?php
class Point_data_model extends CI_Model
{
	private $querys_per_hour = 12;
	function __construct()
	{
		parent::__construct();
		$this->querys_per_hour = $this->get_querys_per_hour();
	}
	
	function get_querys_per_hour(){
		$this->db->select("query_interval");
		$this->db->from('collector_configs');
		$query=$this->db->get();
		$result = $query->row() != null ? $query->row()->query_interval : 300000;
		if($result <= 0)
			$result = 300000;
		return 3600000 / $result;
	}

	function  get_min_batch($point_id){
		$this->db->select_min('batch');
		$this->db->where('batch >', 0);
		$this->db->where('point_id',$point_id);
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->row()->batch > 0? $query->row()->batch:0;
	}
	
	function  get_max_batch($point_id){
		$this->db->select_max('batch');
		$this->db->where('batch >', 0);
		$this->db->where('point_id',$point_id);
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->row()->batch > 0? $query->row()->batch:0;
	}
	
	function  get_min_batch_after_start($point_id, $start="2008-01-01"){
		$this->db->select_min('batch');
		$this->db->where('batch >', 0);
		$this->db->where('timestamp >=', $start. " 00:00:00");
		$this->db->where('point_id',$point_id);
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->row()->batch > 0? $query->row()->batch:0;
	}
	
	function  get_max_batch_before_end($point_id, $end="2008-01-01"){
		$this->db->select_max('batch');
		$this->db->where('batch >', 0);
		$this->db->where('timestamp <=', $end. " 23:59:59");
		$this->db->where('point_id',$point_id);
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->row()->batch > 0? $query->row()->batch:0;
	}
	
	function get_count_batch_start_end($point_id, $start = 0, $end = 0){
		$this->db->select('count(distinct(batch)) as count');
		$this->db->where('batch >', 0);
		$this->db->where('batch >=', $start);
		$this->db->where('batch <=', $end);
		$this->db->where('point_id',$point_id);
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->row()->count;
	}
	
	function  get_time_range($point_id){
		$this->db->select('max(timestamp) as max, min(timestamp) as min');
		$this->db->where('point_id',$point_id);
		$this->db->from('point_data');
		$query=$this->db->get();
		$range["max"] = $query->row()->max;
		$range["min"] = $query->row()->min;
		return $range;
	}
	
	function  get_time_year_month($point_id){

		$sql = 	"SELECT DISTINCT year(timestamp) as year , month(timestamp) as month " .
				"FROM point_data  WHERE point_id = ? ORDER BY year,month ";
		
		$query = $this->db->query($sql, array($point_id));
		return $query->result()==NULL ? array((Object)array("year"=>1970, "month"=>1)) : $query->result();
		
	}
	
	
	function  get_time_by_batch($batch){
		$this->db->select('max(timestamp) as time');
		$this->db->where('batch',$batch);
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->row()->time;
	}
	

	function  get_min_batch_by_date($point_id, $date){
		$this->db->select_min('batch');
		$this->db->where('point_id',$point_id);
		$this->db->where('timestamp >=', $date . " 00:00:00");
		$this->db->where('timestamp <' , $date . " 24:00:00");
		$this->db->where('batch >', 0);
		$this->db->from('point_data');
		$query=$this->db->get();
		$batch = $query->row()->batch;
		if ($batch == null)
			return -1;
		else
			return $batch;
	}
	
	function  get_max_batch_by_date($point_id , $date){
		$this->db->select_max('batch');
		$this->db->where('point_id',$point_id);
		$this->db->where('timestamp >=', $date . " 00:00:00");
		$this->db->where('timestamp <' , $date . " 24:00:00");
		$this->db->where('batch >', 0);
		$this->db->from('point_data');
		$query=$this->db->get();
		$batch = $query->row()->batch;
		if ($batch == null)
			return -1;
		else
			return $batch;
	
	}
	
	
	function get_data_by_point_id($point_id,$start,$end){
		$this->db->select('*');
		$this->db->where('point_id',$point_id);
		$this->db->where('batch >', 0);
		$this->db->where('batch <=', $end);
		$this->db->where('batch >=', $start);
		$this->db->from('point_data');
		$this->db->order_by('batch', 'asc');
		
		$query=$this->db->get();
		return $query->result();
	}
	
	function delete_data_by_point_id($point_id){
		$this->db->delete('point_data', array('point_id' => $point_id));
	}	
	
	function insert_virtual($data){
		$this->db->insert('point_data', $data);
	}
	
	function get_data_by_point_id_time_range($point_id,$start,$end,$point_type){
		if($point_type == 'binary'){
			$this->db->select('timestamp,value');
		}
		else if($point_type == 'accumulated'){
			$this->db->select('timestamp,delta as value');
		}
		else if($point_type == 'transient'){
			$this->db->select('timestamp,value');
		}
		else {
			$this->db->select('timestamp,value');
		}
		
		$this->db->where('point_id',$point_id);
		$this->db->where('timestamp <=', $end);
		$this->db->where('timestamp >=', $start);
		$this->db->from('point_data');
		$this->db->order_by('id', 'asc');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_hour_data_for_runtime($point_id,$start,$end,$point_type){ //for runtime
		if($point_type == 'binary'){
			$sql = 	"SELECT timestamp, max(value) as value FROM point_data " .
					" WHERE point_id = ? AND timestamp <= ? AND timestamp >= ? " .
					" GROUP BY DATE_FORMAT(timestamp, '%Y-%m-%d-%h') " .
					" ORDER BY timestamp asc";
			
		}
		else if($point_type == 'accumulated'){
			$sql = 	"SELECT timestamp, max(delta) as value FROM point_data " .
					" WHERE point_id = ? AND timestamp <= ? AND timestamp >= ? " .
					" GROUP BY DATE_FORMAT(timestamp, '%Y-%m-%d-%h') " .
					" ORDER BY timestamp asc";
				
		}
		else{
			$sql = 	"SELECT timestamp, avg(value) as value FROM point_data " .
					" WHERE point_id = ? AND timestamp <= ? AND timestamp >= ? " .
					" GROUP BY DATE_FORMAT(timestamp, '%Y-%m-%d-%h') " .
					" ORDER BY timestamp asc";
		}

		
		$query = $this->db->query($sql, array($point_id, $end, $start));
		return $query->result();
		
	}
	
	function get_day_data_for_runtime($point_id,$start,$end,$point_type){ //for runtime
		if($point_type == 'binary'){
			$this->db->select('timestamp, max(value) as value');
		}
		else if($point_type == 'accumulated'){
			$this->db->select('timestamp, avg(delta) as value');
		}
		else if($point_type == 'transient'){
			$this->db->select('timestamp, avg(value) as value');
		}
		else{
			$this->db->select('timestamp, avg(value) as value');
		}
		//$this->db->select('timestamp, avg(value) as value');
		$this->db->where('point_id',$point_id);
		$this->db->where('timestamp <=', $end);
		$this->db->where('timestamp >=', $start);
		$this->db->group_by("date(timestamp)");
		$this->db->order_by('timestamp', 'asc');
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_day_data_by_point_id_time_range($point_id,$start,$end,$point_type){
		if($point_type == "transient"){
			$this->db->select('max(value) as max, min(value) as min, avg(value) as avg, sum(value) as sum,
									date(timestamp) as date');
		}
		elseif ($point_type == "accumulated"){
			$this->db->select('max(delta) as max, min(delta) as min, avg(delta) as avg, sum(delta) as sum,
									date(timestamp) as date');
		}
		elseif ($point_type == "binary"){
			$this->db->select('max(value) as max, min(value) as min, avg(value) as avg, sum(value) / '.$this->querys_per_hour.' as sum,
									date(timestamp) as date');
		}
		else
		{
			$this->db->select('max(value) as max, min(value) as min, avg(value) as avg, sum(value) as sum,
									date(timestamp) as date');
		}
		$this->db->where('point_id',$point_id);
		$this->db->where('timestamp <=', $end." 23:59:59");
		$this->db->where('timestamp >=', $start." 00:00:00");
		$this->db->group_by("date(timestamp)");
		$this->db->order_by('timestamp', 'asc');
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_month_data_by_point_id_month($point_id,$year,$month,$point_type){
		if($point_type == "transient"){
			$this->db->select('max(value) as max, min(value) as min, avg(value) as avg, sum(value) as sum,
									date(timestamp) as date');
		}
		elseif ($point_type == "accumulated"){
			$this->db->select('max(delta) as max, min(delta) as min, avg(delta) as avg, sum(delta) as sum,
									date(timestamp) as date');
		}
		elseif ($point_type == "binary"){
			$this->db->select('max(value) as max, min(value) as min, avg(value) as avg, sum(value) / '.$this->querys_per_hour.' as sum,
									date(timestamp) as date');
		}
		else
		{
			$this->db->select('max(value) as max, min(value) as min, avg(value) as avg, sum(value) as sum,
									date(timestamp) as date');
		}
		
		$this->db->where('point_id',$point_id);
		$this->db->where('year(timestamp)', $year);
		$this->db->where('month(timestamp)', $month);
		$this->db->group_by("day(timestamp)");
		$this->db->order_by('timestamp', 'asc');
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_year_data_by_point_id_year($point_id, $year, $point_type){
		if($point_type == "transient"){
			$this->db->select('max(value) as max, min(value) as min, avg(value) as avg, sum(value) as sum,
									date(timestamp) as date');			
		}
		elseif ($point_type == "accumulated"){
			$this->db->select('max(delta) as max, min(delta) as min, avg(delta) as avg, sum(delta) as sum,
									date(timestamp) as date');
		}
		elseif ($point_type == "binary"){
			$this->db->select('max(value) as max, min(value) as min, avg(value) as avg, sum(value) / '.$this->querys_per_hour.' as sum,
									date(timestamp) as date');
		}
		else 
		{
			$this->db->select('max(value) as max, min(value) as min, avg(value) as avg, sum(value) as sum,
									date(timestamp) as date');			
		}		
		$this->db->where('point_id',$point_id);
		$this->db->where('year(timestamp)', $year);
		$this->db->group_by("month(timestamp)");
		$this->db->order_by('timestamp', 'asc');
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_base_by_point_id($point_id,$type,$start,$end){
		$this->db->select($type."(value) as ".$type);
		$this->db->where('point_id',$point_id);
		$this->db->where('timestamp <=', $end . " 23:59:59");
		$this->db->where('timestamp >=', $start . " 00:00:00");
		$this->db->from('point_data');
		$query=$this->db->get();
		return $query->row()->$type;
	}
	
	function get_out_temperature(){
		$this->db->select("value as val");		
		$this->db->join("point","point.id = point_data.point_id");
		$this->db->from('point_data');
		$this->db->where("point.expression","OA_T");
		$this->db->order_by("point_data.timestamp","desc");
		$this->db->limit(1);
		$query=$this->db->get();
		return $query->row() == null? null : $query->row()->val;
	}
	
	function get_out_humidity(){
		$this->db->select("value as val");
		$this->db->join("point","point.id = point_data.point_id");
		$this->db->from('point_data');
		$this->db->where("point.expression","OA_H");
		$this->db->order_by("point_data.timestamp","desc");
		$this->db->limit(1);
		$query=$this->db->get();
		return $query->row() == null? null : $query->row()->val;
	}
	
	function get_season(){
		$this->db->select("value as val");
		$this->db->join("point","point.id = point_data.point_id");
		$this->db->from('point_data');
		$this->db->where("point.expression","MOD_AOP_ST");
		$this->db->order_by("point_data.timestamp","desc");
		$this->db->limit(1);
		$query=$this->db->get();
		return $query->row() == null? null : $query->row()->val;
	}
}
?>
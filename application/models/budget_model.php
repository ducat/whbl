<?php
class Budget_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get_budget_by_year($year = 1970){
		$this->db->select('*');
		$this->db->where('year',$year);
		$this->db->from('budget');
		$query=$this->db->get();
		return $query->row();
	}
	
	function get_budget_by_id($id = 0){
		$this->db->select('*');
		$this->db->where('id',$id);
		$this->db->from('budget');
		$query=$this->db->get();
		return $query->row();
	}
	
	function get_all_budget(){
		$this->db->select('*');
		$this->db->from('budget');
		$query=$this->db->get();
		return $query->result();
	}

	
}
?>
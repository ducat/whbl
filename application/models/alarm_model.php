<?php
class Alarm_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get_info_by_id($id){
		$this->db->select("*");
		$this->db->where("id", $id);
		$this->db->from('alarm');
		$query=$this->db->get();
		return $query->row();
	}

	
}

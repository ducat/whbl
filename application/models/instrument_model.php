<?php

class Instrument_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function add_instrument_image($instrument_image) {
		return $this->db->insert('instrument_image', $instrument_image);
	}
	
	public function delete_instrument_image($id, $instrument_id) {
		$this->db->where('id', $id)
				->delete('instrument_image');
		return $this->db->set('image_url', null)
				->where('id', $instrument_id)
				->update('instrument');
	}
	
	public function delete_instrument_point($id) {
		return $this->db->where('id', $id)
						->delete('instrument_point_relation');
	}
	
	public function update_or_insert_instrument_image_by_instrument_id($instrument_id, $instrument_image) {
		$this->db->where('instrument_id', $instrument_id);
		$query = $this->db->get('instrument_image');
		if(!$query->row()) {
			return self::add_instrument_image($instrument_image);
		} else {
			return $this->db->where('instrument_id', $instrument_id)
							->update('instrument_image', $instrument_image);
		}
	}
	
	public function update_instrument_point_location($id, $x, $y) {
		$this->db->set('related_point_x', $x)
		->set('related_point_y', $y)
		->where('id', $id)
		->update('instrument_point_relation');
		return true;
	}
	
	public function update_instrument_image_loaction($id, $x, $y) {
	
		$this->db->set('image_x', $x)
		->set('image_y', $y)
		->where('id', $id)
		->update('instrument_image_relation');
		return true;
	}
	
	public function update_instrument_group_loaction($id, $x, $y) {
	
		$this->db->set('img_group_x', $x)
		->set('img_group_y', $y)
		->where('id', $id)
		->update('instrument_img_group_relation');
		return true;
	}
	
	
	
	public function get_by_id($id = '') {
		if(empty($id)) {
			return null;
		}
		
		$query = $this->db->get_where('instrument', array('id' => $id));
		return $query->row();
	}
	
	public function get_image_by_inst_id($id = '') {
		if(empty($id)) {
			return null;
		}
	
		$this->db->select('instrument_image_relation.*, image.url, image.display_name')
		->from('instrument_image_relation')
		->join('image', 'image.id = instrument_image_relation.image_id')
		->where('instrument_id', $id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_img_group_by_inst_id($id = '') {
		if(empty($id)) {
			return null;
		}
	
		$this->db->select('instrument_img_group_relation.*, image_group.static_url, image_group.dynamic_url, image_group.display_name')
		->from('instrument_img_group_relation')
		->join('image_group', 'image_group.id = instrument_img_group_relation.img_group_id')
		->where('instrument_id', $id);
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_point_by_instrument_id($instrument_id) {
		if(empty($instrument_id)) {
			return null;
		}
	
		$this->db->select('instrument_point_relation.*, point.display_name, point.chinese_name, point.english_name, point.type, point.unit')
		->from('instrument_point_relation')
		->join('point', 'point.id = instrument_point_relation.point_id')
		->where('instrument_id', $instrument_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_all_with_name() {
		$this->db->select('id, name, sequence')
				->order_by('sequence','asc')
				->from('instrument');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_next_inst($id = 0) {
		$this->db->select('*')
		->order_by('sequence','asc')
		->order_by('id','asc')
		->from('instrument');
		$query = $this->db->get();
		$inst_list = $query->result();
	
		$i = 0;
		for($i; $i < count($inst_list); $i++){
			$inst = $inst_list[$i];
			if($inst->id == $id)
				break;
		}
		$result = $inst_list[($i+1)%count($inst_list)];
		return $result->id;
	}
}

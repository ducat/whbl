<?php
class Cmd_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function insert_cmd($point_id = 0, $cmd_type = 0, $priority = 16, $value = 0){
		$data = array(
               	'point_id' => $point_id,
               	'value' => $value,
				'status' => 0,
				'command_type' => $cmd_type,
				'priority' => $priority
            );
		$this->db->insert('setting_queues', $data); 
		return true;
	}
	
	function get_info($point_id = 0){
		$this->db->select("*");
		$this->db->where("point_id", $point_id);
		$this->db->from('setting_queues');
		$this->db->order_by("id", "desc");
		$query=$this->db->get();
		return $query->row();
	}
}

<?php
class Alarm_point_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get_info_by_id($id){
		$this->db->select("*");
		$this->db->where("id", $id);
		$this->db->from('alarm');
		$query=$this->db->get();
		return $query->row();
	}
	public function get_alarm_point ()
	{
		$query = $this->db->query ("Select * from point where id != 0");
		return $query->result_array ();
	}

	public function get_alarm_point_detail ($point_id)
	{
		$query = $this->db->query ("Select * from point where id = " . $point_id);
		return $query->result_array ();
	}

	public function get_point_type_small () {
		$id = $_GET['id'];

		$ret = $this->db->query ("Select type_small from point where id = '" . $id . "';")->row();
		return $ret->type_small;
	}

	public function detail_update () {
		$id = $_POST ['id'];
		$display_name = $_POST['display_name'];
		$chinese_name = $_POST['chinese_name'];
		$english_name = $_POST['english_name'];
	//	$is_virtual_point = $_POST['is_virtual_point'];
	//	$is_bacnet = $_POST['is_bacnet'];
	//	$type = $_POST['type'];
	//	$type_large = $_POST['type_large'];
	//	$type_small = $_POST['type_small'];
	//	$serve_sys = $_POST['serve_sys'];
	//	$serve_obj = $_POST['serve_obj'];
	//	$unit = $_POST['unit'];
	//	$source_port = $_POST['source_port'];
	//	$offset = $_POST['offset'];
	//	$expression = $_POST['expression'];
	//	$expression_display = $_POST['expression_display'];
	//	$timestamp = $_POST['timestamp'];
	//	$deleted = $_POST['deleted'];
		$alarm_high = $_POST['alarm_high'];
		$alarm_low = $_POST['alarm_low'];

		$alarm_high_summer = isset($_POST['alarm_high_summer']) ? intval($_POST['alarm_high_summer']) : -1;
		$alarm_low_summer = isset($_POST['alarm_low_summer']) ? intval($_POST['alarm_low_summer']) : -1;
		$alarm_high_winter = isset($_POST['alarm_high_winter']) ? intval($_POST['alarm_high_winter']) : -1;
		$alarm_low_winter = isset($_POST['alarm_low_winter']) ? intval($_POST['alarm_low_winter']) : -1;

	//	$alarm_attack = $_POST['alarm_attack'];
	//	$alarm_release = $_POST['alarm_release'];
	//	$alarm_filter_start = $_POST['alarm_filter_start'];
	//	$alarm_filter_end = $_POST['alarm_filter_end'];
	//	$work_filter = $_POST['work_filter'];
	//	$mail_filter = $_POST['work_filter'];
		$alarm_level = $_POST['alarm_level'];
	//	$byte = $_POST['byte'];
	//	$bcu_device_id = $_POST['bcu_device_id'];
	//	$proper_value_high = $_POST['proper_value_high'];
	//	$proper_value_low = $_POST['proper_value_low'];
	//	$modbus_id = $_POST['modbus_id'];

	//	print_r($_POST);
		$this->db->query ("Update point set display_name = '" . $display_name . 
										"', chinese_name = '" . $chinese_name . 
										"', english_name = '" . $english_name .
							//			"', is_virtual_point = '" . $is_virtual_point .
							//			"', is_bacnet = '" . $is_bacnet .
							//			"', type = '" . $type . 
							//			"', type_large = '". $type_large . 
							//			"', type_small = '". $type_small .
							//			"', serve_sys = '" . $serve_sys .
							//			"', serve_obj = '" . $serve_obj .
							//			"', unit = '" . $unit .
							//			"', source_port = '" . $source_port .
							//			"', offset = '" . $offset . 
							//			"', expression = '" . $expression . 
							//			"', expression_display = '" . $expression_display .
							//			"', timestamp = '" . $timestamp .
							//			"', deleted = '" . $deleted .
										"', alarm_high = '". $alarm_high .
										"', alarm_low = '". $alarm_low .
										"', alarm_high_summer = '" . $alarm_high_summer .
										"', alarm_low_summer = '" . $alarm_low_summer .
										"', alarm_high_winter = '" . $alarm_high_winter .
										"', alarm_low_winter = '" . $alarm_low_winter .
							//			"', alarm_attack = '" . $alarm_attack .
							//			"', alarm_release = '" . $alarm_release .
							//			"', alarm_filter_start = '". $alarm_filter_start .
							//			"', alarm_filter_end = '". $alarm_filter_end .
							//			"', work_filter = '" . $work_filter .
							//			"', mail_filter = '" . $mail_filter .
										"', alarm_level = '" . $alarm_level .
							//			"', byte = '" . $byte .
							//			"', proper_value_high = '" . $proper_value_high .
							//			"', proper_value_low = '" . $proper_value_low . 
							//			"', modbus_id = '" . $modbus_id .
										"' where id = " . $id . ";");
	}

	public function alarm_log_update () {
		$logs_id = $_POST['logs_id'];
		$description = $_POST['description'];

		if (current_lang() == 'en') {
			$this->db->query ("Update logs set description='" . $description . "' where id = " . $logs_id . ";");
		}
		else {
			$this->db->query ("Update logs set chinese_description='" . $description . "' where id = " . $logs_id . ";");
		}
	}
	
}
?>
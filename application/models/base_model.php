<?php
class Base_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function update_base_value($id, $value){
		$this->db->set('value', $value)
			->where('id', $id)
			->update('base');
		return true;
	}
	
	function get_bases()
	{
		$this->db->select('*');
		$this->db->from('base');
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_base_by_array($arr)
	{
		$this->db->select('*');
		$this->db->or_where_in('id',$arr);
		$this->db->from('base');
		$query=$this->db->get();
		return $query->result();
	}

	
}
?>
<?php
class Alarm_log_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function get_log(){
		$this->db->select(array('point_id','logs.timestamp','display_name','type_small','alarm_level'));
		$this->db->join('point','logs.point_id = point.id');
		$this->db->order_by('timestamp','asc');
		$this->db->from('logs');
		$query=$this->db->get();
		return $query->result();
	}
	
	/**
	 * 获取最近的100条报警日志
	 * @return array 报警日志
	 */
	function get_log_100(){
		$query=$this->db->query ("SELECT `point`.`display_name`, `point`.`chinese_name`, `point`.`english_name`, `point_id`, `logs`.`timestamp`, `logs`.`value` FROM (`logs`, `point`) where logs.`point_id` = point.`id` ORDER BY `timestamp` desc LIMIT 100");
		return $query->result();
	}
	
	/**
	 * 获取最近一段时间的报警日志
	 * @param  integer $minite 报警时间，默认为10分钟
	 * @return [type]          报警结果
	 */
	function get_log_recent($minite = 10){  //最近的100条数据
		$this->db->select(array('point_id','logs.timestamp'));
		$this->db->order_by('timestamp','desc');
		$this->db->where('timestamp >=', date('Y-m-d H:i:s',time() - 60 * $minite));
		$this->db->from('logs');
		$query=$this->db->get();
		return $query->result();
	}

	private function get_single_point_log($id = 0){  //not used
		$this->db->select("*");
		$this->db->order_by('timestamp','asc');
		$this->db->from('logs');
		$this->db->where("point_id",$id);
		$query=$this->db->get();
		return $query->result();
	}
	
	private function get_alarm_point(){   //not used
		$this->db->select("distinct(point_id)");
		$this->db->where("point_id !=",'');
		$this->db->from("logs");
		$query=$this->db->get();
		return $query->result();
	}
	
	private function get_alarm_count($start, $end, $point_id){  //not used

		$this->db->select("count(*) as count");
		$this->db->where('point_id', $point_id);
		$this->db->where('timestamp >=', $start);	
		$this->db->where('timestamp <=', $end);
		$this->db->from('logs');
		$query=$this->db->get();
		return $query->row()->count;
	}

	public function get_alarm_info () {
		$points = $this->logs_get_points ();
		$levels = $this->logs_get_levels ();
		$times = $this->get_se_time ("logs");

		$data ["points"] = $points;
		$data ["levels"] = $levels;

		$data ["times"]["start_time"] = date ("m/d/Y H:i:s", strtotime($times[0] ["start_time"]));
		$data ["times"]["end_time"] = date ("m/d/Y H:i:s", strtotime($times[0] ["end_time"]));

		return json_encode($data);
	}

	public function logs_get_points () {
		if (current_lang() == 'en')
			$name = "english_name";
		else 
			$name = "chinese_name";

		$result = $this->db->query ("Select id, display_name, " . $name . " as name from point");
		return $result->result_array ();
	}

	public function logs_get_levels () {
		$result = $this->db->query ("Select level from alarm_level;");
		return $result->result_array ();
	}

	public function get_se_time ($table_name)
	{
		$query = $this->db->query ("Select max(timestamp) as end_time, min(timestamp) as start_time from " . $table_name);
		return $query->result_array ();
	}

	public function logs_list_page ($limit, $offset, $condition)
	{
		$ret = array ();
		if (current_lang() == 'en')
		{
			$name = "english_name";
			$description = "description";
		}
		else
		{
			$name = "chinese_name";
			$description = "chinese_description";
		}

		$query = $this->db->query ("Select SQL_CALC_FOUND_ROWS logs.id as id, point_id, logs.timestamp, ifnull(value, '-') as value, " . $description . " 
									as description, level, logs.type, sent, display_name, " . $name . " as name, english_name from logs 
									left join point on logs.`point_id`=point.`id` 
									" . $condition . " 
									order by logs.`timestamp` desc limit " . $limit . " offset " . $offset);

		$ret ["rows"] = $query->result_array ();
		$ret ["total"] = $this->db->query ("Select found_rows() as total")->row ()->total;
		return $ret;	
	}

	public function get_search_data ($limit, $offset, $condition) {
		$ret = array ();
		if (current_lang() == 'en')
		{
			$name = "english_name";
			$description = "description";
		}
		else
		{
			$name = "chinese_name";
			$description = "chinese_description";
		}

		$query = $this->db->query ("Select SQL_CALC_FOUND_ROWS logs.id as id, point_id, logs.timestamp, ifnull(value, '-') as value, " . $description . " 
									as description, level, logs.type, sent, display_name, " . $name . " as name, english_name from logs 
									left join point on logs.`point_id`=point.`id` 
									" . $condition . " 
									order by logs.timestamp desc limit " . $limit . " offset " . $offset);

		$ret ["rows"] = $query->result_array ();
		$ret ["total"] = $this->db->query ("Select found_rows() as total")->row ()->total;
		return $ret;
	}

	public function get_search_data_all ($condition) {
		if (current_lang() == 'en')
		{
			$name = "english_name";
			$description = "description";
		}
		else
		{
			$name = "chinese_name";
			$description = "chinese_description";
		}

		$query = $this->db->query ("Select point_id, logs.timestamp, ifnull(value, '-') as value, " . $description . " as description, level, logs.type, 
									sent, display_name, " . $name . " as name, english_name from logs 
									left join point on logs.`point_id`=point.`id` 
									" . $condition . " 
									order by logs.timestamp desc;");

		return $query->result_array();
	}

	public function logs_update_description () {
		
	}
}
?>
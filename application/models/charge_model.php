<?php
class Charge_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function update_flat($arr) {
		$this->db->set('flat_time', substr(json_encode($arr, JSON_NUMERIC_CHECK), 1, -1))
		->where('id', 0)
		->update('charge');
		return true;
	}
	
	function get_charge_info($id){
		if(!isset($id)) {
			return null;
		}
		$this->db->select('*')
		->from('charge')
		->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	function get_arr_time_range($point_id,$time_arr,$start,$end){
		$this->db->select('sum(delta) as sum, date(timestamp) as date');

		$this->db->where('point_id',$point_id);
		$this->db->where_in('hour(timestamp)',$time_arr);
		$this->db->where('timestamp <=', $end." 23:59:59");
		$this->db->where('timestamp >=', $start." 00:00:00");
		$this->db->group_by("date(timestamp)");
		$this->db->order_by('timestamp', 'asc');
		$this->db->from('point_data');
		$query=$this->db->get();
		$result = $query->result();		
		$data = array();
		foreach ($result as $row){
			$data[$row->date] = $row->sum;
		}
		return $data;
	}
	
	function get_arr_month($point_id,$time_arr,$year,$month){
		$this->db->select('sum(delta) as sum, date(timestamp) as date');
		$this->db->where('point_id',$point_id);
		$this->db->where_in('hour(timestamp)',$time_arr);
		$this->db->where('year(timestamp)', $year);
		$this->db->where('month(timestamp)', $month);
		$this->db->group_by("day(timestamp)");
		$this->db->order_by('timestamp', 'asc');
		$this->db->from('point_data');
		$query=$this->db->get();
		$result = $query->result();		
		$data = array();
		foreach ($result as $row){
			$data[$row->date] = $row->sum;
		}
		return $data;
	}
	
	function get_arr_year($point_id, $time_arr, $year){
		$this->db->select('sum(delta) as sum, month(timestamp) as date');			
		$this->db->where('point_id',$point_id);
		$this->db->where_in('hour(timestamp)',$time_arr);
		$this->db->where('year(timestamp)', $year);
		$this->db->group_by("month(timestamp)");
		$this->db->order_by('timestamp', 'asc');
		$this->db->from('point_data');
		$query=$this->db->get();
		$result = $query->result();		
		$data = array();
		foreach ($result as $row){
			$data[$row->date] = $row->sum;
		}
		return $data;
	}
	
}

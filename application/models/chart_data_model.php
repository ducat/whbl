<?php
class Chart_data_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function select_collums_by_id($id, $arr, $table_name)
	{
		$this->db->select($arr);
		$this->db->where('id',$id);
		$this->db->from($table_name);
		$query=$this->db->get();
		return $query->result();
	}
	
	function select_collums_by_point_id($point_id, $arr, $table_name)
	{
		$this->db->select($arr);
		$this->db->where('point_id',$point_id);
		$this->db->from($table_name);
		$this->db->order_by('id','asc');
		$query=$this->db->get();
		return $query->result();
	}
	
	function select_star_by_id($id, $table_name )
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where('id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	
	function select_star_all($table_name)
	{
		$this->db->where('id',$id);
		$this->db->select('*');
		$this->db->from($table_name);
		$query=$this->db->get();
		return $query->result();
	}
	

	function get_point_info($point_id)
	{
		$this->db->where('id',$point_id);
		$this->db->select('*');
		$this->db->from('point');
		$query=$this->db->get();
		return $query->row();
	}
	
	function get_multi_point_info($custom_report_id)
	{
		$this->db->where('custom_report_id',$custom_report_id);
		$this->db->select('*');
		$this->db->from('custom_report_point_relation');
		$this->db->join('point','custom_report_point_relation.point_id=point.id');
		$query=$this->db->get();
		$result = array();
		foreach ($query->result() as $row ){
			array_push( $result, array(	'point_id' => $row->point_id, 
										'name' => $row->display_name, 
										'type'=>$row->type
					  )
			);
		}
		return $result;
	}
	
	
}
?>
<?php
class Alarm_level_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function get_alarm_level ()
	{
		$query = $this->db->query ("Select * from alarm_level");
		return $query->result_array ();
	}

	public function alarm_level_manage ($added_level, $deleted_level, $updated_level)
	{
		for ($i=0; $i < count($added_level); $i++) {
			$this->db->insert ('alarm_level', $added_level[$i]);
		}

		for ($i=0; $i < count($deleted_level); $i++) { 
			$tmp = (array) $deleted_level[$i];
			$this->db->where('level', $tmp['level']);
			$this->db->delete('alarm_level');
		}

		for ($i=0; $i < count($updated_level); $i++) { 
			$tmp = (array) $updated_level[$i];
			$this->db->where ('level', $tmp['level']);
			$this->db->update ('alarm_level', $updated_level[$i]);
		}
	}

	public function get_filter_time ($primary_key) {
		$query = $this->db->query ("Select work_filter, mail_filter from alarm_level where level = " . $primary_key);
		return $query->row ();
	}
}
?>
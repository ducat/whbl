<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class MY_Controller extends CI_Controller {
	protected $user;
	protected $is_admin;
	protected $menu_map = array (); // 菜单栏
	protected $version = "single"; // 不同版本对应不同的菜单及功能，家乐福单体版(single)、网络版(network)、上药(shangyao)
	public function __construct() {
		parent::__construct ();
		
		//$is_expired = strtotime ( date ( "Y-m-d" ) ) > strtotime ( "2014-11-1" );
		//$this->show_expired ( $is_expired );
		
		$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC10490000000000000000WHBL", NETWORK_EDITION);
		$this->show_errkey($status);
		
		if ($this->ion_auth->logged_in ()) {
			$this->user = $this->ion_auth->user ()->row ();
		} else {
			redirect ( '' );
			exit ();
		}
		$menu_static_map = array (
				'air_condition' => array (
						'air_condition1' => '空调机组1',
						'air_condition2' => '空调机组2',
						'air_condition3' => '空调机组3' 
				),
				'chiller_control' => array (
						'chiller_control1' => '冷机群控1',
						'chiller_control2' => '冷机群控2.1',
						'chiller_control3' => '冷机群控2.2',
						'chiller_control4' => '冷机群控2.3',
						'chiller_control5' => '冷机群控3' 
				),
				'distribution' => array (
						'distribution1' => '配电系统1',
						'distribution2' => '配电系统2',
						'distribution3' => '配电系统3' 
				),
				'new_wind' => array (
						'new_wind1' => '新风机组1',
						'new_wind2' => '新风机组2',
						'new_wind3' => '新风机组3',
						'new_wind4' => '新风机组4'
				),
				'boiler' => array (
						'boiler1' => '锅炉1' 
				),
				'manage' => array (
					'station' => lang ( 'station_management' ),
					/* alarm manage */
					'alarm_level' => lang ( 'alarm_level_manage' ),
						'alarm_point' => lang ( 'alarm_point_management' ),
						'alarm_batch' => lang ( 'alarm_batch_management' ),
					/* point manage */
					'bacnet' => lang ( 'bacnet_management' ),
						'point_bacnet' => lang ( 'point_bacnet_management' ),
						'modbus' => lang ( 'modbus_management' ),
						'point_modbus' => lang ( 'point_modbus_management' ),
						'point_virtual' => lang ( 'point_virtual_management' ),
						'custom_report' => lang ( 'custom_report_management' ),
						'instrument' => lang ( 'instrument_management' ),
						'image' => lang ( 'image_management' ),
						'image_group' => lang ( 'image_group_management' ),
						'charge' => lang ( "charge_management" ),
						'base' => lang ( 'base_management' ),
						'budget' => lang ( 'budget_management' ),
						'sms_sender' => lang ( 'sms' ),
						'mailer' => lang ( 'mailer' ),
						'user' => lang ( 'user_management' ),
						'system' => lang ( 'system_config' ) 
				) 
		);
		//从数据库得出的结果
		$this->load->model('Instrument_model', 'inst');
		$all_inst = $this->inst->get_all_with_name();
		$inst_array = array();
		foreach ($all_inst as $inst) {
			$inst_array['view/' . $inst->id] = $inst->sequence. '-'. $inst->name;
		}
		$menu_dynamic_map['instrument'] = $inst_array;
		
		$menu_merged =  array_merge_recursive($menu_dynamic_map, $menu_static_map);
		$this->menu_map = $menu_merged;
	}
	public function watchdog($pass1 = 0x0000, $pass2 = 0x0000, $HID_pre = 0x00000000, $USERID_pre = 0x00000000, $check = "", $edition = 0) {
		try {
			$Rockey = new COM ( "Com.CRockey4ND" );
		} catch ( Exception $e ) {
			return NO_ROCKEY_COM;
		}
		$Rockey->p1 = new VARIANT ( $pass1, VT_UI2 );
		$Rockey->p2 = new VARIANT ( $pass2, VT_UI2 );
		$Rockey->p3 = new VARIANT ( 0x0000, VT_UI2 );
		$Rockey->p4 = new VARIANT ( 0x0000, VT_UI2 );
		$result = $Rockey->RockeyCM ( 1 );
		if ($result == 0) {
			// $HID = $Rockey->lp1;
			// if($HID != $HID_pre)
			// return ERROR_ROCKEY;
		} else {
			return NO_ROCKEY;
		}
		// Open First Rockey
		$result = $Rockey->RockeyCM ( 3 );
		if ($result != 0) {
			return ERROR_ROCKEY;
		}
		// Read
		$Rockey->buffer = new VARIANT ( "", VT_BSTR );
		$Rockey->p1 = new VARIANT ( 0, VT_UI4 );
		$Rockey->p2 = new VARIANT ( 500, VT_UI4 );
		$result = $Rockey->RockeyCM ( 5 );
		if ($result == 0) {
			$memory = $Rockey->buffer;
			if ($memory != $check) {
				echo "Error code : " . $memory . "--" . $check;
				return ERROR_ROCKEY;
			}
		} else {
			return ERROR_ROCKEY;
		}
		// Read User ID
		
		$result = $Rockey->RockeyCM ( 10 );
		if ($result == 0) {
			$USERID = $Rockey->lp1;
			if ($USERID != $USERID_pre)
				return ERROR_ROCKEY;
		} else {
			return ERROR_ROCKEY;
		}
		
		$Rockey->p1 = new VARIANT ( $edition, VT_UI2 );
		$result = $Rockey->RockeyCM ( 12 );
		if ($result == 0) {
			$module_set = $Rockey->lp1;
			if ($module_set != 1)
				return ERROR_ROCKEY;
		} else {
			return ERROR_ROCKEY;
		}
		
		$result = $Rockey->RockeyCM ( 4 );
		if ($result != 0) {
			return ERROR_ROCKEY;
		}
		return SUCCESS_ROCKEY;
	}
	function show_expired($is_expired) {
		if ($is_expired) {
			show_my_error ( lang ( "expired" ), "" );
			exit ();
		}
	}
	function show_errkey($status) {
		if ($status == NO_ROCKEY) {
			show_my_error ( lang ( "dog_nokey" ), "" );
			exit ();
		} else if ($status == ERROR_ROCKEY) {
			show_my_error ( lang ( "dog_errkey" ), "" );
			exit ();
		} else if ($status == NO_ROCKEY_COM) {
			show_my_error ( lang ( "dog_nocom" ), "" );
			exit ();
		}
	}
}

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/config/lang/chi.php');
require_once('tcpdf/tcpdf.php');

if(!function_exists('export_pdf')) {
	function export_pdf($title, $subject, $logo, $logo_w, $header_title, $header_string, $content, $name) {
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('BUAA-G1049');
		$pdf->SetTitle($title);
		$pdf->SetSubject($subject);
		
		$pdf->setHeaderData($logo, $logo_w, $header_title, $header_string);
		
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		$pdf->AddPage();
		
		$pdf->writeHTML($content);
		
		$pdf->Output($name, 'I');
	}
}
*/

if(!function_exists('current_lang')) {
	function current_lang() {
		$cur_url = current_url();
		$token = strtok($cur_url, "/");
		return strtolower($token);
	}
}



if(!function_exists('show_my_error')) {
	function show_my_error($heading = "error", $message = "error") {
		$message = '<p>'.implode('</p><p>', ( ! is_array($message)) ? array($message) : $message).'</p>';
		ob_start();
		include(APPPATH.'errors/error_general.php');
		$buffer = ob_get_contents();
		ob_end_clean();
		echo $buffer;
		exit;
	}
}

if(!function_exists('show_login_err')) {
	function show_login_err($heading = "error", $message = "error") {
		$message = '<p>'.implode('</p><p>', ( ! is_array($message)) ? array($message) : $message).'</p>';
		ob_start();
		include(APPPATH.'errors/error_login.php');
		$buffer = ob_get_contents();
		ob_end_clean();
		echo $buffer;
		exit;		
	}
}

if(!function_exists('join_table_name')) {
	function join_table_name($field_name) {
		return 'j'.substr( md5($field_name),0,8);
	}
}



<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$operator[18]=array("+","-","*","/","^","sin","cos","(",")","and","or","_big","_small","=","_sum","_root","not","!=");
//表达式编码规则，type=1：二元操作符；2：一元操作符；3：表计名；4：数字；5：其他
if ( ! function_exists('foo'))
{
	//每当数据到来后，对警告表达式进行计算，返回若有报警返回1，否则返回0,，并将产生的报警放入alarmlog数据库
	function check_alarm($batch){
		
		$CI=getmodel();
		$expjsonarray=$CI->point_model->alarm_key_value();
		$result=array();
		if($expjsonarray==null)
			return false;
		else{
			
		while (list($key, $val) = each($expjsonarray))
		{
			//echo "$key => $val";
			$exparray=decode_json($val);
			$r=point_calc($exparray,$batch);
			$result[$key]=$r;
			if($r!=0)
				$CI->point_model->insert_alarm_log($key);
		}
			
		}
		return $result;
	}
	//load model
    function getmodel(){
    	$CI = & get_instance();
    	$CI->load->model("point_model"); 
    	return $CI;
    }
    
    //解析表达式，将表达式中虚拟点位及数据以键值对的方式返回
    function get_point_value_pair($expjson, $batch){
    	$pvp=array();
    	$exparray= decode_json($expjson);
    	if($exparray==null){
    		return null;
    	}
    	foreach($exparray as $expelement){
	    	if($expelement["type"]==3){//表计名
	    			//获得pointid
	    			$id=(int)(substr($expelement["value"],1,strlen($expelement["value"])-1));
	    			//echo($id);
	    			//获取点信息
	    			$r=$CI->point_model->get_point_info($id);
	    			if($r->is_virtual_point==0){
	    				$data=$CI->point_model-> get_point_data($id,$batch);
	    				$pvp[$id]=$data;
	    			}
	    			else{
	    				if($r[0]->expression==NULL)
	    					return false;
	    				else{
	    					$data=$CI->point_model-> get_point_data($id,$batch);
	    					$pvp[$id]=$data;
// 	    					if($data!=null)
// 	    						;
// 	    					else
// 	    						point_calc(decode_json($r[0]->expression),$batch);
	    				}
	    			}
	    		}
    	}
    	return $pvp;
    }
    
    //解析表达式，将小于等于maxbatch批次对应的表达式的值计算出来
    function point_calc_batch($expjson,$maxbatch){
    	$exparray= decode_json($expjson);
    	$i=1;
    	$valuearray=array();
    	for(;$i<=$maxbatch;$i++){
    		$value=point_calc($exparray,$i);
    		$valuearray[$i]=$value;//key是批次号，值是计算得到的结果
//     		echo("\n");
//     		echo($valuearray[$i]);
    	}
    	return $valuearray;
    }
    
     
    function point_calc_all($exparray,$id){
    	$max=$this->point_model->get_max_batch();		
		$ma=(array)($max[0]);
    	$i=1;
    	$valuearray=array();
    	for(;$i<=$ma["max(batch)"];$i++){
    		$value=point_calc($exparray,$i);
    		$valuearray[$i]=$value;//key是批次号，值是计算得到的结果
    		insert_value($id, $i, $value);
    	}
    	return valuearray;
    }
    
    //插入新表达式时使用    
    function point_calc_all_json($expjson,$id){
    	$exparray= decode_json($expjson);
    	return point_calc_all($exparray,$id);
    }
    
    //插入数据时调用
    function insert_data($batch){
    	$CI=getmodel();
    	$idt=$CI->point_model->pid_exp_key_value();
    	while (list($key, $val) = each($idt))
    	{
    		//echo "$key => $val";
    		$dt=$CI->point_model->get_point_data($key, $batch);
    		if($dt==null){
    			$exparray=decode_json($val);
    			$r=point_calc($exparray,$batch);   
    			insert_value($key, $batch, $r);
    		}
    	}    	
    }
    
    //插入数据时调用
    function point_calc_json($expjson,$batch){
    	$exparray= decode_json($expjson);
    	return point_calc($exparray,$batch);
    }
    
    //计算表达式值,参数是经过解json得到的表达式数组,batch是批次号,计算出错返回false
    function point_calc($exparray, $batch){
    	
    	$mystack =& stack_initialize();
    	$CI=getmodel();
    	//$CI->point_model->get_point_and_name();
    	
    	if($exparray==null){
    		return false;
    	}
    	foreach($exparray as $expelement){
    		//echo $expelement["type"];
    		if($expelement["type"]==1){//双目运算符
    			$mark=0;
    			if(stack_size($mystack)<1)
    				return false;
    			if(stack_size($mystack)==1){
    				if($expelement["value"]=="-"){
    					$mark=1;
    					$operfirst=stack_pop($mystack);
    				}
    				else{
    					return false;
    				}
    			}
    			else{
    				$opersecond=stack_pop($mystack);
    				//echo($opersecond);
    				$operfirst=stack_pop($mystack);
    				//echo($operfirst);
    				
    			}
    			switch($expelement["value"]){
    			case "+":{
    				$output=$operfirst+$opersecond;
    				 break;
    			}
    			case "-": {
    				if($mark==0){
    					$output=$operfirst-$opersecond;
    				}
    				else{
    					$output=-1*$operfirst;
    				}
    				
    			}; break;
    			case "*":{
    				$output=$operfirst*$opersecond;
    				break;
    			}
    			case "/":{
    				if($opersecond == 0)
    					$output = 0;
    				else 
    					$output=$operfirst/$opersecond;
    				break;
    			}
    			case "^":{
    				$output=pow($operfirst, $opersecond);
    				break;
    			}
    			case "and":{
    				if($operfirst==0 or $opersecond==0)
    					$output=0;
    				else 
    					$output=1;
    				 break;
    			}
    			case "or":{
    				if($operfirst==0 && $opersecond==0)
    					$output=0;
    				else
    					$output=1; 
    				break;
    			}
    			case "_big":{
    				if($operfirst > $opersecond)
    					$output=1;
    				else 
    					$output=0;
    				break;
    			}
    			case "_small":
    			{
    				if($operfirst < $opersecond)
    					$output=1;
    				else 
    					$output=0;
    				break;
    			};     			
    			case "=":  {
    				if($operfirst == $opersecond)
    					$output=1;
    				else 
    					$output=0;
    				break;
    			}; 
    			case "!=":  {
    				if($operfirst != $opersecond)
    					$output=1;
    				else 
    					$output=0;
    				break;
    			}; 
    			default : return false;
    			}
    			//echo($output);
    			stack_push($mystack,$output);
    		}
    		else if($expelement["type"]==2){//单目运算符
    			if(stack_size($mystack)<1)
    				return false;
    			$operfirst=stack_pop($mystack);
    			//echo($operfirst);
    			switch($expelement["value"]){
    				case "sin":{
    					$output=sin($operfirst);
    					//echo($output);
    					 break;
    				}
    				case "cos":{
    					$output=cos($operfirst);
    					 break;
    				}
    				case "_sum":{
    					$output=5*$operfirst; break;//5应呗t替代。
    				}
    				case "_root": {
    					$output=pow($operfirst,0.5);
    					 break;
    				}
    				case "not":{
    					if($operfirst==0)
    						$output=1;
    					else 
    						$output=0;
    					 break;
    				}
    				default: return false;
    				
    			}
    			stack_push($mystack,$output);
    		}
    		else if($expelement["type"]==3){//表计名
    			
    			//获得pointid
    			$id=(int)(substr($expelement["value"],1,strlen($expelement["value"])-1));
    			
    			//echo($id);
    			//获取点信息
    			$r=$CI->point_model->get_point_info($id);
    			//echo($r[0]->is_virtual_point);
    			if($r->is_virtual_point==0){
    				$data=$CI->point_model-> get_point_data($id,$batch);
					if($data != null)
						stack_push($mystack,$data->value);
					else 
						stack_push($mystack,0);
    				
    			}
    			else{

	    				if($r->expression==NULL)
	    					return false;
	    				else{
	    					$data=$CI->point_model-> get_point_data($id,$batch);
	    					if($data != null){
	    						stack_push($mystack,$data->value);
	    					}
	    					else{
	    						$output=point_calc(decode_json($r->expression),$batch);
	    						if($output != false && $output != null)
	    							stack_push($mystack,$output);
	    						else
	    							stack_push($mystack,0);
	    					}
	    				}

	    				
    			}
    		}
    		else if($expelement["type"]==4){//数字
    			
    			$pos=strpos($expelement["value"],".");
    			if($pos==false){//value为整数
    				stack_push($mystack,(int)($expelement["value"]));
    			}
    			else{//value为小树
    				stack_push($mystack,(float)($expelement["value"]));
    			} 
    			
    		}
    		else{
    			return false;
    		}
    	}
    	$final_result = stack_pop($mystack);
    	if(!is_finite($final_result) || is_nan($final_result))
    		return 0;
    	else 
    		return $final_result;
    }
    
    //将数据插入point_data表
    function insert_value($id, $batch, $value){
    	$CI=getmodel();
    	$CI->point_model->insert_point_data($id,$batch,$value);
    }
    
    //解json，返回array
    function decode_json($expjson){
    	$expdecoded=json_decode($expjson,true);
    	$i=0;
    	$exparray=array();
    	foreach($expdecoded as $ex){
    		$val=json_decode($ex,true);
    		$exparray[$i]=$val;
    		$i++;
    	}
//      	foreach($exparray as $ex){
//     		echo($ex["type"]);
//      		echo($ex["value"]);
//      	}
    	return $exparray;
    }
    
	function calcstack($explist){
		$mystack =& stack_initialize();
		stack_push($mystack, 73);
	}
	function &stack_initialize() {
		// In this case, just return a new array
		$new = array();
		return $new;
	}
	// The destory function will get rid of a stack
	function stack_destroy(&$stack) {
		// Since PHP is nice to us, we can just use unset
		unset($stack);
	}
	// The push operation on a stack adds a new value unto the top of the stack
	function stack_push(&$stack, $value) {
		// We are just adding a value to the end of the array, so can use the
		//  [] PHP Shortcut for this.  It's faster than usin array_push
		$stack[] = $value;
	}
	// Pop removes the top value from the stack and returns it to you
	function stack_pop(&$stack) {
		// Just use array pop:
		return array_pop($stack);
	}
	// Peek returns a copy of the top value from the stack, leaving it in place
	function stack_peek(&$stack) {
		// Return a copy of the value on top of the stack (the end of the array)
		return $stack[count($stack)-1];
	}
	// Size returns the number of elements in the stack
	function stack_size(&$stack) {
		// Just using count will give the proper number:
		return count($stack);
	}
	// Swap takes the top two values of the stack and switches them
	function stack_swap(&$stack) {
		// Calculate the count:
		$n = count($stack);
	
		// Only do anything if count is greater than 1
		if ($n > 1) {
			// Now save a copy of the second to last value
			$second = $stack[$n-2];
			// Place the last value in second to last place:
			$stack[$n-2] = $stack[$n-1];
			// And put the second to last, now in the last place:
			$stack[$n-1] = $second;
		}
	}
	// Dup takes the top value from the stack, duplicates it,
	//  and adds it back onto the stack
	function stack_dup(&$stack) {
		// Actually rather simple, just reinsert the last value:
		$stack[] = $stack[count($stack)-1];
	}
	// Let's use these to create a small stack of data and manipulate it.
	//  Start by adding a few numbers onto it, making it: 73 74 5
	/*$mystack =& stack_initialize();
	stack_push($mystack, 73);
	stack_push($mystack, 74);
	stack_push($mystack, 5);
	// Now duplicate the top, giving us:  73 74 5 5
	stack_dup($mystack);
	// Check the size now, it should be 4
	echo '<p>Stack size is: ', stack_size($mystack), '</p>';
	// Now pop the top, giving us: 5
	echo '<p>Popped off the value: ', stack_pop($mystack), '</p>';
	// Next swap the top two values, leaving us with: 73 5 74
	stack_swap($mystack);
	// Peek at the top element to ensure it is 74
	echo '<p>Current top element is: ', stack_peek($mystack), '</p>';
	// Now destroy it, we are done.
	stack_destroy($mystack);*/
	
	function judge_grammar($exp) {
		$valid_opr = array("+","-","*","/","^","sin","cos","(",")","and","or","_big","_small","=","_sum","_root","not","!=");
		$count_line = substr_count($exp, "_");
		$end_char = substr($exp, -1, 1);	
		
		$valid_line_flag = true;
		$count = 0;
		for($i=0;$i<strlen($exp);$i++)
		{
			$current = substr($exp,$i,1); 
			if ($current == "_"){
				$count++;
				if($count % 2 == 1 && $i > 0){
					$prev = substr($exp, $i - 1, 1);
					if (!in_array($prev, $valid_opr)){
						$valid_line_flag = false;
						break;
					}					
				}
			}			
		}		

		$valid_line_flag = false;
		$valid_end_flag = false;
		if( $end_char == "#")
			$valid_end_flag = true;
		
		if ($count_line % 2 == 0 && $count_line != 0)
			$valid_line_flag = true;
		
		if($valid_line_flag && $valid_end_flag && $valid_line_flag)
			return true;
		else
			return false;
		
	}
	
	function judge_exp_batch($arr_max, $arr_min, $arr_count) {
		sort($arr_max);
		sort($arr_min);
		sort($arr_count);
		$max_max = $arr_max[0];
		$min_max = $arr_max[count($arr_max) - 1];
		$max_min = $arr_min[0];
		$min_min = $arr_min[count($arr_min) - 1];
		$max_count = $arr_count[0];
		$min_count = $arr_count[count($arr_count) - 1];
		if( $max_max == $min_max && $max_min == $min_min && $max_count == $min_count)
			return true;
		else{
			return false;
		}
	
	}
	
}

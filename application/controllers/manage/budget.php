<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Budget extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('budget_management'). "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'budget',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/budget/table');
	}
	
	public function table()
	{
		try{
			$crud = new grocery_CRUD();			
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('budget');
			$crud->set_subject(lang('budget'));
			$crud->required_fields('year','name');
			$crud->columns('year','name');
			$crud->fields('year','name','unit','jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec');
			$crud->display_as('year',lang('budget_year'));
			$crud->display_as('name',lang('budget_name'));
			$crud->display_as('unit',lang('budget_unit'));
			$crud->display_as('jan',lang('jan'));
			$crud->display_as('feb',lang('feb'));
			$crud->display_as('mar',lang('mar'));
			$crud->display_as('apr',lang('apr'));
			$crud->display_as('may',lang('may'));
			$crud->display_as('jun',lang('jun'));
			$crud->display_as('jul',lang('jul'));
			$crud->display_as('aug',lang('aug'));
			$crud->display_as('sep',lang('sep'));
			$crud->display_as('oct',lang('oct'));
			$crud->display_as('nov',lang('nov'));
			$crud->display_as('dec',lang('dec'));
			$crud->field_type("unit", "readonly");
			$crud->field_type("year", "readonly");
			//$crud->set_rules('year',lang('budget_year'),'is_unique[budget.year]|integer');
			$path = UPLOAD_CSV_ROOT;
						
			$crud->set_field_upload('csv_url', $path);
			//$crud->callback_after_upload(array($this,'read_csv_budget'));
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_print();
			$output = $crud->render();
	
			$this->_example_output($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function read_csv_budget($uploader_response,$field_info, $files_to_upload)  //还没用上，代码未检查
	{
		//Is only one file uploaded so it ok to use it with $uploader_response[0].
		$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
			
		$file = fopen($file_uploaded,"r");
		while(! feof($file))
		{
			print_r(fgetcsv($file));
		}
		fclose($file);
	
		echo $file_uploaded;
		
		return true;
	}
	
	//end of 预算管理

}

?>
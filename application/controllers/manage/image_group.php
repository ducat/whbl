<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image_group extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('image_group_management') . "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'image_group',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/image_group/table');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('image_group');
			$crud->set_subject(lang('image'));
			$crud->required_fields('display_name','static_url','dynamic_url');
			$crud->columns('id','display_name','static_url','dynamic_url');
			
			$crud->fields('display_name','static_url','dynamic_url');
			$crud->display_as('id',lang('image_group_id'));
			$crud->display_as('display_name',lang('image_group_display_name'));
			$crud->display_as('static_url',lang('image_group_static_url'));
			$crud->display_as('dynamic_url',lang('image_group_dynamic_url'));
				
			$path = UPLOAD_IMG_ROOT;
			if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $path)) {
				mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . $path, 0777, true);
			}
			
			$crud->set_field_upload('dynamic_url', $path);
			$crud->set_field_upload('static_url', $path);
			
			$crud->unset_print();
			$output = $crud->render();
	
			$this->_example_output($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	

}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charge extends MY_Controller {

	private $peak_arr;
	private $diff;
	private $hours;
	private $hours_num;
	public function __construct()
	{
		parent::__construct();
		$this->peak_arr = array();
		$this->hours = array();
		for($i=0;$i<=23;$i++){
			$this->hours[(string)$i] = $i . ":00-" . ($i+1).":00";		
		}
		$this->hours_num = array();
		for($i=0;$i<=23;$i++){
			array_push($this->hours_num, $i);		
		}
		$this->diff = array();
		$this->load->model("charge_model", "charge");
		
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('manage/charge', array(
				'title' => lang('charge_management') . "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'charge',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/charge/table');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('charge');
			$crud->set_subject(lang('charge'));
			$crud->columns('name','peak','valley','flat','related_point');			
			$crud->fields('name','peak_time','peak','valley_time','valley',
					'flat','related_point');
			$crud->required_fields('name','flat','peak','valley','related_point');
			$crud->display_as('name',lang('charge_name'));
			$crud->display_as('peak_time',lang('charge_peak_time'));
			$crud->display_as('peak',lang('charge_peak'));
			$crud->display_as('valley_time',lang('charge_valley_time'));
			$crud->display_as('valley',lang('charge_valley'));
			$crud->display_as('flat_time',lang('charge_flat_time') . lang('charge_refresh'));
			$crud->display_as('flat',lang('charge_flat'));
			$crud->display_as('related_point',lang('charge_related_point'));

			$crud->set_relation('related_point','point','{chinese_name}',array("type_large"=>"accumulated", 'deleted'=>0));
						
			$crud->set_rules('peak',lang('charge_peak'),'numeric');
			$crud->set_rules('flat',lang('charge_flat'),'numeric');
			$crud->set_rules('valley',lang('charge_valley_time'),'numeric');
			$crud->set_rules('peak_time',lang('charge_peak_time'),'callback_peak_check');
			$crud->set_rules('valley_time',lang('charge_valley_time'),'callback_valley_check');
			
			$crud->callback_after_update(array($this, 'calc_flat_after_update'));
			$crud->field_type('peak_time','multiselect', $this->hours);
			$crud->field_type('valley_time','multiselect', $this->hours);
			$crud->field_type('flat_time','multiselect', $this->hours);
						
			$crud->unset_delete();
			$crud->unset_print();
			$output = $crud->render();
	
			$this->_example_output($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function peak_check($arr)
	{
		if($arr == null)
			$arr = array();
		$this->peak_arr = $arr;
		return TRUE;
	}
	
	public function valley_check($arr)
	{
		if($arr == null)
			$arr = array();
		$intersect = array_intersect($this->peak_arr, $arr);
		$this->diff = array_values(array_diff($this->hours_num, $this->peak_arr, $arr));
		
		if (count($intersect))		
		{
			$this->form_validation->set_message('valley_check', lang("charge_time_intersect"));
			//$this->form_validation->set_message('valley_check', json_encode($this->diff));
			return FALSE;
		}
		else
		{			
			return TRUE;
		}
	}
	
	function calc_flat_after_update($post_array,$primary_key)
	{
		$this->charge->update_flat($this->diff);
		return true;
	}

}

?>
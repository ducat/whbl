<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('image_management') . "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'image',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/image/table');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('image');
			$crud->set_subject(lang('image'));
			$crud->required_fields('display_name','url');
			$crud->columns('id','display_name','url');
			
			$crud->fields('display_name', 'url');
			$crud->display_as('id',lang('image_id'));
			$crud->display_as('display_name',lang('image_display_name'));
			$crud->display_as('url',lang('image_url'));
				
			$path = UPLOAD_IMG_ROOT;
			if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $path)) {
				mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . $path, 0777, true);
			}
			
			$crud->set_field_upload('url', $path);
			
			$crud->unset_print();
			$output = $crud->render();
	
			$this->_example_output($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	

}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Station extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('station_management') . "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'station',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/station/table/edit/1');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('station');
			$crud->set_subject(lang('station'));
			$crud->required_fields('name', 'air_condition_area', 'num_chiller','num_cooling_pump','num_freezing_pump','num_tower');
			$crud->columns('name','city','air_condition_area',
					'building_type','image_url');
			
			$crud->fields('name','city','address',
					'build_year','floor_num','total_area','air_condition_area',
					'num_chiller','num_cooling_pump','num_freezing_pump','num_tower',
					'days_perweek','hours_perday','building_type', 'image_url');
			$crud->display_as('station_id',lang('station_id'));
			$crud->display_as('name',lang('station_name'));
			$crud->display_as('city',lang('station_city'));
			$crud->display_as('address',lang('station_address'));
			$crud->display_as('build_year',lang('station_build_year'));
			$crud->display_as('floor_num',lang('station_floor_num'));
			$crud->display_as('total_area',lang('station_total_area'));
			$crud->display_as('air_condition_area',lang('station_air_condition_area'));
			$crud->display_as('days_perweek',lang('station_days_perweek'));
			$crud->display_as('hours_perday',lang('station_hours_perday'));
			$crud->display_as('building_type',lang('station_building_type'));
			
			$crud->display_as('num_chiller',lang('station_num_chiller'));
			$crud->display_as('power_chiller',lang('station_power_chiller'). " (Ton)");
			$crud->display_as('num_cooling_pump',lang('station_num_cooling_pump'));
			$crud->display_as('power_cooling_pump',lang('station_power_cooling_pump') . " (KW)");
			$crud->display_as('num_freezing_pump',lang('station_num_freezing_pump'));
			$crud->display_as('power_freezing_pump',lang('station_power_freezing_pump'). " (KW)");
			$crud->display_as('num_tower',lang('station_num_tower'));
			$crud->display_as('power_tower',lang('station_power_tower'). " (KW)");
			
			$crud->display_as('image_url',lang('station_img_url'));
			$crud->display_as('translate_csv',lang('station_translate_csv'));
			
			//$crud->set_rules('year',lang('budget_year'),'is_unique[budget.year]|integer');
			$crud->set_rules('year',lang('budget_year'),'is_natural');
			$crud->set_rules('build_year', lang('station_build_year'), 'trim|required|is_natural');
			$crud->set_rules('floor_num', lang('station_floor_num'), 'trim|required|is_natural_no_zero');
			$crud->set_rules('total_area', lang('station_total_area'), 'trim|required|numeric');
			$crud->set_rules('air_condition_area', lang('station_air_condition_area'), 'trim|required|numeric');	
			
			
			$crud->set_rules('num_chiller',lang('station_num_chiller'), 'trim|required|is_natural');
			$crud->set_rules('power_chiller',lang('station_power_chiller'), 'trim|required|is_natural');
			$crud->set_rules('num_cooling_pump',lang('station_num_cooling_pump'), 'trim|required|is_natural');
			$crud->set_rules('power_cooling_pump',lang('station_power_cooling_pump'), 'trim|required|is_natural');
			$crud->set_rules('num_freezing_pump',lang('station_num_freezing_pump'), 'trim|required|is_natural');
			$crud->set_rules('power_freezing_pump',lang('station_power_freezing_pump'), 'trim|required|is_natural');
			$crud->set_rules('num_tower',lang('station_num_tower'), 'trim|required|is_natural');
			$crud->set_rules('power_tower',lang('station_power_tower'), 'trim|required|is_natural');
			
			$crud->change_field_type('station_id', 'readonly');
			$crud->change_field_type('days_perweek', 'enum', array('1','2','3','4','5','6','7'));

			$hours = array();
			for ($i=1;$i<=24;$i++)
				array_push($hours, $i);
			$crud->field_type('hours_perday', 'enum', $hours);
			$crud->field_type('building_type','dropdown',
					array(	'commercial' => lang('station_commercial'),
							'hotel' => lang('station_hotel'),
							'office' => lang('station_office'),
							'hospital' => lang('station_hospital'),
							'others' => lang('station_others')));
			
			
			$path = UPLOAD_IMG_ROOT;
			if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $path)) {
				mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . $path, 0777, true);
			}
			
			$crud->set_field_upload('image_url', $path);
			
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_print();
			$crud->unset_back_to_list();
			$output = $crud->render();
	
			$this->_example_output($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	

}
/* End of file station.php */
/* Location: controller/manage/station.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('url');
		
		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'ion_auth') ?
		$this->load->library('mongo_db') :
		
		$this->load->database();
		
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('user_management'). "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'user',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/user/table');
	}
	
	
	public function table() {
		try {
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('users');
			$crud->set_subject(lang('user'));
			$crud->columns('id', 'username', 'email', 'users_groups');
			$crud->add_fields('username', 'password', 'email', 'company', 'phone', 'users_groups');
			$crud->edit_fields('username', 'password_change', 'email', 'company', 'phone', 'users_groups');
			
			if($this->ion_auth->in_group('level4')){
				$crud->where('username !=', 'administrator');
				$crud->where('username !=', 'Mastertrane');
			}
			
			if($this->ion_auth->in_group('level4')){
				$crud->set_relation_n_n('users_groups', 'users_groups', 'groups_for_level4', 'user_id', 'group_id', 'name');
			}
			else 
				$crud->set_relation_n_n('users_groups', 'users_groups', 'groups', 'user_id', 'group_id', 'name');
			
			$crud->required_fields('username', 'password');
			
			$crud->set_rules('username', lang('user_username'), 'required|min_length[3]|max_length[15]');
			$crud->set_rules('password', lang('user_password'), 'required|min_length[6]|max_length[15]');
			$crud->set_rules('email', lang('user_email'), 'valid_email');
			
			$crud->set_rules('password_change', lang('user_password'), 'min_length[6]|max_length[15]');
			
			$crud->display_as('id', lang('user_id'));
			$crud->display_as('username', lang('user_username'));
			$crud->display_as('password', lang('user_password'));
			$crud->display_as('first_name', lang('user_first_name'));
			$crud->display_as('last_name', lang('user_last_name'));
			$crud->display_as('email', lang('user_email'));
			$crud->display_as('company', lang('user_company'));
			$crud->display_as('phone', lang('user_phone'));
			$crud->display_as('users_groups', lang('user_users_groups'));
			
			$crud->display_as('password_change', lang('user_password_change'));
			
			$crud->unset_print();
			
			$crud->change_field_type('password', 'password');
			
			$crud->callback_edit_field('username',array($this,'set_username_not_change'));
			$crud->callback_edit_field('password_change',array($this,'set_password_empty'));
			
			$crud->callback_insert(array($this, 'create_user'));
			$crud->callback_update(array($this, 'edit_user'));
			
			$output = $crud->render();
			$this->_example_output($output);
			
		} catch (Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function create_user($post_array) {
		$username = $post_array['username'];
		$password = $post_array['password'];
		$email = $post_array['email'];
		$additional_data = array(
			'first_name' => $post_array['first_name'],
			'last_name'  => $post_array['last_name'],
			'company'    => $post_array['company'],
			'phone'      => $post_array['phone'],
		);
		$group_name = $post_array['users_groups'];
		
		return $this->ion_auth->register($username, $password, $email, $additional_data, $group_name);
	}
	
	function edit_user($post_array, $primary_key) {
		
		$data = array(
			'first_name' => $post_array['first_name'],
			'last_name'  => $post_array['last_name'],
			'company'    => $post_array['company'],
			'phone'      => $post_array['phone'],
			'email'		 => $post_array['email']
		);
		
		if(isset($post_array['password_change']) && !empty($post_array['password_change'])) {
			$data['password'] = $post_array['password_change'];
		}
		
		if(!$this->ion_auth->update($primary_key, $data)) {
			return FALSE;
		};
		if(!$this->ion_auth->remove_from_group(false, $primary_key)) {
			return FALSE;
		}
		$groups = $post_array['users_groups'];
		foreach ($groups as $group) {
			if(!$this->ion_auth->add_to_group($group, $primary_key)) {
				return FALSE;
			}
		}
		
		return $primary_key;
	}
	
	function set_username_not_change($value, $primary_key) {
		return '<span style="padding: 5px; line-height: 30px;">' . $value . '</span><input type="hidden" name="username" value="' . $value . '">';
	}
	
	function set_password_empty() {
		return '<input type="password" name="password_change" value="" placeholder="' . lang('user_password_change_hint') . '">';
	}	
}
/* End of file user.php */
/* Location: controller/manage/user.php */
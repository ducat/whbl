<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom_report extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('custom_report_management'). "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'custom_report',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/custom_report/table');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();				
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('custom_report');
			$crud->set_subject(lang('custom_report'));
			$crud->required_fields('display_name','custom_report_point_relation','creator_id');
			$crud->columns('id','creator_id','display_name','custom_report_point_relation');
			$crud->set_relation('creator_id','users','username',array('id' => $this->user->id));
			$crud->set_relation_n_n('custom_report_point_relation', 'custom_report_point_relation', 'point', 'custom_report_id', 'point_id', 'chinese_name');
			$crud->fields('creator_id','display_name','custom_report_point_relation');
			$crud->display_as('id',lang('custom_report_id'));
			$crud->display_as('creator_id',lang('custom_report_creator_id'));
			$crud->display_as('display_name',lang('custom_report_display_name'));
			$crud->display_as('custom_report_point_relation',lang('custom_report_point_relation'));
			$crud->display_as('custom_report_user_relation',lang('custom_report_user_relation'));

			$crud->unset_print();
			$output = $crud->render();
				
			$this->_example_output($output);
				
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	

	

}

?>
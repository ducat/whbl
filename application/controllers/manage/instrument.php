<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instrument extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("point_model", "point");
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('instrument_management') . "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map, 
				'active' => 'instrument',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/instrument/table');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('instrument');
			$crud->set_subject(lang('instrument_only'));
			$crud->required_fields('name','sequence');
			$crud->columns('id','sequence','name');

			$crud->set_relation_n_n('instrument_img_group_relation', 'instrument_img_group_relation', 'image_group', 'instrument_id', 'img_group_id', 'display_name');
			$crud->set_relation_n_n('instrument_image_relation', 'instrument_image_relation', 'image', 'instrument_id', 'image_id', 'display_name');
			$crud->set_relation_n_n('instrument_point_relation', 'instrument_point_relation', 'point', 'instrument_id', 'point_id', 'chinese_name');
			
			$crud->fields('name','sequence','instrument_img_group_relation', 'instrument_image_relation', 'instrument_point_relation');
			$crud->display_as('instrument_img_group_relation',lang('instrument_img_group_select'));
			$crud->display_as('instrument_image_relation',lang('instrument_image_select'));
			$crud->display_as('instrument_point_relation',lang('instrument_point_select'));
			$crud->display_as('id',lang('instrument_id'));
			$crud->display_as('name',lang('instrument_name'));
			$crud->display_as('sequence',lang('instrument_sequence'));
			
			$crud->field_type('sequence', 'enum', array(1,2,3,4,5,6,7,8,9,10));
			
			$crud->unset_print();
			$crud->add_action(lang('image_point_relation_management'), '', 'manage/instrument/set_relation', 'report-icon', null, '_top');
			$crud->add_action(lang('instrument_edit_view'), '', 'manage/instrument/edit_view', 'chart-icon', null, '_top');
			$crud->add_action(lang('point_edit_view'), '', 'manage/instrument/point_edit_view', 'expression-icon', null, '_top');
			
			$output = $crud->render();
			$this->_example_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function set_relation($id)
	{
		if(!$id) {
			show_404();
			return;
		}
		$this->load->model("instrument_model", "inst");
		$instru = $this->inst->get_by_id($id);
	
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('instrument_img_group_relation');
			$crud->set_subject(lang('instrument_only') . '-' . ($instru->name? $instru->name : $id) );
			$crud->where('instrument_id', $id);
			$crud->required_fields();
			$crud->columns('id','img_group_id','related_point_id','dynamic_rule','dynamic_value');
				
			$crud->set_relation('img_group_id','image_group','display_name');
			$crud->set_relation('related_point_id','point','display_name');
			
			$crud->fields('related_point_id', 'dynamic_rule', 'dynamic_value');

			$crud->display_as('id',lang('image_point_relation_id'));
			$crud->display_as('img_group_id',lang('instrument_img_group_select'));
			$crud->display_as('related_point_id',lang('image_point_relation_point'));
			$crud->display_as('dynamic_rule',lang('image_point_relation_dynamic_rule'));
			$crud->display_as('dynamic_value',lang('image_point_relation_dynamic_value'));
				
			$crud->field_type('dynamic_rule', 'dropdown', array( 'eq' => lang("image_point_relation_eq"),
															'ne' => lang("image_point_relation_ne"),
															'gt' => lang("image_point_relation_gt"),
															'ge' => lang("image_point_relation_ge"),
															'lt' => lang("image_point_relation_lt"),
															'le' => lang("image_point_relation_le")));
			$crud->set_rules('dynamic_value',lang('image_point_relation_dynamic_value'),'numeric');
			$crud->callback_after_update(array($this, 'empty_rule_and_value'));
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_print();
	
			$output = $crud->render();
			$this->_example_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function empty_rule_and_value($post_array,$primary_key)
	{
		$update_arr = $post_array;
		if($post_array['related_point_id'] == null || $post_array['dynamic_rule'] == null || $post_array['dynamic_value'] == null){ 
			$update_arr['related_point_id'] = null;
			$update_arr['dynamic_rule'] = null;
			$update_arr['dynamic_value'] = null;
		}						
		return $this->db->update('instrument_img_group_relation', $update_arr, array('id'=>$primary_key));
	}
	
	public function point_edit_view($id) {
		if(!$id) {
			show_404();
			return;
		}
		$this->load->model("instrument_model", "inst");
		$instru = $this->inst->get_by_id($id);
	
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			
			//设置数据库查询
			$crud->set_table('instrument_point_relation');
			$crud->set_subject(lang('instrument_only') . '-' . ($instru->name? $instru->name : $id) );
			$crud->where('instrument_id', $id);
			$crud->required_fields();
			//设置表头
			$crud->columns('id', 'point_id', 'display_text', 'font_weight', 'font_style', 'font_size', 'font_color');
			
			//编辑页面中可供编辑的部分
			$crud->fields('display_text', 'font_weight', 'font_style', 'font_size', 'font_color');
			$crud->display_as('id', lang('instrument_id'));
			$crud->display_as('point_id', lang('point'));
			$crud->display_as('display_text', lang('point_display_text'));
			$crud->display_as('font_weight', lang('point_font_weight'));
			$crud->display_as('font_style', lang('point_font_style'));
			$crud->display_as('font_size', lang('point_font_size'));
			$crud->display_as('font_color', lang('point_font_color'));
			
			$crud->field_type('font_weight', 'enum', array('normal', 'bold', 'bolder', 'lighter'));
			$crud->field_type('font_style', 'enum', array('normal', 'italic', 'oblique'));
			$crud->field_type('font_size', 'integer');
			$crud->field_type('font_color', 'string');

			$crud->callback_after_update(array($this, 'update_point_data'));
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_print();
	
			$output = $crud->render();
			$this->_example_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function update_point_data ($post_array,$primary_key)
	{
		$update_arr = $post_array;
		if($post_array['display_text'] == null)
			$update_arr['display_text'] = null;
		if ($post_array['font_weight'] == null)
			$update_arr['font_weight'] = 'normal';
		if ($post_array['font_style'] == null)
			$update_arr['font_style'] = 'normal';
		if ($post_array['font_size'] == null)
			$update_arr['font_size'] = '12';
		if ($post_array['font_color'] == null)
			$update_arr['font_color'] = '#ff0';

		return $this->db->update('instrument_point_relation', $update_arr, array('id'=>$primary_key));
	}
	
	public function edit_view($id) {
		if(!$id) {
			show_404();
			return;
		}
		$inst = $this->inst->get_by_id($id);
		
		if(!$inst) {
			show_404();
			return;
		}
		$inst_img_groups = $this->inst->get_img_group_by_inst_id($id);
		$inst_images = $this->inst->get_image_by_inst_id($id);
		$inst_points = $this->inst->get_point_by_instrument_id($id);
		
		$this->load->view('instrument/dynamic_view', array(
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'instrument' => $inst,
				'inst_images' => $inst_images,
				'inst_points' => $inst_points,
				'inst_img_groups' => $inst_img_groups,
				'edit' => true));
	}
	
	public function set_location() {
		if(strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
			return null;
		}
		
		$type = $_POST['type'];
		$id = $_POST['id'];
		$x = $_POST['x'];
		$y = $_POST['y'];
		
		
		switch ($type) {
			case 'point':
				$this->inst->update_instrument_point_location($id, $x, $y);
			break;
			
			case 'img':
				$this->inst->update_instrument_image_loaction($id, $x, $y);
			break;

			case 'group':
				$this->inst->update_instrument_group_loaction($id, $x, $y);
				break;
				
			default;
				;
			break;
		}
		
	}
	
	public function resize_image() {
		if(strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
			return null;
		}
		
		$type = $_POST['type'];
		$id = $_POST['id'];
		$w = $_POST['w'];
		$h = $_POST['h'];
		
		$this->load->model('Image_model', 'img');
		switch ($type) {
			case 'img':
				$this->img->update_image_size($id, $w, $h);
				break;
		
			case 'group':
				$this->img->update_image_group_size($id, $w, $h);
				break;
		
			default;
			;
			break;
		}
		
	}
	
	public function remove_item() {
		if(strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
			return null;
		}
		
		$type = $_POST['type'];
		$id = $_POST['id'];
		$inst_id = $_POST['instrument_id'];
		
		switch ($type) {
			case 'point':
				$result = $this->inst->delete_instrument_point($id);
				break;
					
			case 'img':
				$result = $this->inst->delete_instrument_image($id, $inst_id);
				break;
					
			default;
				$result = false;
				break;
		}
		
		echo $result ? '1' : '0';
	}
}
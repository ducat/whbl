<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alarm_batch extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("alarm_model", "alarm");
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('manage/alarm_batch', array(
				'title' => lang('alarm_management'). "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'alarm_batch',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/alarm_batch/table');
	}
	

	public function table()
	{
		try{
		
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('alarm');
			$crud->set_subject(lang('alarm_batch_management'));
			
			if (current_lang() == 'en'){
				$crud->columns('english_name', 'alarm_low', 'alarm_high', 'alarm_level');
				$crud->fields('english_name','alarm_level','unit','alarm_low','alarm_high','alarm_low_summer','alarm_high_summer',
						'alarm_low_winter','alarm_high_winter');
			}
			else {
				$crud->columns('chinese_name', 'alarm_low', 'alarm_high', 'alarm_level');
				$crud->fields('chinese_name','alarm_level','unit','alarm_low','alarm_high','alarm_low_summer','alarm_high_summer',
						'alarm_low_winter','alarm_high_winter');
			}
			
			if($this->ion_auth->in_group('level2')){
				$crud->where('level_show <=', 2);
			}
			
			$crud->required_fields('alarm_level');
			$crud->set_relation('alarm_level', 'alarm_level', "level");
			$crud->display_as('chinese_name',lang('point_chinese_name'));
			$crud->display_as('english_name',lang('point_english_name'));
			$crud->display_as('type',lang('alarm_type'));
			$crud->display_as('alarm_level',lang('alarm_level'));
			$crud->display_as('unit',lang('point_unit'));
			$crud->display_as('alarm_high',lang('alarm_high'));
			$crud->display_as('alarm_low',lang('alarm_low'));
			$crud->display_as('alarm_high_summer',lang('alarm_high_summer'));
			$crud->display_as('alarm_low_summer',lang('alarm_low_summer'));
			$crud->display_as('alarm_high_winter',lang('alarm_high_winter'));
			$crud->display_as('alarm_low_winter',lang('alarm_low_winter'));

			$crud->change_field_type('chinese_name', 'readonly');
			$crud->change_field_type('english_name', 'readonly');
			$crud->change_field_type('unit', 'readonly');
			//$crud->change_field_type('alarm_level','enum', array('2','3','4'));

			$crud->callback_after_update(array($this, 'update_point_alarm'));

			//$crud->add_action(lang('edit'), '', "",'edit-icon',array($this,'edit_alarm'));

			$crud->unset_print();
			$crud->unset_add();
			//$crud->unset_edit();
			$crud->unset_delete();
			$output = $crud->render();
			$state = $crud->getState();
			$stateInfo = $crud->getStateInfo();
			
			$output->state = $state;
			$output->remove_season = true;
			if($state == 'edit')
			{
				$info = $this->alarm->get_info_by_id($stateInfo->primary_key);
				if($info != null && $info->unit == 62)					//only for carrefoure
				//if($info != null && $info->english_name == "Area Temperature")
					$output->remove_season = false;
			}
			
			$this->_example_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
/*	
	function update_point_alarm($post_array,$primary_key)  //not for Carrefour 非家乐福项目只需按照报警的类别修改
	{
		foreach ($post_array as $key => $value){
			if ($value == null)
				$post_array[$key] = null;
		}
	
		$alarm_batch_info = $this->alarm->get_info_by_id($primary_key);
	
		if($alarm_batch_info != null){
			$where_exp = "";
			if($alarm_batch_info->type)
				$where_exp = $alarm_batch_info->type;
			else
				return false;
			$this->db->where("type_small", $where_exp);
			$this->db->update('point',$post_array);
		}
		return true;
	}*/

	/**
	 * 报警批处理后更新后，自动修改匹配点位的报警设置
	 * @param  array $post_array   更新数组
	 * @param  integer $primary_key  主键
	 * @return boolean              true or false
	 */
	function update_point_alarm($post_array,$primary_key)  //for Carrefour 家乐福按照单位和点位名称定义了各种报警规则
	{
		foreach ($post_array as $key => $value){
			if ($value == null)
				$post_array[$key] = null;	
		}

		$alarm_batch_info = $this->alarm->get_info_by_id($primary_key);  
	
		if($alarm_batch_info != null){
			$where_exp = "";
			if($alarm_batch_info->unit != null)
				$where_exp = "unit = " . $alarm_batch_info->unit;
			else
				$where_exp = "unit IS NULL";

			if($alarm_batch_info->rule != null){
				$rules = explode("|", $alarm_batch_info->rule);
				$where_exp = $where_exp . " AND ( expression like '" . $rules[0] . "'";
				for ($i = 1; $i < count($rules); $i++)
					$where_exp = $where_exp . " OR expression like '" . $rules[$i] . "'";
				$where_exp = $where_exp . " )";
			}
			
			$this->db->where($where_exp);
			$this->db->update('point',$post_array);
		}
		return true;
	}
}

?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alarm_point extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
				
 		//$is_expired = strtotime(date("Y-m-d")) > strtotime("2013-12-25");
 		//$this->show_expired($is_expired);		
		//$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC1049", MONO_EDITION);
		//$this->show_errkey($status);
			
		$this->load->model("alarm_point_model","alarm_point");
	}
	
	public function index() {  //No Use
		redirect('manage/alarm_point/table');
	}
	
	public function table ()
	{
		$data = $this->alarm_point->get_alarm_point ();
		$this->load->view ('alarm/alarm_point', array(
				'title' => lang('alarm_point_manage') . "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'alarm_point',
				'data' => $data));
	}

	public function get_point_detail ()
	{
		$type_small = $this->alarm_point->get_point_type_small ();

		if ($type_small == "Temperature") {
			$this->load->view("alarm/alarm_detail_temp");
		}
		else {
			$this->load->view("alarm/alarm_detail");
		}
		//	$this->load->view("alarm/alarm_detail");

	}

	public function update_alarm_point_detail () {
		$this->alarm_point->detail_update ();
		redirect("manage/alarm_point");
	}
}
?>
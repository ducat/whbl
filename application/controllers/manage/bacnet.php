<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bacnet extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('collector_management'). "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'bacnet',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/bacnet/table');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('bcu_devices');
			$crud->set_subject(lang('bcu_devices'));
			$crud->columns('id','name','ip_address','port','instance_number', 'type');
		//	$crud->columns('id','name','ip_address','port','instance_number', 'status', 'type');			//暂时不开放bcu设备的控制
				
			$crud->fields('name','ip_address','port','instance_number', 'status', 'type');
			$crud->display_as('id',lang('bcu_id'));
			$crud->display_as('name',lang('bcu_name'));
			$crud->display_as('ip_address',lang('bcu_ip_address'));
			$crud->display_as('port',lang('bcu_port'));	
			$crud->display_as('instance_number',lang('bcu_instance_number'));
			$crud->display_as('status', lang('bcu_status'));
			$crud->display_as('type', lang('bcu_type'));

			$crud->set_rules('instance_number',lang('bcu_instance_number'),'integer');
			$crud->set_rules('port',lang('bcu_port'),'integer');
			$crud->set_rules('ip_address',lang('bcu_ip_address'),'valid_ip');

		//	$crud->field_type('status','dropdown', array ('0'=>lang('bcu_close'), '1'=>lang('bcu_open')));	//暂时不开放
			$crud->field_type('type','dropdown', array ('0'=>'BCU', '1'=>'SC'));
			$crud->unset_print();
	
			$output = $crud->render();
	
			$this->_example_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
/* End of file bacnet.php */
/* Location: controller/manage/bacnet.php */

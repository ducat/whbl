<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mailer extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('mailer'). "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'mailer',
				'output' => $output
				));
	}
	
	public function index()
	{
		redirect('manage/mailer/table/edit/1');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			
			$crud->set_table('mailers');
			$crud->set_subject(lang('mailer'));
			$crud->columns('from', 'host', 'user', 'password', 'smtp_auth');
			$crud->fields('from', 'host', 'user', 'password', 'smtp_auth');
			
			$crud->display_as('from',lang('mailer_from'));
			$crud->display_as('host',lang('mailer_host'));
			$crud->display_as('user',lang('mailer_user'));
			$crud->display_as('password',lang('mailer_password'));
			$crud->display_as('smtp_auth',lang('mailer_smtp_auth'));

			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_print();
			$crud->unset_back_to_list();
			$output = $crud->render();
	
			$this->_example_output($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	}

/* End of file mailer.php */
/* Location: controller/manage/mailer.php */

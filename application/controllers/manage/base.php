<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("point_data_model", "point_data");
		$this->load->model("base_model", "base");
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('base_management') . "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'base',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/base/table');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();			
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('base');
			$crud->set_subject(lang('base'));
			$crud->required_fields('name','point_id','type');
			
			$crud->columns('id','name','point_id','type','start_time','end_time','value');
			$crud->fields('name','point_id','type','start_time','end_time');
				
			$crud->display_as('id',lang('base_id'));
			$crud->display_as('name',lang('base_name'));
			$crud->display_as('point_id',lang('base_point_id'));
			$crud->display_as('type',lang('base_type'));
			$crud->display_as('start_time',lang('base_start_time'));
			$crud->display_as('end_time',lang('base_end_time'));
			$crud->display_as('value',lang('base_value'));

			if (current_lang() == 'en')
				$crud->set_relation('point_id','point','english_name');
			else
				$crud->set_relation('point_id','point','chinese_name');
			
			$crud->field_type('type', 'dropdown', 
				array(	'max'=>lang('base_max'),
						'min'=>lang('base_min'),
						'avg'=>lang('base_avg'),
						'sum'=>lang('base_sum'))
					);
				
			//$crud->set_rules('year',lang('budget_year'),'is_unique[budget.year]|integer');

			$crud->callback_after_insert(array($this, 'calc_value'));
			$crud->callback_after_update(array($this, 'calc_value'));
				
			$crud->unset_print();
			$output = $crud->render();
	
			$this->_example_output($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	
	function calc_value($post_array,$primary_key)
	{

	    $point_id = $post_array['point_id'];
	    $type = $post_array['type'];
	    $start = (string)$post_array['start_time'];
	    $end = (string)$post_array['end_time'];

	    $value = $this->point_data->get_base_by_point_id($point_id,$type,$start,$end);
		
		$this->base->update_base_value($primary_key, round($value,2));
	
		return true;
	}
	

}
/* End of file base.php */
/* Location: controller/manage/base.php */

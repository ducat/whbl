<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Point_virtual extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	//	$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC10491010000000000000MONO", MONO_EDITION);
	//	$this->show_errkey($status);
		
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('manage/point', array(
				'title' => lang('point_virtual_management') . "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'point_virtual',
				'type' => 'virtual',
				'output'=>$output
				));
	}
	
	//start of 点位管理
	public function index()
	{
		redirect("manage/point_virtual/table");
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();	
			if (current_lang() == 'en')
				$crud->set_language("english");	
			$crud->set_table('point');
			$crud->where('is_virtual_point', 1);
			$crud->where('id !=', 0);					//Do not display SYSTEM point
			$crud->set_subject(lang('point_virtual'));
			$crud->required_fields('display_name');
			$crud->columns('id','display_name','chinese_name','english_name','expression_display','unit');
			$crud->fields('display_name','chinese_name','english_name','unit',
					'type_large','type_small','serve_sys','serve_obj');
				
			$crud->display_as('id',lang('point_id'));
			$crud->display_as('display_name',lang('point_display_name'));
			$crud->display_as('chinese_name',lang('point_chinese_name'));
			$crud->display_as('english_name',lang('point_english_name'));
			$crud->display_as('type',lang('point_type'));
			$crud->display_as('unit',lang('point_unit'));
			$crud->display_as('source_port',lang('point_port'));
			$crud->display_as('is_virtual_point',lang('point_is_virtual_point'));
			$crud->display_as('expression_display',lang('point_expression'));
			$crud->display_as('index',lang('point_index'));
			$crud->display_as('is_bacnet',lang('point_is_bacnet'));
			$crud->display_as('type_large',lang('point_type_large'));
			$crud->display_as('type_small',lang('point_type_small'));
			$crud->display_as('serve_sys',lang('point_serve_sys'));
			$crud->display_as('serve_obj',lang('point_serve_obj'));

			$crud->callback_field('type_large',array($this,'set_type_large'));
			$crud->callback_field('type_small',array($this,'set_type_small'));
			$crud->callback_field('serve_sys',array($this,'set_serve_sys'));
			$crud->callback_field('serve_obj',array($this,'set_serve_obj'));
			
			$crud->callback_after_insert(array($this, 'set_virtual'));
				
			//$crud->change_field_type('type', 'enum',array('AI','AO','BI','BO'));
			//$crud->change_field_type('expression', 'readonly');
			//$crud->set_field_upload('expression','assets/uploads/files');				
			//$crud->set_rules('source_port',lang('point_port'),'is_natural|required');	
			//$crud->set_rules('index',lang('point_index'),'is_natural');
			$crud->unset_print();
			//$crud->add_action(lang('point_modify'), '', '','ui-icon-search',array($this,'modify_point'));
			$crud->add_action(lang('point_expression'), '', '','expression-icon',array($this,'point_expression'));
			
			$state = $crud->getState();
			$stateInfo = $crud->getStateInfo();
			$output = $crud->render();
				
			$output->state = $state;
				
			if($state == 'edit')
			{
				$this->load->model('point_model','point');
				$info = $this->point->get_point_info($stateInfo->primary_key);
				$output->serve_sys = $info->serve_sys;
				$output->serve_obj = $info->serve_obj;
				$output->type_large = $info->type_large;
				$output->type_small = $info->type_small;
			}
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function set_virtual($post_array,$primary_key)
	{
		$vir = array(
				"is_virtual_point" => 1
		);
		$this->db->where('id', $primary_key);
		$this->db->update('point',$vir);
	
		return true;
	}
	
	
	public  function point_expression($primary_key , $row)
	{
		return site_url('calc/index').'/'.$row->id;
	}
	
	function set_type_large($value='', $primary_key = null) {
		if (current_lang() == "en")
			return "<select id='type-large'  name='type_large' onchange=\"select(where1, where1_display_english, this,'type-small')\"></select>";
		else 
			return "<select id='type-large'  name='type_large' onchange=\"select(where1, where1_display_chinese, this,'type-small')\"></select>";
	}
	function set_type_small($value='', $primary_key = null) {
		return "<select id='type-small' name='type_small'></select>";
	}
	function set_serve_sys($value='', $primary_key = null) {
		if (current_lang() == "en")
			return "<select id='serve-sys'  name='serve_sys'  onchange=\"select(where2, where2_display_english, this,'serve-obj')\"></select>";
		else
			return "<select id='serve-sys'  name='serve_sys'  onchange=\"select(where2, where2_display_chinese, this,'serve-obj')\"></select>";
	}
	function set_serve_obj($value='', $primary_key = null) {
		return "<select id='serve-obj' name='serve_obj'></select>";
	}
	
}
/* End of file point_virtual.php */
/* Location: controller/manage/point_virtual.php */

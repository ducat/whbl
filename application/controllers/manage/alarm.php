<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alarm extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("point_model", "point");
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('manage/alarm', array(
				'title' => lang('alarm_management'). "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'alarm',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/alarm/table');
	}
	

	public function table()
	{
		try{
		
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('point');
			$crud->set_subject(lang('alarm'));
			
			if (current_lang() == 'en'){
				$crud->columns('id','display_name','english_name','alarm_attack','alarm_release',
					'alarm_filter_start','alarm_filter_end','alarm_level','unit');
				$crud->fields('english_name','alarm_level','alarm_low','alarm_high','alarm_low_summer','alarm_high_summer',
					'alarm_low_winter','alarm_high_winter','alarm_attack','alarm_release',
					'alarm_filter_start','alarm_filter_end');
			}
			else {
				$crud->columns('id','display_name','chinese_name','alarm_attack','alarm_release',
						'alarm_filter_start','alarm_filter_end','alarm_level','unit');
				$crud->fields('chinese_name','alarm_level','alarm_low','alarm_high','alarm_low_summer','alarm_high_summer',
						'alarm_low_winter','alarm_high_winter','alarm_attack','alarm_release',
						'alarm_filter_start','alarm_filter_end');
			}
			
			$crud->required_fields('alarm_level');

			$crud->display_as('id',lang('point_id'));
			$crud->display_as('display_name',lang('point_display_name'));
			$crud->display_as('chinese_name',lang('point_chinese_name'));
			$crud->display_as('english_name',lang('point_english_name'));
			$crud->display_as('alarm_type',lang('alarm_type'));
			$crud->display_as('alarm_level',lang('alarm_level'));
			$crud->display_as('unit',lang('point_unit'));
				
			$crud->display_as('alarm_high',lang('alarm_high'));
			$crud->display_as('alarm_low',lang('alarm_low'));
			$crud->display_as('alarm_high_summer',lang('alarm_high_summer'));
			$crud->display_as('alarm_low_summer',lang('alarm_low_summer'));
			$crud->display_as('alarm_high_winter',lang('alarm_high_winter'));
			$crud->display_as('alarm_low_winter',lang('alarm_low_winter'));
			$crud->display_as('alarm_attack',lang('alarm_latency'));
			$crud->display_as('alarm_release',lang('alarm_recovery'));			
			$crud->display_as('alarm_filter_start',lang('alarm_filter_start'));
			$crud->display_as('alarm_filter_end',lang('alarm_filter_end'));
			$crud->display_as('proper_value_low',lang('proper_value_low'));
			$crud->display_as('proper_value_high',lang('proper_value_high'));

			$hours = array();
			for($i=0;$i<=24;$i++){
				$hours[(string)$i] = $i . ":00";
			}
			$crud->change_field_type('chinese_name', 'readonly');
			$crud->change_field_type('english_name', 'readonly');
			$crud->change_field_type('alarm_level','enum', array('2', '3', '4'));
			$crud->change_field_type('alarm_filter_start','dropdown', $hours);
			$crud->change_field_type('alarm_filter_end','dropdown', $hours);
			
				
			$crud->unset_print();
			$crud->unset_add();

			$crud->unset_delete();
			$output = $crud->render();
			$state = $crud->getState();
			$stateInfo = $crud->getStateInfo();
			
			$output->state = $state;
			$output->remove_season = true;
			if($state == 'edit')
			{
				$unit = $this->point->get_unit_by_id($stateInfo->primary_key);
				if($unit != null && $unit == 62)	
					$output->remove_season = false; 
			}
			
			
			$this->_example_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}
/* Not used now, replaced by alarm_point.php*/
/* End of file alarm.php */
/* Location: ./application/controllers/manage/alarm.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alarm_level extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
				
 		//$is_expired = strtotime(date("Y-m-d")) > strtotime("2013-12-25");
 		//$this->show_expired($is_expired);		
		//$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC1049", MONO_EDITION);
		//$this->show_errkey($status);
			
		$this->load->model("alarm_level_model","alarm_level");
	}

	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud_calendar', array(
			'title' => lang('alarm_level_manage'). "__" . lang('manage'),
			'user' => $this->user,
			'menu_map' => $this->menu_map,
			'active' => 'alarm_level',
			'output'=>$output
			));
	}
	
	public function index()
	{
		redirect('manage/alarm_level/table');
	}

	public function table() {
		try {
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('alarm_level');
			$crud->set_subject(lang('alarm_level'));
			$crud->where('level !=', 0);

			$crud->columns('level', 'mail_setting', 'upload_setting', 'recovery_time', 'delay_time', 'mail_gap_time', 
						   'duration', 'work_filter', 'mail_filter', 'is_clear', 'amount', 'mail_receivers');

			$crud->add_fields('level', 'mail_setting', 'upload_setting', 'recovery_time', 'delay_time', 'mail_gap_time', 
						   'duration', 'work_filter', 'mail_filter', 'is_clear', 'amount', 'mail_receivers');
			$crud->edit_fields('level', 'mail_setting', 'upload_setting', 'is_clear', 'recovery_time', 'delay_time', 'mail_gap_time', 
						   'duration', 'amount', 'mail_receivers', 'work_filter', 'mail_filter');


			$crud->display_as('level', lang('alarm_manage_level'));
			$crud->display_as('mail_setting', lang('alarm_manage_mail_send'));
			$crud->display_as('upload_setting', lang('alarm_manage_upload'));
			$crud->display_as('recovery_time', lang('alarm_manage_recovery_time') . '(s)');
			$crud->display_as('delay_time', lang('alarm_manage_delay_time') . '(s)');
			$crud->display_as('mail_gap_time', lang('alarm_manage_mail_gap') . '(h)');
			$crud->display_as('duration', lang('alarm_manage_duration') . '(h)');
			$crud->display_as('work_filter', lang('alarm_manage_work_filter'));
			$crud->display_as('mail_filter', lang('alarm_manage_mail_filter'));
			$crud->display_as('is_clear', lang('alarm_manage_clear'));
			$crud->display_as('amount', lang('alarm_manage_amount'));
			$crud->display_as('mail_receivers', lang('alarm_manage_mail_receivers'));

			$crud->field_type('level', 'readonly');
			$crud->field_type('mail_setting', 'dropdown', array("0"=>lang("no"), "1"=>lang("yes")));
			$crud->field_type('upload_setting', 'dropdown', array("0"=>lang("no"), "1"=>lang("yes")));
			$crud->field_type('is_clear', 'dropdown', array("0"=>lang("no"), "1"=>lang("yes")));

			$crud->set_rules('recovery_time', lang('alarm_manage_recovery_time'), "trim|required|is_natural");
			$crud->set_rules('delay_time', lang('alarm_manage_delay_time'), "trim|required|is_natural");
			$crud->set_rules('mail_gap_time', lang('alarm_manage_mail_gap'), "trim|required");
			$crud->set_rules('duration', lang('alarm_manage_duration'), "trim|required");
			$crud->set_rules('amount', lang('alarm_manage_amount'), "trim|required|is_natural");

			$crud->callback_field('work_filter',array($this,'set_work_filter'));
			$crud->callback_field('mail_filter',array($this,'set_mail_filter'));


			$crud->unset_print();
			$crud->unset_add ();
			$crud->unset_delete ();
			
			$output = $crud->render();
			$this->_example_output($output);
			
		} catch (Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function set_work_filter($value='', $primary_key = null) {
		$filter_time = $this->alarm_level->get_filter_time ($primary_key);
		$work_filter_time = explode (',', $filter_time->work_filter);
		$work_event_data = "";

		for ($i=0; $i < count($work_filter_time) && count($work_filter_time) > 1; $i+=2) { 

			$start_hour = 0;
			$start_minute = 0;
			$end_hour = 0;
			$end_minute = 0;

			$arr = explode( '.', $work_filter_time[$i]);
			if (count($arr) == 1) {
				$start_hour = $arr [0];
				$start_minute = 0;
			}
			else {
				$start_hour = $arr [0];
				$start_minute = $arr [1] * 60 / 10;
			}

			$arr = explode( '.', $work_filter_time[$i + 1]);
			if (count($arr) == 1) {
				$end_hour = $arr [0];
				$end_minute = 0;
			}
			else {
				$end_hour = $arr [0];
				$end_minute = $arr [1] * 60 / 10;
			}

			$work_event_data = $work_event_data . "{'id':" . $work_filter_time[$i] . ", 'start': new Date(year, month, day, " . $start_hour . ", " . $start_minute . "), 'end': new Date(year, month, day, " . $end_hour . ", " . $end_minute . "), title: ''},";
		}

		return "<input type='hidden' id='work_filter' name='work_filter' value=''>
				<div style='width:500px' id='work_filter_cal'></div>
				<script type='text/javascript'>
					var year = new Date().getFullYear();
					var month = new Date().getMonth();
					var day = new Date().getDate();
					var eventData = {
						events : [" . $work_event_data . "]
					};
					$('#work_filter').val ('" . $filter_time->work_filter . "');
					$('#work_filter_cal').weekCalendar({
						timeFormat: 'g:i A',
						daysToShow : 1,
						timeslotsPerHour: 2,
						timeslotHeight : 20,
						buttons: false,
						businessHours: false,
						newEventText: '',
						allowEventDelete: true,
						defaultEventLength: 1,
						draggable: function (calEvent, eventElement) {					//disable draggable
							return false;
						},
						resizable: function (calEvent, eventElement) {					//disable resizable
							return false;
						},
						height: function($calendar){
							return 200;
						},
						eventNew: function(calEvent, element) {
							var id = get_event_hour ('work_filter', calEvent.start);
							calEvent.id = id;
							update_filter ('work_filter');
						},
						eventClick: function(calEvent, element, dayFreeBusyManager, 
											 calendar, clickEvent) {
							remove_filter_event ('work_filter', calEvent);
							update_filter ('work_filter');
						},
    					data: eventData
					});
				</script>";
	}

	function set_mail_filter($value='', $primary_key = null) {
		$filter_time = $this->alarm_level->get_filter_time ($primary_key);
		$mail_filter_time = explode (',', $filter_time->mail_filter);
		$mail_event_data = "";
		for ($i=0; $i < count($mail_filter_time) && count($mail_filter_time) > 1; $i+=2) { 

			$start_hour = 0;
			$start_minute = 0;
			$end_hour = 0;
			$end_minute = 0;

			$arr = explode( '.', $mail_filter_time[$i]);
			if (count($arr) == 1) {
				$start_hour = $arr [0];
				$start_minute = 0;
			}
			else {
				$start_hour = $arr [0];
				$start_minute = $arr [1] * 60 / 10;
			}

			$arr = explode( '.', $mail_filter_time[$i + 1]);
			if (count($arr) == 1) {
				$end_hour = $arr [0];
				$end_minute = 0;
			}
			else {
				$end_hour = $arr [0];
				$end_minute = $arr [1] * 60 / 10;
			}

			$mail_event_data = $mail_event_data . "{'id':" . $mail_filter_time[$i] . ", 'start': new Date(year, month, day, " . $start_hour . ", " . $start_minute . "), 'end': new Date(year, month, day, " . $end_hour . ", " . $end_minute . "), title: ''},";
		}

		return "<input type='hidden' id='mail_filter' name='mail_filter' value=''>
				<div style='width:500px' id='mail_filter_cal'></div>
				<script type='text/javascript'>
					var year = new Date().getFullYear();
					var month = new Date().getMonth();
					var day = new Date().getDate();

					var eventData = {
						events : [" . $mail_event_data . "]
					};
					$('#mail_filter').val ('" . $filter_time->mail_filter . "');

					$('#mail_filter_cal').weekCalendar({
						timeFormat: 'g:i A',
						daysToShow : 1,
						timeslotsPerHour: 2,
						timeslotHeight : 20,
						buttons: false,
						businessHours: false,
						newEventText: '',
						allowEventDelete: true,
						defaultEventLength: 1,
						draggable: function (calEvent, eventElement) {					//disable draggable
							return false;
						},
						resizable: function (calEvent, eventElement) {					//disable resizable
							return false;
						},
						height: function($calendar){
							return 200;
						},
						eventNew: function(calEvent, element) {
							var id = get_event_hour ('mail_filter', calEvent.start);
							calEvent.id = id;
							update_filter ('mail_filter');
						},
						eventClick: function(calEvent, element, dayFreeBusyManager, 
											 calendar, clickEvent) {
							remove_filter_event ('mail_filter', calEvent);
							update_filter ('mail_filter');
						},
    					data: eventData
					});
				</script>";
	}
}
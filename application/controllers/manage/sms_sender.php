<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms_sender extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('sms'). "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'sms_sender',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/sms_sender/table/edit/1');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			
			$crud->set_table('sms_senders');
			$crud->set_subject(lang('sms'));
				
			$crud->fields('com_port', 'pin', 'baudrate', 'device_type');
			$crud->required_fields('com_port', 'pin', 'device_type');
			$crud->display_as('pin',lang('sms_pin'));
			$crud->display_as('com_port',lang('sms_com'));
			$crud->display_as('device_type',lang('sms_device_type'));
			$crud->display_as('baudrate',lang('sms_baudrate'));
			$crud->field_type('device_type', 'enum', array('GSM','CDMA'));
	
			$crud->set_rules('baudrate',lang('sms_baudrate'),'is_natural|required');
				
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_print();
			$crud->unset_back_to_list();
			$output = $crud->render();
	
			$this->_example_output($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	

}
/* End of file sms_sender.php */
/* Location: controller/manage/sms_sender.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("translate_model", "translate");
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('system_config'). "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'system',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/system/table/edit/1');					//Default station id is 1
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			
			$crud->set_table('collector_configs');
			$crud->set_subject(lang('system_config'));

			if ($this->version == 'shangyao') {											//上药需要添加页面自动跳转功能，需要在这个页面设置交替周期
				$crud->fields('station_name', 'master_port', 'query_interval', 'refresh_dynamic_view', 'translate_csv');
				$crud->required_fields('station_name', 'master_port', 'query_interval', 'refresh_dynamic_view');
				$crud->display_as('refresh_dynamic_view',lang('system_refresh_interval') . "(second)");
				$crud->set_rules('refresh_dynamic_view',lang('system_refresh_interval'),'is_natural');
			}
			else {																		//非上药的版本不需要页面刷新周期设置
				$crud->fields('station_name', 'master_port', 'query_interval', 'translate_csv');
				$crud->required_fields('station_name', 'master_port', 'query_interval');
			}
			
				
			$this->config->set_item('grocery_crud_file_upload_allow_file_types','csv');
			$crud->display_as('station_name',lang('system_station_name'));
			$crud->display_as('broadcast_address',lang('system_broadcast_address'));
			$crud->display_as('master_port',lang('system_master_port'));
			$crud->display_as('translate_csv',lang('system_translate_csv') . lang('system_translate_empty'));
			$crud->display_as('log_receiving_maillist',lang('system_mail_receiver'));
			$crud->display_as('log_receiving_numberlist',lang('system_mesg_receiver'));
			$crud->display_as('query_interval',lang('system_query_interval'));
			$crud->display_as('notice_template',lang('system_notice_template'));
			
			$crud->set_rules('master_port',lang('system_master_port'),'is_natural');
			$crud->set_rules('broadcast_address',lang('system_broadcast_address'),'valid_ip');
			$crud->set_rules('server_ip',lang('system_center_ip'),'valid_ip');
			
			$crud->field_type('query_interval', 'dropdown', array(	'60000'  => lang('one_minute'),
																	'120000' => lang('two_minute'),
																	'180000' => lang('three_minute'),
																	'300000' => lang('five_minute'),
																	'600000' => lang('ten_minute'),
																	'1200000'=> lang('twenty_minute'),
																	'1800000'=> lang('thirty_minute'),
																	'3600000'=> lang('one_hour'),
																	'7200000'=> lang('two_hour'),
																	'14400000' => lang('three_hour'),
																	'21600000' => lang('six_hour'),
																	'28800000' => lang('eight_hour'),
																	'43200000' => lang('twelve_hour'),
																	'86400000' => lang('twenty_four_hour')
																	));
			$path = UPLOAD_CSV_ROOT;
			
			$crud->set_field_upload('translate_csv', $path);
			
			$crud->callback_after_upload(array($this,'read_trans_csv'));
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_print();
			$crud->unset_back_to_list();
			$output = $crud->render();
	
			$this->_example_output($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	/**
	 * 从上传的翻译表中读取翻译信息,并写入到翻译表中
	 * @param  [type] $uploader_response [description]
	 * @param  [type] $field_info        [description]
	 * @param  [type] $files_to_upload   [description]
	 * @return [type]                    [description]
	 */
	function read_trans_csv($uploader_response,$field_info, $files_to_upload)
	{
		$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;

		$trans_array = array();
		$file = fopen($file_uploaded,"r");	
		if(! feof($file)){
			$line = fgetcsv($file);	
			while(! feof($file))
			{
				$line = fgetcsv($file);
				$arr = array("id" => $line[0],
							"display_name" => $line[1],
							"chinese_name" => $line[2],
							"english_name" => $line[3],							
							"is_virtual_point" => 0,
							"is_bacnet" => 1,
							"type_large" => $line[4],
							"type_small" => $line[5],						
							"serve_sys" => $line[6],
							"serve_obj" => $line[7],
							"alarm_level" => $line[8],
							"upload" => 0
						);
				array_push($trans_array, $arr);
			}
		}		
		fclose($file);

		$this->translate->empty_translate();
		$this->translate->insert_into_translate($trans_array);
		
		return true;
	}
}
/* End of file system.php */
/* Location: controller/manage/system.php */

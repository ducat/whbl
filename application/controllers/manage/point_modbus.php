<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Point_modbus extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('manage/point', array(
				'title' => lang('point_modbus_management') . "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'point_modbus',
				'type' => 'modbus',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/point_modbus/table');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();	
			if (current_lang() == 'en')
				$crud->set_language("english");

			$crud->set_table('point');
			$crud->where('is_virtual_point',null);
			$crud->where('is_bacnet',null);
			$crud->where('id !=', 0);						//Do not display SYSTEM point
			$crud->set_subject(lang('point_modbus'));
			$crud->required_fields('display_name','type','expression','offset','byte');
			$crud->columns('id','display_name','chinese_name','english_name','type','expression','offset','byte','unit');
			$crud->fields('display_name','chinese_name','english_name','type','unit','expression','offset','byte',
					'type_large','type_small','serve_sys','serve_obj');
				
			$crud->display_as('id',lang('point_id'));
			$crud->display_as('display_name',lang('point_display_name'));
			$crud->display_as('chinese_name',lang('point_chinese_name'));
			$crud->display_as('english_name',lang('point_english_name'));
			$crud->display_as('type',lang('point_type'));
			$crud->display_as('unit',lang('point_unit'));
			$crud->display_as('is_virtual_point',lang('point_is_virtual_point'));
			$crud->display_as('expression',lang('point_modbus_index'));
			$crud->display_as('offset',lang('point_modbus_addr'));
			$crud->display_as('is_bacnet',lang('point_is_bacnet'));
			$crud->display_as('type_large',lang('point_type_large'));
			$crud->display_as('type_small',lang('point_type_small'));
			$crud->display_as('serve_sys',lang('point_serve_sys'));
			$crud->display_as('serve_obj',lang('point_serve_obj'));

			$crud->callback_field('type_large',array($this,'set_type_large'));
			$crud->callback_field('type_small',array($this,'set_type_small'));
			$crud->callback_field('serve_sys',array($this,'set_serve_sys'));
			$crud->callback_field('serve_obj',array($this,'set_serve_obj'));
			
			$crud->callback_after_insert(array($this, 'set_bit'));
			//$crud->add_action(lang('point_modify'), '', '','modify-icon',array($this,'modify_point'));
			$crud->change_field_type('type', 'enum',array('16UINT',
															'16INT',
															'32UINT',
															'32INT',
															'32UINT_SWAPPED',
															'32INT_SWAPPED',
															'32FLOAT',
															'32FLOAT_SWAPPED',
															'BINARY'));			
			
			$crud->set_rules('expression',lang('point_index'),'is_natural');
			$crud->unset_print();
			
			
			$state = $crud->getState();
			$stateInfo = $crud->getStateInfo();
			$output = $crud->render();
			
			$output->state = $state;
			
			if($state == 'edit')
			{
				$this->load->model('point_model','point');
				$info = $this->point->get_point_info($stateInfo->primary_key);
				$output->serve_sys = $info->serve_sys;
				$output->serve_obj = $info->serve_obj;
				$output->type_large = $info->type_large;
				$output->type_small = $info->type_small;
			}
			$this->_example_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public  function modify_point($primary_key , $row)
	{
		return site_url('manage/point_modbus/modify_point_table').'/'.$row->id;
	}
	
	function set_bit($post_array,$primary_key)
	{
		$bits = array(
				"is_virtual_point" => null,
				"is_bacnet" => null
		);
		$this->db->where('id', $primary_key);
		$this->db->update('point',$bits);
	
		return true;
	}
	
	function set_type_large($value='', $primary_key = null) {
		if (current_lang() == "en")
			return "<select id='type-large'  name='type_large' onchange=\"select(where1, where1_display_english, this,'type-small')\"></select>";
		else 
			return "<select id='type-large'  name='type_large' onchange=\"select(where1, where1_display_chinese, this,'type-small')\"></select>";
	}
	function set_type_small($value='', $primary_key = null) {
		return "<select id='type-small' name='type_small'></select>";
	}
	function set_serve_sys($value='', $primary_key = null) {
		if (current_lang() == "en")
			return "<select id='serve-sys'  name='serve_sys'  onchange=\"select(where2, where2_display_english, this,'serve-obj')\"></select>";
		else
			return "<select id='serve-sys'  name='serve_sys'  onchange=\"select(where2, where2_display_chinese, this,'serve-obj')\"></select>";
	}
	function set_serve_obj($value='', $primary_key = null) {
		return "<select id='serve-obj' name='serve_obj'></select>";
	}
	
	
	function single_point_table($point_id) //单个点位的数据表格，只有导出操作
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
	
			//$crud->set_theme('datatables');
			$crud->where('point_id',$point_id);
			$crud->set_table('point_data');
			$crud->set_subject(lang('report_point_data'));
			$crud->columns('timestamp','value');
			$crud->fields('id','timestamp','value');
			$crud->display_as('id',lang('report_id'));
			//$crud->display_as('point_id',lang('report_point_id'));
			$crud->display_as('value',lang('report_value'));
			$crud->display_as('timestamp',lang('report_timestamp'));
			$crud->display_as('batch',lang('report_batch'));
			$crud->change_field_type('id', 'readonly');
			$crud->change_field_type('timestamp', 'readonly');
			$crud->change_field_type('batch', 'readonly');
				
			$crud->unset_print();
			$crud->unset_add();
			$crud->unset_delete();
			$output = $crud->render();
			return $output;
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function modify_point_table($point_id)
	{
		$output = $this->single_point_table($point_id);
		$this->load->view('/analysis/modify_point.php', array(
				'title' => lang('point_modify'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'point_modbus',
				'output'=>$output
		));
	
	}
	
}
/* End of file point_modbus.php */
/* Location: controller/manage/point_modbus.php */

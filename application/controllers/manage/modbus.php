<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modbus extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('modbus_management'). "__" . lang('manage'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'modbus',
				'output'=>$output
				));
	}
	
	public function index()
	{
		redirect('manage/modbus/table');
	}
	
	public function table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('modbuses');
			$crud->set_subject(lang('modbus'));
			$crud->required_fields('comm_port_id','protocol','baud','parity','data_bit_len','stop_bit_len');
			$crud->columns('comm_port_id','protocol','baud','parity','data_bit_len','stop_bit_len');
			
			$crud->fields('comm_port_id','protocol','baud','parity','data_bit_len','stop_bit_len');
			
			$crud->display_as('protocol',lang('modbus_protocol'));
			$crud->display_as('baud',lang('modbus_baud'));
			$crud->display_as('parity',lang('modbus_parity'));
			$crud->display_as('data_bit_len',lang('modbus_data_bit_len'));
			$crud->display_as('stop_bit_len',lang('modbus_stop_bit_len'));
			$crud->display_as('comm_port_id',lang('modbus_comm_port_id'));
	
			
			$crud->set_rules('baud',lang('modbus_baud'),'is_natural|required');
			$crud->set_rules('data_bit_len',lang('modbus_data_bit_len'),'is_natural|required');
		//	$crud->set_rules('stop_bit_len',lang('modbus_stop_bit_len'),'is_natural|required');
			
			
			$crud->change_field_type('protocol', 'enum', array('RTU', 'ASCII'));
			$crud->change_field_type('parity', 'enum', array('None', 'Odd', 'Even'));
			$crud->change_field_type('stop_bit_len', 'enum', array('1', '1.5', '2.0','2.5'));
			
			$crud->unset_add();
			//$crud->unset_delete();
			$crud->unset_print();
			$output = $crud->render();
	
			$this->_example_output($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	//end of 采集管理

}

/* End of file modbus.php */
/* Location: controller/manage/modbus.php */

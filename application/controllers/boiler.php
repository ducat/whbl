<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Boiler extends MY_Controller {
	function __construct() {
		parent::__construct ();
	}
	public function index() {
		redirect ( 'boiler/boiler1' );
	}
	public function boiler1() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
}
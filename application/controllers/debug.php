<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Debug extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	public function index($type = "network", $user_id = 1) {
		$user_id = 0x00000001;
		switch ($type)
		{
			case "network":
				echo "Network Edition";
				$module_collector = 14; //only bacnet
				$module_website = 0; 
				break;
			case "network_plus":
				echo "Network Query Edition";
				$module_collector = 14; //only bacnet
				$module_website = 1;
				break;
			case "mono":
				echo "Mono Edition";
				$module_collector = 15;  //has modbus
				$module_website = 2;
				break;
			default:
				$module_collector = 14; //only bacnet
				$module_website = 0;
				echo "Unknown type Default Network Edition";
		}
		
		// Create COM object Com.CRockey4ND
		print "<ul>";
		print "<li><h3>Create Rockey4ND COM object</h3>";
		$Rockey = new COM("Com.CRockey4ND") or die("<h3>Unable to instanciate Rockey COM object</h3>");
		
		
		//Display COM interface;
		//com_print_typeinfo($Rockey);
		
		//Set Parameters
		print "<li><h3>Set Parameters</h3>";
	
		$Rockey->p1 = new VARIANT(0x70ef, VT_UI2);
		$Rockey->p2 = new VARIANT(0xbc4f, VT_UI2);
		$Rockey->p3 = new VARIANT(0x7038, VT_UI2);
		$Rockey->p4 = new VARIANT(0xbfbf, VT_UI2);
		
		//Find First Rockey
		print "<li><h3>Find First Rockey</h3>";
		$result = $Rockey->RockeyCM(1);
		if ($result==0) {
			print "<h4>Found, HID = ";
			printf("0x%08X </h4>", $Rockey->lp1 );
		} else {
			print "<h4>No Rockey is detected</h4>"	;
			exit();
		}
		
		//Open First Rockey
		print "<li><h3>Open First Rockey</h3>";
		$result = $Rockey->RockeyCM(3);
		if ($result==0) {
			print "<h4>Success</h4>";
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}
		
		//Write
		$text = "LDMC1049";
		print "<li><h3>Write \"$text\" to User Memory 1</h3>";
		$Rockey->buffer = new VARIANT($text, VT_BSTR);
		$Rockey->p1 = new VARIANT(0, VT_UI4);
		$Rockey->p2 = new VARIANT(floor(strlen($text)/2+1)*2, VT_UI4);
		$result = $Rockey->RockeyCM(6);
		if ($result==0) {
			print "<h4>Success</h4>";
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}
		
		//Read
		print "<li><h3>Read User Memory 1</h3>";
		$Rockey->buffer = new VARIANT($text, VT_BSTR);
		$Rockey->p1 = new VARIANT(0, VT_UI4);
		$Rockey->p2 = new VARIANT(500, VT_UI4);
		$result = $Rockey->RockeyCM(5);
		if ($result==0) {
			print "<h4>Success, Result: ".$Rockey->buffer."</h4>";
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}

		

		//Write User ID
		$Rockey->lp1 = new VARIANT($user_id, VT_UI4);
		printf("<li><h3>Write User ID 0x%8X</h3>",$Rockey->lp1);
		$result = $Rockey->RockeyCM(9);
		if ($result==0) {
			print "<h4>Success</h4>";
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}
		
		//Read User ID
		print "<li><h3>Read User ID</h3>";
		$result = $Rockey->RockeyCM(10);
		if ($result==0) {
			printf("<h4>Success, User ID: 0x%8X</h4>",$Rockey->lp1);
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}
		
		//clear Module Collector
		$Rockey->p1 = new VARIANT(14, VT_UI2);
		$Rockey->p2 = new VARIANT(0, VT_UI2);
		$Rockey->p3 = new VARIANT(0, VT_UI2);
		print "<li><h3>Set Module</h3>";
		$result = $Rockey->RockeyCM(11);
		if ($result==0) {
			printf("<h4>Set Collector Moudle %d: Pass = 0x%04X Decrease no allow</h4>", $module_collector, $Rockey->p2 & 0xFFFF);
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}
		$Rockey->p1 = new VARIANT(15, VT_UI2);
		$Rockey->p2 = new VARIANT(0, VT_UI2);
		$Rockey->p3 = new VARIANT(0, VT_UI2);
		print "<li><h3>Set Module</h3>";
		$result = $Rockey->RockeyCM(11);
		if ($result==0) {
			printf("<h4>Set Collector Moudle %d: Pass = 0x%04X Decrease no allow</h4>", $module_collector, $Rockey->p2 & 0xFFFF);
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}
		//Set Module Collector
		
		$Rockey->p1 = new VARIANT($module_collector, VT_UI2);
		$Rockey->p2 = new VARIANT(0x2121, VT_UI2);
		$Rockey->p3 = new VARIANT(0, VT_UI2);
		print "<li><h3>Set Module</h3>";
		$result = $Rockey->RockeyCM(11);
		if ($result==0) {
			printf("<h4>Set Collector Moudle %d: Pass = 0x%04X Decrease no allow</h4>", $module_collector, $Rockey->p2 & 0xFFFF);
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}
		
		//Set Module Website
		$Rockey->p1 = new VARIANT($module_website, VT_UI2);
		$Rockey->p2 = new VARIANT(0x2121, VT_UI2);
		$Rockey->p3 = new VARIANT(0, VT_UI2);
		print "<li><h3>Set Module</h3>";
		$result = $Rockey->RockeyCM(11);
		if ($result==0) {
			printf("<h4>Set Website Moudle %d: Pass = 0x%04X Decrease no allow</h4>", $module_website, $Rockey->p2 & 0xFFFF);
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}
		
		
		$Rockey->p1 = new VARIANT($module_collector, VT_UI2);
		print "<li><h3>Check Collector Module</h3>";
		$result = $Rockey->RockeyCM(12);
		if ($result==0) {
			printf("<h4>Check Collector Moudle %d: %s Decrease: %s</h4>", $module_collector, $Rockey->p2?"Allow":"Not Allow", $Rockey->p3?"Allow":"Not Allow");
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}
		
		$Rockey->p1 = new VARIANT($module_website, VT_UI2);
		print "<li><h3>Check Website Module</h3>";
		$result = $Rockey->RockeyCM(12);
		if ($result==0) {
			printf("<h4>Check Website Moudle %d: %s Decrease: %s</h4>", $module_website, $Rockey->p2?"Allow":"Not Allow", $Rockey->p3?"Allow":"Not Allow");
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}
		
				
		print "<li><h3>Close Rockey</h3>";
		$result = $Rockey->RockeyCM(4);
		if ($result==0) {
			print "<h4>Success</h4>";
		} else {
			print "<h4>Failed, Error Code: $result</h4>";
			exit();
		}	
	}
}
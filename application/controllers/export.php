<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This file is part of the exporting module for Highcharts JS.
 * www.highcharts.com/license
 *
 *
 * Available POST variables:
 *
 * $filename  string   The desired filename without extension
 * $type      string   The MIME type for export.
 * $width     int      The pixel width of the exported raster image. The height is calculated.
 * $svg       string   The SVG source code to convert.
 */

//require('/assets/fpdf/index.php');
class Export extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model("alarm_log_model","logs");
	}
	
	public function index() {
		// Options
		
		define ('BATIK_PATH', '.\\batik-rasterizer.jar');
		
		///////////////////////////////////////////////////////////////////////////////
		ini_set('magic_quotes_gpc', 'off');
		
		$type = $_POST['type'];
		$svg = (string) $_POST['svg'];
		$filename = (string) $_POST['filename'];
		
		// prepare variables
		if (!$filename) $filename = 'chart';
		if (get_magic_quotes_gpc()) {
			$svg = stripslashes($svg);
		}
		
		// check for malicious attack in SVG
		if(strpos($svg,"<!ENTITY") !== false){
			exit("Execution is topped, the posted SVG could contain code for a mailcious attack");
		}
		
		$tempName = md5(rand());
		
		// allow no other than predefined types
		if ($type == 'image/png') {
			$typeString = '-m image/png';
			$ext = 'png';
		
		} elseif ($type == 'image/jpeg') {
			$typeString = '-m image/jpeg';
			$ext = 'jpg';
		
		} elseif ($type == 'application/pdf') {
			$typeString = '-m application/pdf';
			$ext = 'pdf';
		
		} elseif ($type == 'image/svg+xml') {
			$ext = 'svg';
		}
		$outfile = ".\\assets\\chart_export\\$tempName.$ext";
		
		if (isset($typeString)) {
		
			// size
			if ($_POST['width']) {
				$width = (int)$_POST['width'];
				if ($width) $width = "-w $width";
			}
		
			// generate the temporary file
			if (!file_put_contents(".\\assets\\chart_export\\$tempName.svg", $svg)) {
				die("Couldn't create temporary file. Check that the directory permissions for
			the /temp directory are set to 777.");
			}
		
			$command = "java -jar ". BATIK_PATH ." $typeString -d $outfile $width .\\assets\\chart_export\\$tempName.svg";
			// do the conversion
			$output = shell_exec($command);
			
			// catch error
			if (!is_file($outfile) || filesize($outfile) < 10) {
				echo "<pre>$output</pre>";
				echo "Error while converting SVG. ";
		
				if (strpos($output, 'SVGConverter.error.while.rasterizing.file') !== false) {
					echo "
			<h4>Debug steps</h4>
			<ol>
			<li>Copy the SVG:<br/><textarea rows=5>" . htmlentities(str_replace('>', ">\n", $svg)) . "</textarea></li>
			<li>Go to <a href='http://validator.w3.org/#validate_by_input' target='_blank'>validator.w3.org/#validate_by_input</a></li>
			<li>Paste the SVG</li>
			<li>Click More Options and select SVG 1.1 for Use Doctype</li>
			<li>Click the Check button</li>
			</ol>";
				}
			}
			

			// stream it
			else {
				header("Content-Disposition: attachment; filename=\"$filename.$ext\"");
				header("Content-Type: $type");
				echo file_get_contents($outfile);
			}
				
			// delete it
			//unlink(".\\assets\\chart_export\\$tempName.svg");
			//unlink($outfile);
		
			// SVG can be streamed directly back
			
			
		} else if ($ext == 'svg') {
			header("Content-Disposition: attachment; filename=\"$filename.$ext\"");
			header("Content-Type: $type; charset=UTF-8");
			echo $svg;
		
		} else {
			echo "Invalid type";
		}
		
		
	}
/*

	public function exportpdf(){
		$condition = $this->session->userdata("condition");
		$arr = $this->logs->get_search_data_all ($condition);
		for($i=0;$i<count($arr);$i++){
			unset($arr[$i]['id']);
		}
		$pdf = new pdf();
		$header=array('时间','报警报文','报警点位','点位名称','报警数值','报警等级','发送状态');
		$headerw=array('24','24','24','24','24','24','24');
		$pdf->pdftable($header,$headerw,5,$arr); //列标题，列宽度，字体大小，数据
	}
*/
	/**
	 * 导出报警记录到 execl 文件中
	 * @return [type] [description]
	 */
	public function exportexcel(){
		$condition = $this->session->userdata("condition");
		$arr = $this->logs->get_search_data_all ($condition);
		$fname=iconv("utf-8","gbk","报警记录");
		header("Content-Type: application/vnd.ms-execl");
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=".$fname.".xls");
		header("Expires: 0");

		echo iconv("utf-8","gbk","时间\t报警报文\t报警点位\t点位名称\t报警数值\t报警等级\t发送状态\n");
		foreach($arr as $v){ //循环输出表体
			if ($v['sent'] == 0) 
				$v['sent'] = lang('alarm_not_sent');
			else if ($v['sent'] < 0)
				$v['sent'] = lang('alarm_failed_sent');
			else if ($v['sent'] > 0)
				$v['sent'] = lang('alarm_success_sent');
                
			echo iconv("utf-8","gbk",
					$v['timestamp']."\t".
					$v['description']."\t".
					$v['display_name']."\t".
					$v['name']."\t".
					$v['value']."\t".
					$v['level']."\t".
					$v['sent']."\n"
			);
		}
	}
}
/* End of file export.php */
/* Location: controller/export.php */
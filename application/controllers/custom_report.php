<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom_report extends MY_Controller {

	function __construct()
	{
		parent::__construct();	

 		//$is_expired = strtotime(date("Y-m-d")) > strtotime("2013-12-25");
 		//$this->show_expired($is_expired);
		
		//$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC10491010000000000000MONO", MONO_EDITION);
		//$this->show_errkey($status);
		
		
		$this->load->model('Custom_report_model', 'custom_report');
	}	
	public function index() {
		redirect('custom_report/custom_report_thumbnail');
		
	}
	public function custom_report_thumbnail(){
		$report_info = $this->custom_report->get_all_with_name();
		$this->load->view('custom_report/thumbnail', array(
			'user' => $this->user,
			'menu_map' => $this->menu_map,
			'reports' => $report_info
			));		
	}
	
	
	public function custom_report_view($id) {
		if(!$id) {
			show_404();
			return;
		}
		
		$report_info = $this->custom_report->get_by_id($id);
		if(!$report_info) {
			show_404();
			return;
		}

		$points = array();
		$point_ids = $this->custom_report->get_multi_point_by_report_id($id);

		foreach ($point_ids as $row){
			if($row->point_id != null){
				$point = array(	"id" => $row->point_id, 
								"name" => $row->chinese_name, 
								"unit" => $row->unit == null? "null" : $row->unit);
				array_push($points, $point);
			}
				
		}
		
		$report = array( 'report_info' => $report_info , 'points' => $points );

		$this->load->view('custom_report/view', array(
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'custom_report' => (object)$report,
		));
	}
}
/* End of file custom_report.php */
/* Location: controller/custom_report.php */
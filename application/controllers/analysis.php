<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Analysis extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//$is_expired = strtotime(date("Y-m-d")) > strtotime("2013-12-25");
		//$this->show_expired($is_expired);
		
		//$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC1049", MONO_EDITION);
		//$this->show_errkey($status);
		
		//$this->load->helper('calc');  to delete
		$this->load->model('Point_model','point');
		$this->load->model('Point_data_model','point_data');
		$this->load->model('Charge_model','charge');
		$this->load->model('Budget_model','budget');
	}	

	public function index() {
		redirect(site_url('analysis/point_chart'));
	}
	
	public function budget_view()
	{

		$budget_arr = array();
		$items = $this->budget->get_all_budget();

		foreach ($items as $row){
			array_push($budget_arr, array("name"=>$row->name, "id"=>$row->id));
		}
		$this->load->view('analysis/select_budget.php',array(
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'budget' => $budget_arr
		));
	}
	public function budget_show()
	{
		$cur_year = (int)date("Y"); 
		if (isset($_POST['base']))
			$base = $_POST['base'];
		else {
			$item = $this->budget->get_budget_by_id($cur_year - 1);
			$base = $item == null? 0 : $item->year;
		}
		
		if (isset($_POST['compare']))
			$compare = $_POST['compare'];
		else{
			$item = $this->budget->get_budget_by_id($cur_year - 2);
			$compare = $item == null?  0: $item->year;
		}
		
		$base_items = $this->budget->get_budget_by_id($base);
		$compare_items = $this->budget->get_budget_by_id($compare);
		
		$base_data = array();
		$compare_data = array();
		if($base_items != null && $compare_items != null){
			array_push($base_data, $base_items->jan);
			array_push($base_data, $base_items->feb);
			array_push($base_data, $base_items->mar);
			array_push($base_data, $base_items->apr);
			array_push($base_data, $base_items->may);
			array_push($base_data, $base_items->jun);
			array_push($base_data, $base_items->jul);
			array_push($base_data, $base_items->aug);
			array_push($base_data, $base_items->sep);
			array_push($base_data, $base_items->oct);
			array_push($base_data, $base_items->nov);
			array_push($base_data, $base_items->dec);
			
			array_push($compare_data, $compare_items->jan);
			array_push($compare_data, $compare_items->feb);
			array_push($compare_data, $compare_items->mar);
			array_push($compare_data, $compare_items->apr);
			array_push($compare_data, $compare_items->may);
			array_push($compare_data, $compare_items->jun);
			array_push($compare_data, $compare_items->jul);
			array_push($compare_data, $compare_items->aug);
			array_push($compare_data, $compare_items->sep);
			array_push($compare_data, $compare_items->oct);
			array_push($compare_data, $compare_items->nov);
			array_push($compare_data, $compare_items->dec);
		
			$data = array('base' => $base_data,
						  'compare' => $compare_data);
			$info['base']['name'] =  $base_items->name;
			$info['base']['unit'] =  $base_items->unit;
			$info['base']['year'] =  $base_items->year;
			$info['compare']['name'] =  $compare_items->name;
			$info['compare']['unit'] =  $compare_items->unit;
			$info['compare']['year'] =  $compare_items->year;
			$result = array('data' => $data, 'info' => $info);
		}
		else{
			$result = null;
		}
		$this->load->view('analysis/budget', array(
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'result' => $result
				));
	}
	

		
	public function point_chart()
	{
		redirect('analysis/table/chart');
	}
	
	public function point_table()
	{
		redirect('analysis/table/table');
	}
	
	public function show_table($id="")
	{
		redirect('analysis/single_point_table/' . $id);
	}
	
	public function show_chart($point_id="")
	{
		$result = array();
		$info = $this->point->get_point_info($point_id);
		$time_range = $this->point_data->get_time_range($point_id);
		$year_month = array();
		$items = $this->point_data->get_time_year_month($point_id);

		foreach ($items as $row){
			if (!array_key_exists($row->year, $year_month)){				
				$year_month[$row->year] = array();
			}
			array_push($year_month[$row->year], $row->month);
		}
		$this->load->view('/analysis/select_chart.php',array(
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'year_month' => $year_month,
				'info' => $info,
				'start' => date("Y-m-d",strtotime($time_range['min'])), 
				'end' => date("Y-m-d",strtotime($time_range['max']))
		));
	}
	
	public function _example_output($output = null, $type)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('point_' . $type) . "__" . lang('analysis'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'point_' . $type,
				'output'=>$output
		));
	}

	public function _example_output_single_pt_tb($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('point_report'). "__" . lang('analysis'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'point_table',
				'output'=>$output
		));
	}
	
	public function single_point_report($point_id="") 
	{
		$report_type = $_POST['report_type'];

		$result = array();
		$this->load->model('point_model', 'point');
		$this->load->model('point_data_model', 'point_data');
		$info = $this->point->get_point_info($point_id);

		$result['id'] = $info->id;
		$result['chinese_name'] = $info->chinese_name;
		$result['english_name'] = $info->english_name;
		$result['type'] = $info->type;
		$result['unit'] = $info->unit;
		$result['is_virtual'] = $info->is_virtual_point;
		$result['type_large'] = $info->type_large;
		
		if($report_type == "year"){
			$year=$_POST["start_year"];
			
			$data = $this->point_data->get_year_data_by_point_id_year($point_id, $year, $info->type_large);
			$result['data'] = $data;
			$result['year'] = $year;
			$this->load->view('/analysis/single_point_report_year.php',array(
					'user' => $this->user,
					'menu_map' => $this->menu_map,
					'point' => $result
			));
		}
		else if($report_type == "month"){
			$year=$_POST["start_year"];
			$month=$_POST["start_month"];
			$data = $this->point_data->get_month_data_by_point_id_month($point_id,$year,$month,$info->type_large);
			$result['data'] = $data;
			$result['year'] = $year;
			$result['month'] = $month;
			$this->load->view('/analysis/single_point_report_month.php',array(
					'user' => $this->user,
					'menu_map' => $this->menu_map,
					'point' => $result
			));
		}
		else if($report_type == "day"){
			$from = $_POST['from'];
			$to = $_POST['to'];
			$data = $this->point_data->get_day_data_by_point_id_time_range($point_id,$from,$to,$info->type_large);
			$result['data'] = $data;
			$result['from'] = $from;
			$result['to'] = $to;
			$this->load->view('/analysis/single_point_report_day.php',array(
					'user' => $this->user,
					'menu_map' => $this->menu_map,
					'point' => $result
			));
		}
		else if($report_type == "runtime"){
			$from = $_POST['from'];
			$to = $_POST['to'];
			$data = $this->point_data->get_data_by_point_id_time_range($point_id,$from,$to,$info->type_large);
			$result['data'] = $data;
			$result['from'] = $from;
			$result['to'] = $to;
			$this->load->view('/analysis/point_chart.php',array(
					'user' => $this->user,
					'menu_map' => $this->menu_map,
					'start' => strtotime($from . " 00:00:00") . "000",
					'end' => strtotime($to . " 23:59:59") . "000",
					'url' => site_url("analysis/get_single_point_data"),
					'point' => $result
					));
		}
	}	

	
	private function get_13bit_timestamp($time_10bit)
	{
		return strtotime($time_10bit) . '000';				
	}
	
	private function prepare_single_point_chart_data($point_id)
	{
		$info = $this->point->get_point_info($point_id);
		$chart = array(
				'chinese_name' => $info->chinese_name,
				'english_name' => $info->english_name,
				'data_url' => 'analysis/get_single_point_data/' . $point_id,
		);	
		return (object)$chart;	
	}
	

	public function get_single_point_data() //highcharts 请求数据
	{
		$callback = !empty($_GET['callback']) ? $_GET['callback'] : '';
		if (!preg_match('/^[a-zA-Z0-9_]+$/', $callback)) {
			die('Invalid callback name');
		}		
		$point_id = !empty($_GET['id']) ? $_GET['id'] : '';
		if (!preg_match('/^[0-9]+$/', $point_id)) {
			die('Invalid point id');
		}		
		$start = !empty($_GET['start']) ? $_GET['start'] : '0';
		if ($start && !preg_match('/^[0-9]+$/', $start)) {
			die("Invalid start parameter: $start");
		}		
		$end = !empty($_GET['end']) ? $_GET['end'] : strtotime(date("Y-m-d H:i:s"))."000";
		if ($end && !preg_match('/^[0-9]+$/', $end)) {
			die("Invalid end parameter: $end");
		}
		
		$this->load->model("point_model",'point');
		$info = $this->point->get_point_info($point_id);
		// set some utility variables
		$start = $start / 1000;
		$start = $start - $start % 60;
		$end = $end / 1000;
		$end = $end - $end % 60;
		$range = $end - $start;
		
		$startTime = strftime('%Y-%m-%d %H:%M:%S', $start);
		$endTime = strftime('%Y-%m-%d %H:%M:%S', $end);
			
		// find the right table
		// two days range loads minute data
		if ($range < 7 * 24 * 3600) {
			$data = $this->point_data-> get_data_by_point_id_time_range($point_id, $startTime, $endTime,$info->type_large);		
		// one month range loads hourly data
		} elseif ($range < 90 * 24 * 3600) {
			$data = $this->point_data-> get_hour_data_for_runtime($point_id, $startTime, $endTime,$info->type_large);
		// one year range loads daily data
		} else {
			if($info->type_large == 'binary'){
				$data = $this->point_data-> get_hour_data_for_runtime($point_id, $startTime, $endTime,$info->type_large);	
			}
			else{
				$data = $this->point_data-> get_day_data_for_runtime($point_id, $startTime, $endTime,$info->type_large);
			}
			
		}
		$rows = array();
		foreach ($data as $row)
		{
			$time_origin = $row->timestamp;
			$time = strtotime(substr($time_origin,0,-2). "00")."000";
			$rows[] = "[$time,$row->value]";
		}		
		// print it
		header('Content-Type: text/javascript');
		echo "/* console.log(' start = $start, end = $end, startTime = $startTime, endTime = $endTime '); */";
		echo $callback ."([\n" . join(",\n", $rows) ."\n]);";
	}
	
	public function table($type="chart")
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			
			$crud->set_table('point');
			$crud->set_subject(lang('point_' . $type));
			if (current_lang() == 'en'){
				$crud->columns('id','display_name','english_name','unit','type_large');
			}
			else 
				$crud->columns('id','display_name','chinese_name','unit','type_large');
			
			$crud->where("deleted", 0);
			$crud->display_as('id',lang('point_id'));
			$crud->display_as('display_name',lang('point_display_name'));
			$crud->display_as('chinese_name',lang('point_chinese_name'));
			$crud->display_as('english_name',lang('point_english_name'));
			$crud->display_as('type',lang('point_type'));
			$crud->display_as('unit',lang('point_unit'));
			$crud->display_as('source_port',lang('point_port'));
			$crud->display_as('is_virtual_point',lang('point_is_virtual_point'));
			$crud->display_as('expression',lang('point_bacnet_identi'));
			$crud->display_as('index',lang('point_index'));
			$crud->display_as('is_bacnet',lang('point_is_bacnet'));
			$crud->display_as('type_large',lang('point_type_large'));
			$crud->display_as('type_small',lang('point_type_small'));
			$crud->display_as('serve_sys',lang('point_serve_sys'));
			$crud->display_as('serve_obj',lang('point_serve_obj'));
				
			$crud->callback_column('type_large',array($this,'_callback_type_large'));
			if ($type == "chart")
				$crud->add_action(lang('point_chart'), '', '','chart-icon',array($this,'_show_chart'));
			else if ($type == "table")
				$crud->add_action(lang('point_report'), '', '','report-icon',array($this,'_show_table'));
			else
			{
				show_404();
				return;
			}
			
			$crud->unset_print();
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$output = $crud->render();
	
			$this->_example_output($output, $type);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function _show_chart($primary_key , $row)
	{
		return site_url('analysis/show_chart/'.$primary_key);
	}
	
	function _show_table($primary_key , $row)
	{
		return site_url('analysis/show_table/'.$primary_key);
	}
	
	public function _callback_type_large($value, $row)
	{
		if($row->type_large == 'transient')
			return lang("point_transient");
		elseif ($row->type_large == 'accumulated')
			return lang("point_accumulated");
		elseif ($row->type_large == 'binary')
			return lang("point_binary");
		else  
			return null;
	}
	
	public function single_point_table($id="")
	{
		$info = $this->point->get_point_info($id);
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('point_data');
			if (current_lang() == 'en')
				$crud->set_subject(lang('point_report') . "__" . $info->english_name);
			else
				$crud->set_subject(lang('point_report') . "__" . $info->chinese_name);
			if ($info->type_large == 'accumulated')
				$crud->columns('point_id','timestamp','delta');
			else 
				$crud->columns('point_id','timestamp','value');
			$crud->where("point_id", $id);	
			$crud->set_relation("point_id", "point", "{chinese_name}");
			$crud->display_as('point_id',lang('report_point_id'));
			$crud->display_as('value',lang('report_value'));
			$crud->display_as('delta',lang('report_delta'));
			$crud->display_as('timestamp',lang('report_timestamp'));
			$crud->order_by("timestamp","desc");
			$crud->unset_print();
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$output = $crud->render();
	
			$this->_example_output_single_pt_tb($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	
	public function _example_output_base_tb($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('base_view'). "__" . lang('analysis'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'base_table',
				'output'=>$output
		));
	}
	
	public function base_table()
	{
	try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();			
			if (current_lang() == 'en')
				$crud->set_language("english");
			$crud->set_table('base');
			$crud->set_subject(lang('base'));
			$crud->required_fields('name','point_id','type');
			
			$crud->columns('id','name','point_id','type','start_time','end_time','value');
			$crud->fields('name','point_id','type','start_time','end_time');
				
			$crud->display_as('id',lang('base_id'));
			$crud->display_as('name',lang('base_name'));
			$crud->display_as('point_id',lang('base_point_id'));
			$crud->display_as('type',lang('base_type'));
			$crud->display_as('start_time',lang('base_start_time'));
			$crud->display_as('end_time',lang('base_end_time'));
			$crud->display_as('value',lang('base_value'));

			if (current_lang() == 'en')
				$crud->set_relation('point_id','point','english_name',array('deleted'=>0));
			else
				$crud->set_relation('point_id','point','chinese_name',array('deleted'=>0));
			
			$crud->field_type('type', 'dropdown', 
				array(	'max'=>lang('base_max'),
						'min'=>lang('base_min'), 
						'avg'=>lang('base_avg'),
						'sum'=>lang('base_sum'))
					);
				
			//$crud->set_rules('year',lang('budget_year'),'is_unique[budget.year]|integer');

			$crud->callback_after_insert(array($this, 'calc_value'));
			$crud->callback_after_update(array($this, 'calc_value'));
				
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->unset_print();
			$output = $crud->render();
	
			$this->_example_output_base_tb($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function _example_output_charge_tb($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('charge_view'). "__" . lang('analysis'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'charge_table',
				'output'=>$output
		));
	}
	
	public function charge_table()
	{
		try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");
			//$crud->set_theme('datatables');
			$crud->set_table('charge');
			$crud->set_subject(lang('charge'));
			$crud->columns('name','peak','valley','flat','related_point');
			$crud->fields('name','peak_time','peak','valley_time','valley',
					'flat','related_point');
			$crud->required_fields('name','flat','peak','valley');
			$crud->set_relation('related_point','point','{chinese_name}',array('deleted'=>0));
				
			$crud->display_as('name',lang('charge_name'));
			$crud->display_as('peak_time',lang('charge_peak_time'));
			$crud->display_as('peak',lang('charge_peak'));
			$crud->display_as('valley_time',lang('charge_valley_time'));
			$crud->display_as('valley',lang('charge_valley'));
			$crud->display_as('flat_time',lang('charge_flat_time') . lang('charge_refresh'));
			$crud->display_as('flat',lang('charge_flat'));
			$crud->display_as('related_point',lang('charge_related_point'));

			$crud->add_action(lang('point_table'), '', '','chart-icon',array($this,'_show_charge'));

			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->unset_print();
			$output = $crud->render();
	
			$this->_example_output_charge_tb($output);
	
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function _show_charge($primary_key , $row)
	{
		return site_url('analysis/show_charge/'.$primary_key);
	}
	
	public function show_charge($charge_id="")
	{
		$result = array();
		$info = $this->charge->get_charge_info($charge_id);
		$time_range = $this->point_data->get_time_range($info->related_point);
		$year_month = array();
		$items = $this->point_data->get_time_year_month($info->related_point);

		foreach ($items as $row){
			if (!array_key_exists($row->year, $year_month)){
				$year_month[$row->year] = array();
			}
			array_push($year_month[$row->year], $row->month);
		}
		$this->load->view('/charge/select_charge.php',array(
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'year_month' => $year_month,
				'info' => $info,
				'start' => date("Y-m-d",strtotime($time_range['min'])),
				'end' => date("Y-m-d",strtotime($time_range['max']))
		));
	}
	
	public function charge_report($charge_id="")
	{
		$report_type = $_POST['report_type'];
	
		$result = array();
		$info = $this->charge->get_charge_info($charge_id);
	
		$result['id'] = $info->id;
		$result['name'] = $info->name;
		$result['peak_charge'] = $info->peak;
		$result['valley_charge'] = $info->valley;
		$result['flat_charge'] = $info->flat;
				
		$peak_arr = explode(",",$info->peak_time);
		$flat_arr = explode(",",$info->flat_time);
		$valley_arr = explode(",",$info->valley_time);

		$all_arr = array();
		for($i=0;$i<=23;$i++){
			array_push($all_arr, $i);
		}
	
		if($report_type == "year"){
			$year=$_POST["start_year"];
				
			$peak = $this->charge->get_arr_year($info->related_point,$peak_arr, $year);
			$flat = $this->charge->get_arr_year($info->related_point,$flat_arr, $year);
			$valley = $this->charge->get_arr_year($info->related_point,$valley_arr, $year);
			$all = $this->charge->get_arr_year($info->related_point,$all_arr, $year);
			
			$pairs = array();
			foreach ($peak as $key => $val){
				$pairs[$key]['peak'] = $val;
			}
			foreach ($flat as $key => $val){
				$pairs[$key]['flat'] = $val;
			}
			foreach ($valley as $key => $val){
				$pairs[$key]['valley'] = $val;
			}
			foreach ($all as $key => $val){
				if (!isset($pairs[$key]['flat']))
					$flat_calc = 0;
				else
					$flat_calc = $pairs[$key]['flat'];
				if (!isset($pairs[$key]['valley']))
					$valley_calc = 0;
				else
					$valley_calc = $pairs[$key]['valley'];
				if (!isset($pairs[$key]['peak']))
					$peak_calc = 0;
				else
					$peak_calc = $pairs[$key]['peak'];
				$pairs[$key]['all'] = $peak_calc * $result['peak_charge'] + $flat_calc * $result['flat_charge'] +
				$valley_calc * $result['valley_charge'];
			}
				
			foreach ($pairs as $key => $val){
				if(!isset($pairs[$key]['peak']))
					$pairs[$key]['peak'] = 0;
				if(!isset($pairs[$key]['flat']))
					$pairs[$key]['flat'] = 0;
				if(!isset($pairs[$key]['valley']))
					$pairs[$key]['valley'] = 0;
				if(!isset($pairs[$key]['all']))
					$pairs[$key]['all'] = 0;
			}
			
			$result['data'] = $pairs;
			$result['year'] = $year;
			$this->load->view('/charge/charge_year.php',array(
					'user' => $this->user,
					'menu_map' => $this->menu_map,
					'charge' => $result
			));
		}
		else if($report_type == "month"){
			$year=$_POST["start_year"];
			$month=$_POST["start_month"];
			$peak = $this->charge->get_arr_month($info->related_point,$peak_arr,$year,$month);
			$flat = $this->charge->get_arr_month($info->related_point,$flat_arr,$year,$month);
			$valley = $this->charge->get_arr_month($info->related_point,$valley_arr,$year,$month);
			$all = $this->charge->get_arr_month($info->related_point,$all_arr,$year,$month);
			

			$pairs = array();
			foreach ($peak as $key => $val){
				$pairs[$key]['peak'] = $val;
			}
			foreach ($flat as $key => $val){
				$pairs[$key]['flat'] = $val;
			}
			foreach ($valley as $key => $val){
				$pairs[$key]['valley'] = $val;
			}
			foreach ($all as $key => $val){
				if (!isset($pairs[$key]['flat']))
					$flat_calc = 0;
				else
					$flat_calc = $pairs[$key]['flat'];
				if (!isset($pairs[$key]['valley']))
					$valley_calc = 0;
				else
					$valley_calc = $pairs[$key]['valley'];
				if (!isset($pairs[$key]['peak']))
					$peak_calc = 0;
				else
					$peak_calc = $pairs[$key]['peak'];
				$pairs[$key]['all'] = $peak_calc * $result['peak_charge'] + $flat_calc * $result['flat_charge'] +
				$valley_calc * $result['valley_charge'];
			}
				
			foreach ($pairs as $key => $val){
				if(!isset($pairs[$key]['peak']))
					$pairs[$key]['peak'] = 0;
				if(!isset($pairs[$key]['flat']))
					$pairs[$key]['flat'] = 0;
				if(!isset($pairs[$key]['valley']))
					$pairs[$key]['valley'] = 0;
				if(!isset($pairs[$key]['all']))
					$pairs[$key]['all'] = 0;
			}
			
			$result['data'] = $pairs;
			$result['year'] = $year;
			$result['month'] = $month;
			$this->load->view('/charge/charge_month.php',array(
					'user' => $this->user,
					'menu_map' => $this->menu_map,
					'charge' => $result
			));
		}
		else if($report_type == "day"){
			$from = $_POST['from'];
			$to = $_POST['to'];
			$peak = $this->charge->get_arr_time_range($info->related_point,$peak_arr,$from,$to);
			$flat = $this->charge->get_arr_time_range($info->related_point,$flat_arr,$from,$to);
			$valley = $this->charge->get_arr_time_range($info->related_point,$valley_arr,$from,$to);
			$all = $this->charge->get_arr_time_range($info->related_point,$all_arr,$from,$to);
			
			$pairs = array();
			foreach ($peak as $key => $val){
				$pairs[$key]['peak'] = $val;
			}
			foreach ($flat as $key => $val){
				$pairs[$key]['flat'] = $val;
			}
			foreach ($valley as $key => $val){
				$pairs[$key]['valley'] = $val;
			}
			foreach ($all as $key => $val){
				if (!isset($pairs[$key]['flat']))
					$flat_calc = 0;
				else 
					$flat_calc = $pairs[$key]['flat'];
				if (!isset($pairs[$key]['valley']))
					$valley_calc = 0;
				else
					$valley_calc = $pairs[$key]['valley'];
				if (!isset($pairs[$key]['peak']))
					$peak_calc = 0;
				else
					$peak_calc = $pairs[$key]['peak'];
				$pairs[$key]['all'] = $peak_calc * $result['peak_charge'] + $flat_calc * $result['flat_charge'] +
										$valley_calc * $result['valley_charge'];
			}
			
			foreach ($pairs as $key => $val){
				if(!isset($pairs[$key]['peak']))
					$pairs[$key]['peak'] = 0;
				if(!isset($pairs[$key]['flat']))
					$pairs[$key]['flat'] = 0;
				if(!isset($pairs[$key]['valley']))
					$pairs[$key]['valley'] = 0;
				if(!isset($pairs[$key]['all']))
					$pairs[$key]['all'] = 0;
			}
			
			$result['data'] = $pairs;
			$result['from'] = $from;
			$result['to'] = $to;
			$this->load->view('/charge/charge_day.php',array(
					'user' => $this->user,
					'menu_map' => $this->menu_map,
					'charge' => $result
			));
		}

	}
}
/* End of file analysis.php */
/* Location: controller/analysis.php */
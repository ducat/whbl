<?php

class User extends CI_Controller {
	
	public function login() {
		if(strtolower($_SERVER['REQUEST_METHOD']) != "post" ||
				!$_POST['username'] ||
				!$_POST['password']) {
			show_login_err(lang('login_err_title'), lang('login_err_message'));
		}
		
		$username = $_POST['username'];
		$password = $_POST['password'];
		$remember = isset($_POST['remember']) ? true : false;
		$result = $this->ion_auth->login($username, $password, $remember);
		
		$current_lang = current_lang();
		
		redirect(site_url('/' . $current_lang));
	}
	
	public function logout() {
		$this->ion_auth->logout();
		
		$current_lang = current_lang();
		
		redirect(site_url('/' . $current_lang));
	}
}
/* End of file user.php */
/* Location: controller/user.php */

<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
//表达式编码规则，type=1：二元操作符；2：一元操作符；3：表计名；4：数字；5：其他
class Calc extends MY_Controller {
	private $operator=array("+","-","*","/","^","sin","cos","and","or","_big","_small","=","_sum","_root","not","!=");
	private $binoper=array("+","-","*","/","^","and","or","_big","_small","=","!=");
	function __construct()
	{
		parent::__construct();
		
		//$is_expired = strtotime(date("Y-m-d")) > strtotime("2013-12-25");
		//$this->show_expired($is_expired);
		
		//$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC1049", MONO_EDITION);
		//$this->show_errkey($status);
		
		
		$this->load->model("point_model", "point");
		$this->load->model("base_model", "base");
	}
	
	private function isOperator($str){
		if(in_array($str,$this->operator)==true)
			return true;
		return false;
	}
	private function isBinOper($str){
		if(in_array($str,$this->binoper)==true)
			return true;
		return false;
	}
	private function isMeter($str){
		$char1=substr( $str, 0, 1);
		if($char1=='_')
			return true;
		return false;
	}
	private function isDigit($str){
		if(strspn($str,"1234567890.-")==strlen($str))
			return true;
		return false;
	}
	private function jEncode($str){
		$jcode=null;
		if($this->isBinOper($str)==true){//当为二元操作符时，注意减号的问题
			$arr=array("type"=>1,"value"=>$str);
			$jcode=json_encode($arr);
		}
		else if($this->isOperator($str)){//当为一元操作符时
			$arr=array("type"=>2,"value"=>$str);
			$jcode=json_encode($arr);
		}
		else if($this->isMeter($str)){//党委表计时
			$arr=array("type"=>3,"value"=>$str);
			$jcode=json_encode($arr);
		}
		else if($this->isDigit($str)){//党委数字时
			$arr=array("type"=>4,"value"=>$str);
			$jcode=json_encode($arr);
		}
		else{
			$arr=array("type"=>5,"value"=>$str);
			$jcode=json_encode($arr);
		}
		return $jcode;
	}
//  	public function __construct($id,$type){//暂时注释
//  		parent::__construct();
//  		if(strtolower($_SERVER['REQUEST_METHOD']) != "post" ||
//  				!$_POST['id']||
// 				!$_POST['type']) {
//  			show_404();
//  		}
//  		$this->id = $id;// $_POST['id'];//点id
//  		$this->type=$type; //$_POST['type'];
//  	}
	public function show_calc($id) {
		//获取点id
		
		//$this->type=$type; //$_POST['type'];
	
		//$items =get_point_and_name();
		$start = date("Y-m-d");		
		$end = date("Y-m-d");	
		$points = array("");	
		$bases = array("");
		
		if(isset($_POST['start_time']))
			$start = $_POST['start_time'];
		if(isset($_POST['end_time']))
			$end = $_POST['end_time'];
		if(isset($_POST['point']))
			$points = $_POST['point'];
		if(isset($_POST['base']))
			$bases = $_POST['base'];
		$dot = $this->point->get_point_and_name_by_array($points);
		$items = $this->base->get_base_by_array($bases);
		$info = $this->point->get_point_info($id);
		//echo(json_encode($dot));
		$dotarray=array();
		foreach($dot as $row){
			$dotarray['_'.$row->id.'_'] = $row->chinese_name;
			//array_push($dotarray,array($row->id => $row->display_name));
		}

		//$dot->id
		//$dot=array('A' => '电流', 'B' => '电压', 'C' => '电阻', 'D' => '电功率');
		$this->load->view("calc/view",array('dot' => $dotarray,
											'base' => $items,
											'info' => $info,
											'start' => $start,
											'end' => $end,
											'user' => $this->user,
											'menu_map' => $this->menu_map, 
											'id' => $id));
				//'menu_map' => $this->menu_map));//,
				//array('user' => $user));
	}
	
	public function index($id = 0){
		$points = $this->point->get_all_non_virtual_points();
		$bases = $this->base->get_bases();
		$info = $this->point->get_point_info($id);
		$this->load->view("calc/select",array(	'user' => $this->user,
												'menu_map' => $this->menu_map,
												'info' => $info,
												'points' => $points,
												'bases' => $bases));

	}
	public function get_non_virtual_points(){
		
		$result = array();			
		$items = $this->point->get_all_non_virtual_points();
		foreach ($items as $row){
			array_push($result, array(  "display_name" => $row->chinese_name));
		}
		echo json_encode($result);
	}
	
	public function submit(){

		if(strtolower($_SERVER['REQUEST_METHOD']) != "post" ||
				!$_POST['exp']||
				!$_POST['exp_display']||
				!$_POST['start']||
				!$_POST['end']||
				//!$_POST['type']||
				!$_POST['id']) {
			return;
			
		}
		//$this->type=$_POST['type'];
		$id=$_POST['id'];
		$expstr = $_POST['exp'];
		$expstr_display = $_POST['exp_display'];
		$start = $_POST['start'];
		$end = $_POST['end'];

		if(judge_grammar($expstr)){
			$str_replaced = str_replace("^","^^",$expstr);
			$a = exec(".\\test.exe ".$str_replaced,$out,$status);
			if($status!=0){
				echo("error");
				return false;
			}
			else if($out[0]=="error"){
				echo("error2");
				return false;
			}
		}
		else {
			echo("error");
			return false;
		}

			
		//print_r($out[0]);

		$explist=explode('|',$out[0]);
		$expjcode=array();
		

		for($i=0;$i<count($explist);$i++){
			$jcode=$this->jEncode($explist[$i]);
			$expjcode[$i]=$jcode;
		}
		
		$expj=json_encode($expjcode);


		
		$this->load->model("point_model");
		//insert_data(9);
// 		if($this->type==0){
 			//echo "sadfsfa"  . $expj;
			$this->point_model->update_expression($id,$expj,$expstr_display);
// 			$valuearray=point_calc_all_json($expj);
// 			while (list($key, $val) = each($valuearray))
// 			{
// 				$this->point_model->insert_point_data($this->id,$key,$val);
// 			}			
// 		}
//		else if($this->type==1)
//			$this->point_model->update_alarm_expression($this->id,$expj,$expstr_display);
//		else{
 			//error;
// 		}	



 		$this->load->model('point_data_model');

 		$data = array();
 		$this->point_data_model->delete_data_by_point_id($id);
 		
 		$exp_arr = decode_json($expj);
 		
 		$exp_points_max = array();
 		$exp_points_min = array();
 		$exp_points_count = array();
 		$base_point = 0;
 		$min_batch = -1;
 		$max_batch = -1;
 		foreach ($exp_arr as $arr){
 			$val = trim($arr['value'], "_"); 			
 			if( $arr['type'] == 3){
 				$base_point = $val;
 				$min_point_batch = $this->point_data_model->get_min_batch_after_start($val, $start); 
 				$max_point_batch = $this->point_data_model->get_max_batch_before_end($val, $end);
 				$count_point_batch = $this->point_data_model->get_count_batch_start_end($val, $min_point_batch, $max_point_batch);
 				array_push($exp_points_max, $max_point_batch);
 				array_push($exp_points_min, $min_point_batch);
 				array_push($exp_points_count, $count_point_batch);
 			} 			
 		}
 		
 		if(judge_exp_batch($exp_points_min, $exp_points_max, $exp_points_count)){
	 		$min_batch = $this->point_data_model->get_min_batch_after_start($base_point, $start) ;
	 		$max_batch = $this->point_data_model->get_max_batch_before_end($base_point, $end) ;

	 		for($i=$min_batch;$i<=$max_batch;$i++){
	 		
	 			$cal= point_calc($exp_arr, $i);	 	
	 			$time=$this->point_data_model->get_time_by_batch($i);
	 		
	 			$pair = array('timestamp' => $time , 'value' => $cal, 'batch' =>$i, 'point_id' => $id);
	 			$this->point_data_model->insert_virtual($pair);
	 		}
	 		echo "success";
 		}
 		else 
 		{
 			echo "error_batch";
 			return;
 		}
 		
 		
		//echo($expj);
		//解json		
		
		//$data=point_calc($expdecoded,1);
		//$data=point_calc_batch($expj,8);
		//$alarm=check_alarm(1);
// 		while (list($key, $val) = each($alarm))
// 		{
// 			echo "$key => $val";
// 			echo "\n";
// 		}

// 		foreach($max as $row){
// 			echo($row->max(batch));
// 			echo("\n");
// 			//array_push($dotarray,array($row->id => $row->display_name));
// 		}
// 		$i=1;
// 		foreach($data as $dt){
// 			echo($dt);
// 			echo("\n");
// 		}
		//svar_dump("data:".$data);

		//解json

		
	}


	
}
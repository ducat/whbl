<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Distribution extends MY_Controller {
	function __construct() {
		parent::__construct ();
	}
	public function index() {
		redirect ( 'distribution/distribution1' );
	}
	public function distribution1() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function distribution2() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function distribution3() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
}
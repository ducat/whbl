<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export_csv extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	public function index() {
		redirect('export_csv/export');
	}
	
	function export($type = 'budget', $year_base = 2010, $year_compare = 2011)
	{
		$this->autoRender = false;
		$csvname = $type.date("YmdHis").'.csv';   //文件命名

		if($type == 'budget'){
			$this->load->model('Budget_model','budget');
			$base = $this->budget->get_budget_by_year($year_base);
			$compare = $this->budget->get_budget_by_year($year_compare);
			
			if($base != null  && $compare != null){
				$data=lang('budget_month').",".$base->name.",".$compare->name.",".lang('delta')." \n";
				$base_data = array();
				$compare_data = array();
				array_push($base_data, $base->jan);
				array_push($base_data, $base->feb);
				array_push($base_data, $base->mar);
				array_push($base_data, $base->apr);
				array_push($base_data, $base->may);
				array_push($base_data, $base->jun);
				array_push($base_data, $base->jul);
				array_push($base_data, $base->aug);
				array_push($base_data, $base->sep);
				array_push($base_data, $base->oct);
				array_push($base_data, $base->nov);
				array_push($base_data, $base->dec);
				
				array_push($compare_data, $compare->jan);
				array_push($compare_data, $compare->feb);
				array_push($compare_data, $compare->mar);
				array_push($compare_data, $compare->apr);
				array_push($compare_data, $compare->may);
				array_push($compare_data, $compare->jun);
				array_push($compare_data, $compare->jul);
				array_push($compare_data, $compare->aug);
				array_push($compare_data, $compare->sep);
				array_push($compare_data, $compare->oct);
				array_push($compare_data, $compare->nov);
				array_push($compare_data, $compare->dec);
				
				for($i=0;$i<12;$i++) {
					$data .= ($i+1) .','. $base_data[$i].','.$compare_data[$i].','.($base_data[$i] - $compare_data[$i])."\n";
				}
			}
			else
				$data="No Data , , , \n";
				
		}
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=" . $csvname);
		header('Cache-Control:   must-revalidate,   post-check=0,   pre-check=0');
		header('Expires:   0');
		header('Pragma:   public');
		$data=  iconv("utf-8",'gbk', $data);
		echo $data;
	}
	


	
	
}
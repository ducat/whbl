<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Chiller_control extends MY_Controller {
	function __construct() {
		parent::__construct ();
	}
	public function index() {
		redirect ( 'chiller_control/chiller_control1' );
	}
	public function chiller_control1() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function chiller_control2() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function chiller_control3() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function chiller_control4() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function chiller_control5() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
}
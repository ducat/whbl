<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instrument extends MY_Controller {

	function __construct()
	{
		parent::__construct();

 		//$is_expired = strtotime(date("Y-m-d")) > strtotime("2013-12-25");
 		//$this->show_expired($is_expired);
		
		//$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC1049", MONO_EDITION);
		//$this->show_errkey($status);
		
		$this->load->model("station_model", "station");
		$this->load->model('Point_data_model','point_data');
		$this->load->model('Point_model','point');
		$this->load->model('Config_model','myconfig');
	}
	
	public function index() {
		redirect(site_url('instrument/cop'));
	}
	
	public function cop() {
		
		$station_info = $this->get_station_info();
		$sc_info = $this->get_sc_info();

		$initialized = $this->station->initialized();
		if($initialized == 0)
			$this->station->set_initialized();
		
		$cur_temp = $this->get_out_temperature();
		$cur_humi = $this->get_out_humidity();

		$today = date("Y-m-d");
		
		$cop_data = $this->get_cop_data($today);
		$this->load->view('instrument/cop', array(
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'station_info' => $station_info,
				'sc_info' => $sc_info,
				'cur_temp' => $cur_temp,
				'cur_humi' => $cur_humi,
				'cop_data' => $cop_data));
	}
	
	public function get_station_info(){
		return $this->station->get_station_info();	
	}

	public function get_sc_info () {
		return $this->station->get_sc_info ();
	}
	public function get_out_temperature(){
		
		$cur_temp = $this->point_data->get_out_temperature();
		return $cur_temp == null ? 27 : round($cur_temp, 2);
	}
	
	public function get_out_humidity(){	
		
		$cur_humi = $this->point_data->get_out_humidity();
		return $cur_humi == null ? 30 : round($cur_humi, 2);
	}
	
		
	public function get_cop_data($date) { //最近20个数据
		if((!$date)) {
			show_404();
			return;
		}
		
		$current = $this->point->get_chiller_current();	//current point_id	
		$st = $this->point->get_chiller_current_st(); //current st point_id
		$flw = $this->point->get_chiller_flw();  //flw point_id
		$t1 = $this->point->get_chiller_swt();   //swt point_id
		$t2 = $this->point->get_chiller_rwt();   //rwt point_id
		$min = $this->point_data->get_min_batch_by_date($flw, $date);
		$max = $this->point_data->get_max_batch_by_date($flw, $date);
		
		if ($max - $min >20){
			$start = $max - 20;
			$end = $max;
		}
		else {
			$start = $min;
			$end = $max;
		}
		if($flw == null || $t1 == null || $t2 == null || $start == null || $end == null || $start = -1 || $end = -1 ){
			$result = array( 'W' => array() , 'Q' => array(), 'COP' => array(), 'sumW' => array(), 'sumQ' => array() );
			return $result;
		}
		$current_arr = array();
		$st_arr = array();
		$W = array();
		$Q = array();
		$COP = array();
		for($cur_i = 0;$cur_i < count($current);$cur_i++){
			$current_arr_ch = $this->point_data->get_data_by_point_id($current[$cur_i]->id,$start,$end);
			$current_arr[$cur_i] = $current_arr_ch;
		}
		for ($st_i =0; $st_i < count($st); $st_i++){
			$st_arr_ch = $this->point_data->get_data_by_point_id($st[$st_i]->id,$start,$end);
			$st_arr[$st_i] = $st_arr_ch;
		}

		$flw_arr = $this->point_data->get_data_by_point_id($flw,$start,$end);
		$t1_arr = $this->point_data->get_data_by_point_id($t1,$start,$end);
		$t2_arr = $this->point_data->get_data_by_point_id($t2,$start,$end);
				
		//计算
	
		for($i = 0; $i < $end - $start; $i++){
			$time = strtotime($flw_arr[$i]->timestamp) . '000';
			$a = 0;
			for ($j = 0; $j < count($current_arr);$j++){
				$a += 649.933 * ( $current_arr[$j][$i]->value) * ($st_arr[$j][$i]->value) / 1000;  //KW
				//W=1.732*380(V)*冷机电流(A)*85%
			}
			array_push($W, array($time, round($a, 2)));
			$b = 1.163 * $flw_arr[$i]->value * ($t1_arr[$i]->value - $t2_arr[$i]->value);  //KW
			//Q=Cp*r*Vs*△T=4.1868*1000* Vs *(T供水 - T回水) / 3600= 1.163* Vs * (T供水- T回水)
			$b = abs($b);
			array_push($Q, array($time, round($b, 2)));
			if ($a == 0){
				array_push($COP, array($time, 0));
			}
			else{
				$c = $b / $a;
				if($c > 6.5){
					$c = 6.5;
				}
				array_push($COP, array($time, round($c, 2)));
			}
		}
		$result = array('W' => $W, 'Q' => $Q, 'COP' => $COP);
		return $result;
	}
	
	public function get_sum_data($date) {
		if((!$date)) {
			$result = array('sumW' => 0, 'sumQ' => 0);
			echo json_encode($result);
		}
	
		$query_per_hour = $this->point_data->get_querys_per_hour();
		$current = $this->point->get_chiller_current();	//current point_id
		$st = $this->point->get_chiller_current_st(); //current st point_id
		$flw = $this->point->get_chiller_flw();  //flw point_id
		$t1 = $this->point->get_chiller_swt();   //swt point_id
		$t2 = $this->point->get_chiller_rwt();   //rwt point_id
		$start = $this->point_data->get_min_batch_by_date($flw, $date);
		$end = $this->point_data->get_max_batch_by_date($flw, $date);
		
		if($flw == null || $t1 == null || $t2 == null || $start == null || $end == null || $start = -1 || $end = -1){
			$result = array('sumW' => 0, 'sumQ' => 0);
			echo json_encode($result);
			return;
		}
		$current_arr = array();
		$st_arr = array();
		$W = array();
		$Q = array();
		$COP = array();
		for($cur_i = 0;$cur_i < count($current);$cur_i++){
			$current_arr_ch = $this->point_data->get_data_by_point_id($current[$cur_i]->id,$start,$end);
			$current_arr[$cur_i] = $current_arr_ch;
		}
		for ($st_i =0; $st_i < count($st); $st_i++){
			$st_arr_ch = $this->point_data->get_data_by_point_id($st[$st_i]->id,$start,$end);
			$st_arr[$st_i] = $st_arr_ch;
		}
	
		$flw_arr = $this->point_data->get_data_by_point_id($flw,$start,$end);
		$t1_arr = $this->point_data->get_data_by_point_id($t1,$start,$end);
		$t2_arr = $this->point_data->get_data_by_point_id($t2,$start,$end);
	
		//计算
		$sumW = 0;
		$sumQ = 0;	
		for($i = 0; $i < $end - $start; $i++){
			$a = 0;
			for ($j = 0; $j < count($current_arr);$j++){
				$a += 649.933 * ( $current_arr[$j][$i]->value) * ($st_arr[$j][$i]->value) / 1000;  //KW
				//W=1.732*380(V)*冷机电流(A)*85%
			}
			$sumW += $a;
			$b = 1.163 * $flw_arr[$i]->value * ($t1_arr[$i]->value - $t2_arr[$i]->value);  //KW
			//Q=Cp*r*Vs*△T=4.1868*1000* Vs *(T供水 - T回水) / 3600= 1.163* Vs * (T供水- T回水)
			$b = abs($b);
			$sumQ += $b;
		}
		$result = array('sumW' => round($sumW, 2), 'sumQ' => round($sumQ, 2));
		echo json_encode($result);
	}
	
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('instrument_control') . "__" . lang('instrument'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'control',
				'output' => $output
		));
	}
	
	public function control(){
		try{
			$crud = new grocery_CRUD();
			if (current_lang() == 'en')
				$crud->set_language("english");				
			$crud->set_table('setting_queues');
			$crud->set_subject(lang('instrument_control'));
			$crud->columns('timestamp','point_id','priority','value','status');
			$crud->fields('point_id','priority','value');
			$crud->required_fields('point_id','value','priority');
			$crud->set_relation('point_id','point','{chinese_name}({type})',array('type !='=>'Analog Input', 'type !='=>'Binary Input', 'deleted' => 0));				
			$crud->display_as('point_id',lang('control_point'));
			$crud->display_as('timestamp',lang('control_timestamp'));
			$crud->display_as('value',lang('control_setvalue'));
			$crud->display_as('status',lang('control_status'));		
			$crud->display_as('priority',lang('control_priority'));
			$crud->order_by('timestamp','desc');
			
			$crud->field_type('priority', 'enum', array(3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15));
			$crud->field_type('status', 'dropdown', array('0' => lang('control_st_notsent'),
														  '1' => lang('control_st_success'),
														  '-1' => lang('control_st_failed')));
			$crud->set_rules('value', lang('control_setvalue'), 'numeric');
			
			$crud->callback_before_insert(array($this, 'judge_value_after_insert'));
			
			$crud->unset_print();
			//$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$output = $crud->render();	
			$this->_example_output($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function judge_value_after_insert($post_array,$primary_key)
	{
		$type = $this->point->get_type($post_array['point_id']);
		if($type == "Binary Output"){
			if($post_array['value'] != 0)
				$post_array['value'] = 1;
		}

		return $post_array;
	}
	public function send_cmd($point_id = 0, $cmd_type = 0, $priority = 16, $value = 0){
		$this->load->model('cmd_model', 'cmd');
		$result = null;
		if ($cmd_type == 0){//control
			$this->cmd->insert_cmd($point_id, $cmd_type, $priority, $value);
			sleep(1);
			$info = $this->cmd->get_info($point_id);
			if($info != null)
				$status = $info->status;
			else 
				$status = 0;
			switch ($status){
				case 0:
					$result = lang('control_st_notsent');
					break;
				case -1:
					$result = lang('control_st_failed');
					break;
				case 1:
					$result = lang('control_st_success');
					break;
				default:
					$result = lang('control_st_failed');
			}
		}
		else if ($cmd_type == 1){//get level list
			$this->cmd->insert_cmd($point_id, $cmd_type, 16, 0);
			sleep(1);
			$info = $this->cmd->get_info($point_id);
			if($info != null)
				$comment = $info->comment;
			else
				$comment = null;
			if($comment == null)
				$result = json_encode(lang("control_failed_getlist"));
			else {
				$token = strtok($comment, ";");
				$json = array();
				while ($token !== false)
				{
					array_push($json, $token);
					$token = strtok(";");
				}
				$result = json_encode($json);
			}
	
			
		}
		else if ($cmd_type == 2){ //delete level
			$this->cmd->insert_cmd($point_id, $cmd_type, $priority, 0);
			sleep(1);
			$info = $this->cmd->get_info($point_id);
			if($info != null)
				$comment = $info->comment;
			else
				$comment = null;
			if($comment == null)
				$result = json_encode(lang("control_failed_getlist"));
			else {
				$token = strtok($comment, ";");
				$json = array();
				while ($token !== false)
				{
					array_push($json, $token);
					$token = strtok(";");
				}
				$result = json_encode($json);
			}
			
		}
		
		echo $result;
		return;
	}
	
	
	
	public function view($id) {
		if(!$id) {
			show_404();
			return;
		}
		$inst = $this->inst->get_by_id($id);
		if(!$inst) {
			show_404();
			return;
		}
	

		$next_inst = $this->inst->get_next_inst($id);
		$next_url = site_url('instrument/view/' . ($next_inst == null? $id:$next_inst));
		$refresh_interval = $this->myconfig->get_refresh_interval();
		$inst_img_groups = $this->inst->get_img_group_by_inst_id($id);
		$inst_images = $this->inst->get_image_by_inst_id($id);
		$inst_points = $this->inst->get_point_by_instrument_id($id);
		foreach ($inst_points as $point){
			switch ($point->type){
				case "Analog Input":
					$point->type = "ai";
					break;
				case "Analog Output":
					$point->type = "ao";
					break;
				case "Binary Input":
					$point->type = "bi";
					break;
				case "Binary Output":
					$point->type = "bo";
					break;
				default:
					$point->type = "other";
					break;
			}
		}
		$this->load->view('instrument/dynamic_view', array(
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'instrument' => $inst,
				'inst_images' => $inst_images,
				'inst_points' => $inst_points,
				'inst_img_groups' => $inst_img_groups,
				'next_url' => $next_url,
				'refresh_interval' => $refresh_interval,
				'edit' => false));
	}
	
	public function get_points_data() {
		if(strtolower($_SERVER['REQUEST_METHOD']) != 'post' || empty($_POST['instrument_id'])) {
			return null;
		}
	
		$id = $_POST['instrument_id'];
		$virtual_points = $this->point->get_virtual_point_by_instrument_id($id);
		$latest_batch = $this->point->get_latest_batch()->batch;

		$virtual_points_data = array();
		foreach ($virtual_points as $row){
			$virtual_point = array();
			$virtual_point['id'] = $row->id;
			$virtual_point['relation_id'] = $row->ralation_id;
			$virtual_point['value'] =  point_calc(decode_json($row->expression),$latest_batch);
			array_push($virtual_points_data, (object)$virtual_point) ;
		}
		$non_virtual_points_data = $this->point->get_non_virtual_point_latest_data_by_instrument_id($id);
		$points_data_for_dynamic = $this->point->get_dynamic_point_data_by_instrument_id($id);
		echo json_encode(array_merge($non_virtual_points_data, $virtual_points_data, $points_data_for_dynamic));
	}
}
/* End of file instrument.php */
/* Location: controller/instrument.php */

<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Air_condition extends MY_Controller {
	function __construct() {
		parent::__construct ();
	}
	public function index() {
		redirect ( 'building_control/chiller/air_condition/air_condition1' );
	}
	public function air_condition1() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function air_condition2() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function air_condition3() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
}
<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Building_control extends MY_Controller {
	function __construct() {
		parent::__construct ();
	}
	public function index() {
		$link = array (
				'空调机组1' => site_url('air_condition/air_condition1'),
				'空调机组2' => site_url('air_condition/air_condition2'),
				'空调机组3' => site_url('air_condition/air_condition3'),
				'新风机组1' => site_url('new_wind/new_wind1'),
				'新风机组2' => site_url('new_wind/new_wind2'),
				'新风机组3' => site_url('new_wind/new_wind3'),
				'新风机组4' => site_url('new_wind/new_wind4'),
				'冷机群控1' => site_url('chiller_control/chiller_control1'),
				'冷机群控2.1' => site_url('chiller_control/chiller_control2'),
				'冷机群控2.2' => site_url('chiller_control/chiller_control3'),
				'冷机群控2.3' => site_url('chiller_control/chiller_control4'),
				'冷机群控3' => site_url('chiller_control/chiller_control5'),
				'配电系统1' => site_url('distribution/distribution1'),
				'配电系统2' => site_url('distribution/distribution2'),
				'配电系统3' => site_url('distribution/distribution3'),
				'锅炉系统' => site_url('boiler/boiler1') 
		);
		$this->load->view ( 'building/index', array (
				'user' => $this->user,
				'link' => $link,
				'active' => __FUNCTION__,
				'menu_map' => $this->menu_map 
		) );
	}
	public function chiller($id) {
		$field = array (
				'air_condition1' => array (
						'name',
						'run_state',
						'value',
                        'command'
				),
				'air_condition2' => array (
						'name',
						'run_state',
						'value',
                        'command'
				),
				'air_condition3' => array (
						'name',
						'run_state',
						'value',
                        'command'
				),
				'chiller_control1' => array (
						'name',
						'run_state',
						'alarm',
						'auto_manual'
				),
				'chiller_control2' => array (
						'name',
						'value'
				),
				'chiller_control3' => array (
						'name',
						'run_state',
						'alarm',
						'auto_manual',
						'frequency',
				),
				'chiller_control4' => array (
						'name',
						'run_state'
				),
				'chiller_control5' => array (
						'name',
						'on_state',
						'off_state',
				),
				'distribution1' => array (
						'name',
						'value'
				),
				'distribution2' => array (
						'name',
						'value'
				),
				'distribution3' => array (
						'name',
						'value'
				),
				'new_wind1' => array (
						'name',
						'run_state',
						'value',
                        'command'
				),
				'new_wind2' => array (
						'name',
						'run_state',
						'value',
                        'command'
				),
				'new_wind3' => array (
						'name',
						'run_state',
						'value',
                        'command'
				),
				'new_wind4' => array (
						'name',
						'run_state',
						'value',
                        'command'
				),
				'boiler1' => array (
						'name',
						'run_state',
						'alarm',
						'high_alarm'
				)
		);
		$translate = array (
				'run_state' => array (
						'0' => '关',
						'1' => '开' 
				),
				'on_state' => array (
						'0' => '否',
						'1' => '是' 
				),
				'off_state' => array (
						'0' => '否',
						'1' => '是' 
				),
				'alarm' => array (
						'0' => '无',
						'1' => '有' 
				),
				'high_alarm' => array (
						'0' => '正常',
						'1' => '高' 
				),
				'auto_manual' => array (
						'0' => '手动',
						'1' => '自动' 
				),
                'command' => array(
                    '1' => '开',
                    '0' => '关',
                    '2' => 'hold'
                )
		);
		$select_field = implode ( ',', $field [$id] ).',unit';
		$data = $this->db->query ( 'select ' . $select_field . ' from building_control where type = "' . $id . '"' )->result_array ();
		$point = $this->db->query ( 'select display_name,last_value from point' )->result_array ();
		$tmp = array ();
		foreach ( $point as $v ) {
			$tmp += array (
					$v ['display_name'] => $v ['last_value'] 
			);
		}
		$replace_field = $field [$id];
		unset ( $replace_field [0] );
		foreach ( $data as $k => $v ) {
			foreach ( $replace_field as $f ) {
				if($f != 'unit'){
					$value = isset ( $tmp [$v [$f]] ) ? $tmp [$v [$f]] : '';
					$value = isset ( $translate [$f] [$value] ) ? $translate [$f] [$value] : $value;
					$data [$k] [$f] = $value;
				}
			}
		}
		$this->load->view ( 'building/chiller', array (
				'user' => $this->user,
				'data' => $data,
				'field' => $field [$id],
				'active' => $id,
				'menu_map' => $this->menu_map 
		) );
	}
}
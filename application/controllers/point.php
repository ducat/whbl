<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Point extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();

		//$is_expired = strtotime(date("Y-m-d")) > strtotime("2013-12-25");
		//$this->show_expired($is_expired);
		
		//$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC1049", MONO_EDITION);
		//$this->show_errkey($status);
		
		
		$this->load->model("point_model", "point");
		$this->load->model("translate_model", "translate");
	}
	public function index() {
		redirect(site_url('point/translate'));
	}
	
	public function translate($type = "all") {
		if ($type == "all"){
			$points = $this->point->get_all_bacnet_points();
		}
		else{
			$points = $this->point->get_bacnet_points_need_trans();
		}		
		foreach ($points as $row){
			$info = ($row == null? null:$this->translate->get_point_info_by_name($row->expression));
			$arr = $this->judge($info, $row);
			$this->point->update_point_info($arr, $row->id);
		}

		redirect("manage/point_bacnet");	
	}
	
	function judge($info, $row){
		$regex_RA_T = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_RA_T$#";
		$regex_OAV_PCT_ST = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_OAV_PCT_ST$#";
		$regex_RAV_PCT_ST = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_RAV_PCT_ST$#";
		$regex_CWV_PCT_ST = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_CWV_PCT_ST$#";
		$regex_OA_T = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_OA_T$#";
		$regex_OA_H = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_OA_H$#";
		$regex_OA_E = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_OA_E$#";
		$regex_SA_T = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_SA_T$#";
		$regex_ZN_T = "#^(V|VB|VAV)\d{1,4}-\d{1,4}_ZN_T$#";
		$regex_RM_IT_T = "#^(RM_IT)\d{1,4}_IT_T$#";
		$regex_RM_UPS_T = "#^(RM_UPS)\d{1,4}_T$#";
		$regex_CO2_PPM = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_CO2_PPM$#";
		$regex_FAN_FREQ_HZST = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_FAN_FREQ_HZST$#";
		$regex_FAN_AM = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_FAN_A/M$#";
		$regex_BOP_FAN_AM = "#^BOP-(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_FAN_A/M$#";
		$regex_SFAN_ST = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_SFAN_ST$#";
		$regex_FAN_AL = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_FAN_AL$#";
		$regex_FAN_FREQ_HZSPT = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_FAN_FREQ_HZSPT$#";
		$regex_DP_AL = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_DP\d{1,4}_AL$#";
		$regex_FRPF_AL = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_FRPF_AL$#";
		$regex_AM = "#^(EFAN|PAU|PY|PAU-RF)\d{1,4}_A/M$#";
		$regex_ST = "#^(EFAN|PAU|PY|PAU-RF)\d{1,4}_ST$#";
		$regex_AL = "#^(EFAN|PAU|PY|PAU-RF)\d{1,4}_AL$#";
		$regex_SFAN_SS = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_SFAN_SS$#";
		$regex_OAV_PCT_SPT = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_OAV_PCT_SPT$#";
		$regex_RAV_PCT_SPT = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_RAV_PCT_SPT$#";
		$regex_CWV_PCT_SPT = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_CWV_PCT_SPT$#";
		$regex_CO2_PPM_ALSPT = "#^(K|ZK|DK|AHU|H)\d{1,4}-\d{1,4}_CO2_PPM_ALSPT$#";
		
		
		if($info == null){
			$matches = array();
			$str = $row->expression;
			if(preg_match($regex_RA_T, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU回风温度或区域温度",
						"english_name" => "AHU Return Air（or zone) Temperature",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "Area_Temperature",
						"unit" => 62,
						"alarm_level" => 4,
						"upload" => 0);
			}
			elseif (preg_match($regex_OAV_PCT_ST, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU新风阀开度反馈状态(AIP)",
						"english_name" => "AHU Out Air Valve Open Percent State",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "Regulated_value",
						//"unit" => "%",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_RAV_PCT_ST, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU回风阀开度反馈状态(AIP)",
						"english_name" => "AHU Return Air Valve Open Percent State",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "Regulated_value",
						//"unit" => "%",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_CWV_PCT_ST, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU冷水阀开度反馈状态(AIP)",
						"english_name" => "AHU Chilled Water Valve Open Percent State",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "Regulated_value",
						//"unit" => "%",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_OA_T, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "新风温度",
						"english_name" => "Outdoor Air Temperature",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "Temperature",
						"unit" => "62",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_OA_H, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "新风湿度",
						"english_name" => "Outdoor Air Humidity",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "Humidity",
						//"unit" => "%",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_OA_E, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "新风焓值",
						"english_name" => "Outdoor Air Enthalpy",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "I",
						//"unit" => "kj/kg",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_SA_T, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "VAV系统AHU送风温度",
						"english_name" => "AHU Supply Air Temperature (VAV sys.)",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "VAV_Temperature",
						"unit" => 62,
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_ZN_T, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "VAV区域房间温度",
						"english_name" => "VAV Zone Temperature",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "VAV_A_Temperature",
						"unit" => 62,
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_RM_IT_T, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "IT房间温度",
						"english_name" => "IT Room Temperature",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "IT_A_Temperature",
						"unit" => 62,
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_RM_UPS_T, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "UPS房间温度",
						"english_name" => "UPS Room Temperature",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "UPS_A_Temperature",
						"unit" => 62,
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_CO2_PPM, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "区域CO2浓度",
						"english_name" => "Zone CO2ppm",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "CO2",
						//"unit" => "ppm",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_FAN_FREQ_HZST, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU风机运行频率反馈状态(AIP)",
						"english_name" => "AHU Fan Run Frequence HZ-State",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Input",
						"type_large" => "transient",
						"type_small" => "Frequence_HZ_State",
						//"unit" => "Hz",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_FAN_AM, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU风机手自动状态",
						"english_name" => "AHU Fan Manual/Auto Operater State",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Input",
						"type_large" => "binary",
						"type_small" => "Manual/Auto",
						//"unit" => "Manual/Auto",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_BOP_FAN_AM, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU风机-BOP点软故障状态(BOP)",
						"english_name" => "AHU Fan Programe BOP-Manual/Auto Alarm",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Input",
						"type_large" => "binary",
						"type_small" => "Fault/Normal",
						//"unit" => "Fault/Normal",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_SFAN_ST, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU送风机运行状态",
						"english_name" => "AHU Supply Fan Running State",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Input",
						"type_large" => "binary",
						"type_small" => "On/Off",
						//"unit" => "On/Off",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_FAN_AL, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU风机故障",
						"english_name" => "AHU Fan Alarm",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Input",
						"type_large" => "binary",
						"type_small" => "Fault/Normal",
						//"unit" => "Fault/Normal",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_FAN_FREQ_HZSPT, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU风机运行频率需求设定(AOP)",
						"english_name" => "AHU Fan Run Frequence Requested Setpoint",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Analog Output",
						"type_large" => "transient",
						"type_small" => "Frequence_Requested_SPT",
						//"unit" => "Hz",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_DP_AL, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU滤网报警",
						"english_name" => "AHU Filter Alarm",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Input",
						"type_large" => "binary",
						"type_small" => "Fault/Normal",
						//"unit" => "Fault/Normal",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_FRPF_AL, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU防冻开关报警",
						"english_name" => "AHU Prevent Freezeproof Switch Alarm",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Input",
						"type_large" => "binary",
						"type_small" => "Fault/Normal",
						//"unit" => "Fault/Normal",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_AM, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "排风机手自动状态",
						"english_name" => "Exhaust Fan Manual/Auto Operater State",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Input",
						"type_large" => "binary",
						"type_small" => "Manual/Auto",
						//"unit" => "Manual/Auto",
						"alarm_level" => 4, 
						"upload" => 0);
			
			}
			elseif (preg_match($regex_ST, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "排风机运行状态",
						"english_name" => "Exhaust Fan Running State",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Input",
						"type_large" => "binary",
						"type_small" => "On/Off",
						//"unit" => "On/Off",
						"alarm_level" => 4,
						"upload" => 0);
			}
			elseif (preg_match($regex_AL, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "排风机故障报警",
						"english_name" => "Exhaust Fan Alarm",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Input",
						"type_large" => "binary",
						"type_small" => "Fault/Normal",
						//"unit" => "Fault/Normal",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_SFAN_SS, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU送风机启停(BOP)",
						"english_name" => "AHU Supply Fan Start/Stop",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Output",
						"type_large" => "binary",
						"type_small" => "Start/Stop",
						//"unit" => "Start/Stop",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_OAV_PCT_SPT, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU新风阀开度设定(AOP)",
						"english_name" => "AHU Out Air Valve Open Percent Setpoint",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Output",
						"type_large" => "transient",
						"type_small" => "SPT",
						//"unit" => "%",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_RAV_PCT_SPT, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU回风阀开度设定(AOP)",
						"english_name" => "AHU Return Air Valve Open Percent Setpoint",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Output",
						"type_large" => "transient",
						"type_small" => "SPT",
						//"unit" => "%",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			elseif (preg_match($regex_CWV_PCT_SPT, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "AHU冷水阀开度设定(AOP)",
						"english_name" => "AHU Chilled Water Valve Open Percent Setpoint",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Output",
						"type_large" => "transient",
						"type_small" => "SPT",
						//"unit" => "%",
						"alarm_level" => 4,
						"upload" => 0);
			}
			elseif (preg_match($regex_CO2_PPM_ALSPT, $str, $matches)){
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => "区域CO2浓度报警设定（AOP)",
						"english_name" => "CO2 Alarm Setpoint",
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => "Binary Output",
						"type_large" => "transient",
						"type_small" => "SPT",
						//"unit" => "ppm",
						"alarm_level" => 4, 
						"upload" => 0);
			}
			else{
				$type_large = ($row->type == ('Binary Output' || 'Binary Input'))? "binary" : "transient";
				$arr = array(	"display_name" => $row->expression,
						"chinese_name" => $row->expression,
						"english_name" => $row->expression,
						"is_virtual_point" => 0,
						"is_bacnet" => 1,
						"type" => $row->type,
						"type_large" => "transient",
						"type_small" => "Undefined",
						"unit" => $row->unit,
						"alarm_level" => 4,
						"upload" => 0
				);
			}
			

		}
		else{
			$arr = array(	"display_name" => $info->display_name,
						"chinese_name" => $info->chinese_name,
						"english_name" => $info->english_name,
						"is_virtual_point" => $info->is_virtual_point,
						"is_bacnet" => $info->is_bacnet,
						"type" => $info->type,
						"type_large" => $info->type_large,
						"type_small" => $info->type_small,
						"serve_sys" => $info->serve_sys,
						"serve_obj" => $info->serve_obj,
						"unit" => $info->unit,
						"alarm_level" => $info->alarm_level,
						"upload" => $info->upload
					);				
		}
		return $arr;
	}
}
/* End of file point.php */
/* Location: controller/point.php */
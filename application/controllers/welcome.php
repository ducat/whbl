<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Welcome extends CI_Controller {
	public function index() {
		//$is_expired = strtotime(date("Y-m-d")) > strtotime("2014-11-1");
		//$this->show_expired($is_expired);
		
		//$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC10491000000000000000NETWORK", NETWORK_EDITION);
		//$this->show_errkey($status);
		
		if($this->ion_auth->logged_in()) {
			$user = $this->ion_auth->user()->row();
		} else {
			$user = false;
		}

		$this->load->view("welcome", array('user' => $user));

	}
	public function watchdog($pass1 = 0x0000, $pass2 = 0x0000, $HID_pre = 0x00000000, $USERID_pre = 0x00000000, $check = "", $edition = 0) {
		try {
			$Rockey = new COM("Com.CRockey4ND") ;
		} catch (Exception $e) {
			return NO_ROCKEY_COM;
		}
		$Rockey->p1 = new VARIANT($pass1, VT_UI2);
		$Rockey->p2 = new VARIANT($pass2, VT_UI2);
		$Rockey->p3 = new VARIANT(0x0000, VT_UI2);
		$Rockey->p4 = new VARIANT(0x0000, VT_UI2);
		$result = $Rockey->RockeyCM(1);
		if ($result==0) {
			// 			$HID = $Rockey->lp1;
			// 			if($HID != $HID_pre)
				// 				return ERROR_ROCKEY;
		} else {
			return NO_ROCKEY;
		}
		//Open First Rockey
		$result = $Rockey->RockeyCM(3);
		if ($result!=0) {
			return ERROR_ROCKEY;
		}
		//Read
		$Rockey->buffer = new VARIANT("", VT_BSTR);
		$Rockey->p1 = new VARIANT(0, VT_UI4);
		$Rockey->p2 = new VARIANT(500, VT_UI4);
		$result = $Rockey->RockeyCM(5);
		if ($result == 0) {
			$memory = $Rockey->buffer;
			if($memory != $check)
				return ERROR_ROCKEY;
		} else {
			return ERROR_ROCKEY;
		}
		//Read User ID
	
		$result = $Rockey->RockeyCM(10);
		if ($result==0) {
			$USERID = $Rockey->lp1;
			if($USERID != $USERID_pre)
				return ERROR_ROCKEY;
		} else {
			return ERROR_ROCKEY;
		}
	
	
		$Rockey->p1 = new VARIANT($edition, VT_UI2);
		$result = $Rockey->RockeyCM(12);
		if ($result==0) {
			$module_set = $Rockey->lp1;
			if($module_set != 1)
				return ERROR_ROCKEY;
		} else {
			return ERROR_ROCKEY;
		}
	
	
		$result = $Rockey->RockeyCM(4);
		if ($result!=0) {
			return ERROR_ROCKEY;
		}
		return SUCCESS_ROCKEY;
	}
	
	function show_expired($is_expired) {
		if($is_expired){
			show_my_error(lang("expired"), "");
			exit;
		}
	}
	
	function show_errkey($status) {
		if($status == NO_ROCKEY){
			show_my_error(lang("dog_nokey"), "");
			exit;
		}
		else if($status == ERROR_ROCKEY){
			show_my_error(lang("dog_errkey"), "");
			exit;
		}
		else if($status == NO_ROCKEY_COM){
			show_my_error(lang("dog_nocom"), "");
			exit;
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
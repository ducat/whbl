<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class New_wind extends MY_Controller {
	function __construct() {
		parent::__construct ();
	}
	public function index() {
		redirect ( 'new_wind/new_wind1' );
	}
	public function new_wind1() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function new_wind2() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function new_wind3() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
	public function new_wind4() {
		redirect ( 'building_control/chiller/'.__FUNCTION__ );
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class demo extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
				
 		//$is_expired = strtotime(date("Y-m-d")) > strtotime("2013-12-25");
 		//$this->show_expired($is_expired);		
		//$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC1049", MONO_EDITION);
		//$this->show_errkey($status);
	}
	
	public function index() {  //No Use
		redirect('demo/main_show');
	}
	
	public function main_show () {
		$this->load->view('demo/main_page', array(
				'title' => lang('demo'),
				'user' => $this->user
		));
	}
	
	/*public function map_show () {
		$this->load->view('demo/map_page', array (
				'title' => lang('data_display'),
				'user' => $this->user
		));
	}*/

	public function cop_show () {
		/*redirect ('camp/graphical_info/camp_overview/water');*/
		/*redirect('demo/main_show');*/
		redirect ('instrument/cop');
	}

	public function system_manage () {
		/*redirect ('manage/point_mbus');*/
		redirect('demo/main_show');
	}
	public function business_info () {
		/*redirect ('business/energy_monitor/hour_data/water');*/
		redirect('demo/main_show');
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alarm extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
				
 		//$is_expired = strtotime(date("Y-m-d")) > strtotime("2013-12-25");
 		//$this->show_expired($is_expired);		
		//$status = $this->watchdog(0x70ef, 0xbc4f, 0x00000000, 0x00000001, "LDMC1049", MONO_EDITION);
		//$this->show_errkey($status);
			
		$this->load->model("alarm_log_model","logs");
		$this->load->model("config_model","sys_config");
	}
	
	public function index() {  //No Use
		redirect('alarm/log');
	}
	
	public function log() {
		redirect("alarm/table");
	}
	
	public function _example_output($output = null)
	{
		$this->load->view('templates/manage_grocery_crud', array(
				'title' => lang('alarm_log') . "__" . lang('alarm'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'log',
				'output' => $output
		));
	}
	
	public function scatter() {
		$data = $this->get_scatter_data(); 
		$this->load->view('alarm/scatter', array(
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'data' => $data));
	}

	private function get_scatter_data(){
		$data = array();
		$items = $this->logs->get_log_100(); //get newest 100 to reduce No. of logs
		foreach ($items as $row){
			if ($row->point_id)
				array_push($data, array("x" =>strtotime($row->timestamp) . '000',
										"y"=>$row->point_id, 
										"display_name" => $row->display_name,
										"chinese_name" => $row->chinese_name,
										"english_name" => $row->english_name,
										"value" => $row->value));
			else 
				array_push($data, array(strtotime($row->timestamp) . '000', 0));
		}

		return $data;
	}
	
	public function table()
	{
		$cookie = array ("condition" => " ");
		$this->session->set_userdata ($cookie);

		$data = $this->logs->get_alarm_info ();

		$this->load->view('alarm/alarm_log', array(
				'title' => lang('alarm_log') . "__" . lang('alarm'),
				'user' => $this->user,
				'menu_map' => $this->menu_map,
				'active' => 'log',
				'data' => $data
		));
	}

	public function get_logs_data ()
	{
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;

		$result = array ();
		$offset = ($page - 1)*$rows;

		//Get condition
		$condition = $this->session->userdata("condition");
		$result = $this->logs->logs_list_page ($rows, $offset, $condition);

		echo json_encode ($result);
	}

	public function get_search_data () {
		$condition = $_POST ['condition'];
		$page_number = $_POST ["page_number"];
		$page_size = $_POST ["page_size"];
		$offset = ($page_number - 1) * $page_size;

		//Save condition
		$cookie = array ("condition" => $condition);
		$this->session->set_userdata ($cookie);

		$search_data = $this->logs->get_search_data ($page_size, $offset, $condition);

		echo json_encode ($search_data);
	}

	public function update_alarm_log(){
		if (current_lang() == 'en')
			$description = "description";
		else 
			$description = "chinese_description";

		$updated_rows=json_decode($_POST['updated_rows']);
		$data=array();
		foreach($updated_rows as $k=>$v){
			$data[]=array('id'=>$v->id,$description=>$v->description);
		}
		$this->db->update_batch('logs',$data,'id');
	}

	public function judge_alarm($minite = 10){
		$items = $this->logs->get_log_recent($minite); 
		if(count($items)>0)
			echo 'true';
		else
			echo 'false';
		return;
	}
	
	public function _callback_description($value, $row)
	{		
		$type = $token = strtok($row->description, "|");	
		//parse alarm type	$second is the 2nd parameter divided by |
		if($type == 'BOOT'){
			return lang("alarm_boot");
		}			
		elseif ($type == 'LEAVE'){
			return lang("alarm_leave");
		}		
		elseif ($type == 'POINT'){
			return lang("alarm_release");
		}
		elseif ($type == 'USBKEY'){
			$second = $token = strtok("|");
			if($second == "CAN_NOT_DETECT"){
				return lang("alarm_usbkey_notdetect");
			}
			else if($second == "VALIDATION_FAILED") {
				return lang("alarm_usbkey_invalid");
			}
			else
				return $row->description;
		}
			
		elseif ($type == 'MODBUS'){
			$second = $token = strtok("|");
			if($second == "INF_ERROR"){
				return lang("alarm_modbus_inferr");
			}
			elseif($second == "UNREACHABLE") {
				return lang("alarm_modbus_unreachable");
			}
			else 
				return $row->description;
		}			
		elseif ($type == 'BCU'){
			$second = $token = strtok("|");
			if($second == "READ_ERROR"){
				return lang("alarm_bcu_readerr");
			}
			else{
				$third = $token = strtok("|");
				if($third == "UNREACHABLE"){
					return lang("alarm_bcu_unreachable"). " ip:" . $second;
				}
				else 
					return $row->description;
			}
		}			
		elseif ($type == 'ALARM'){
			$second = $token = strtok("|");
			if($second == "HIGH"){
				return lang("alarm_alarmhigh");
			}
			elseif ($second == "LOW"){
				return lang("alarm_alarmlow");
			}
			elseif ($second == "OFFLINE"){
				return lang("alarm_alarmoffline");
			}
			else{
				return $row->description;
			}
		}			
		else
			return $row->description;
	}
}
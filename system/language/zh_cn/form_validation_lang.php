<?php

$lang['required']			= "%s字段是必需的.";
$lang['isset']				= "%s字段必须包含一个值.";
$lang['valid_email']		= "%s字段包含的值必须是一个合法的Email地址.";
$lang['valid_emails']		= "%s字段包含的Email地址必须全部合法.";
$lang['valid_phones']		= "%s字段包含的手机号码必须全部合法.";
$lang['valid_url']			= "%s字段包含的值必须是一个合法的URL.";
$lang['valid_ip']			= "%s字段包含的值必须是一个合法的IP地址.";
$lang['min_length']			= "%s字段包含的值至少 %s个字符.";
$lang['max_length']			= "%s字段包含的值长度不能超过 %s个字符.";
$lang['exact_length']		= "%s字段包含的值长度必须是 %s个字符.";
$lang['alpha']				= "%s字段不能包含除字母以外的其他字符.";
$lang['alpha_numeric']		= "%s字段不能包含除字母和数字以外的其他字符.";
$lang['alpha_dash']			= "%s字段不能包含除字母/数字/下划线/破折号以外的其他字符.";
$lang['numeric']			= "%s字段包含的值必须是一个数.";
$lang['is_numeric']			= "%s字段必须包含数字.";
$lang['integer']			= "%s字段包含的值必须是一个整数.";
$lang['regex_match']		= "%s字段格式有误.";
$lang['matches']			= "%s字段与%s字段不匹配.";
$lang['is_unique'] 			= "%s字段包含的值必须是唯一的.";
$lang['is_natural']			= "%s字段包含的值必须是一个自然数.";
$lang['is_natural_no_zero']	= "%s字段包含的值必须是一个正整数.";
$lang['decimal']			= "%s字段包含的值必须是一个十进制数.";
$lang['less_than']			= "%s字段包含的值必须小于%s.";
$lang['greater_than']		= "%s字段包含的值必须大于%s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
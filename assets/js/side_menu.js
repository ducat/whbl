$(function() {
	$('.accordion-group .accordion-toggle').each(function() {
		$(this).click(function() {
			$('.accordion-group .accordion-toggle:not("#' + $(this).attr('id') + '")').addClass('collapsed');
		});
	});
});
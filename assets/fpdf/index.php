<?php
require('chinese.php');
class PDF extends PDF_Chinese
{
	function pdftable($header,$headerw,$fontsize,$arr){//列标题，列宽度，字体大小，数据
		$this->AddGBFont(); //设置中文字体
		$this->Open(); //开始创建PDF
		$this->AddPage(); //增加一页
		
		$this->SetFont('GB','',$fontsize); //设置字体样式
		
		for($i=0;$i<count($header);$i++){ //循环输出表头
			$header[$i]=iconv('utf-8','GBK',$header[$i]);
			$this->Cell($headerw[$i],6,$header[$i],1);
		}
		$this->Ln();
		
		foreach($arr as $row){ //循环输出表体
			$i=0;
			foreach($row as $v){
				$v=iconv('utf-8','GBK',$v);
				$this->Cell($headerw[$i],6,$v,1);
				$i++;
			}
			$this->Ln();
		}
		ob_end_clean();
		$this->Output(); //下载PDF文件
	}

}
?> 